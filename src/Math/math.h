#pragma once

#include "Core/core.h"
#include "Common/interface/BasicMath.hpp"

namespace nshl
{
    using float2 = Diligent::float2;
    using float3 = Diligent::float3;
    using float4 = Diligent::float4;
    using float3x3 = Diligent::float3x3;
    using float4x4 = Diligent::float4x4;
    using Quaternion = Diligent::Quaternion;

    static const float3 f3Zero = {.0f, .0f, .0f};
    static const float3 f3One = {1.0f, 1.0f, 1.0f};
    static const float3 f3Frwrd = {.0f, .0f, 1.0f};
    static const float3 f3Aside = {1.0f, .0f, .0f};
    static const float3 f3Up = {.0f, 1.0f, .0f};
    static const float3x3 m3Identity = float3x3::Identity();
    static const float4x4 m4Identity = float4x4::Identity();

    template<typename T>
    constexpr T clamp(const T val, const T min, const T max)
    {
        return val < min ? min : val > max ? max : val;
    }

    template<typename T>
    constexpr T max(const T a, const T b)
    {
        return a >= b ? a : b;
    }

    template<typename T>
    constexpr T min(const T a, const T b)
    {
        return a <= b ? a : b;
    }

    constexpr float modAngle(const float angle, const float halfAngle)
    {
        if (halfAngle == 0.0f)
        {
            return 0.0f;
        }
        const int totalSteps = static_cast<int>(angle / halfAngle);
        if (totalSteps == 0)
            return angle;
        return halfAngle * -1.0f * (float)(totalSteps % 2) + fmodf(angle, halfAngle);
    }

    template <uint32 NumComponents = 3, typename VectorType>
    auto dot(const VectorType& a, const VectorType& b)
    {
        auto value = a[0] * b[0];
        for (uint32 i = 1; i < NumComponents; i++)
            value += a[i] * b[i];
        return value;
    }

    template <uint32 NumComponents = 3, typename VectorType>
    auto length(const VectorType& a)
    {
        return sqrt(dot<NumComponents, VectorType>(a, a));
    }

    template <uint32 NumComponents = 3, class VectorType>
    VectorType normalized(const VectorType& a)
    {
        VectorType b = a;
        auto len = length<NumComponents, VectorType>(b);
        for (uint32 i = 0; i < NumComponents; i++)
            b[i] /= len;
        return b;
    }

    template <uint32 NumComponents = 3, class VectorType>
    auto normalize(VectorType& a)
    {
        auto len = length<NumComponents, VectorType>(a);
        for (uint32 i = 0; i < NumComponents; i++)
            a[i] /= len;
        return len;
    }

    template <typename T>
    T lerp_t(const T& Left, const T& Right, float w)
    {
        return Left * (1.f - w) + Right * w;
    }
    template<>Quaternion lerp_t<Quaternion>(const Quaternion& Left, const Quaternion& Right, float w);

    struct float4x3
    {
        float3x3 Orientation;
        float3 Value;
    };

    float3 multiplyEach(const float3& lhs, const float3& rhs);

    inline float3 operator*(const float3& lhs, const float2& rhs) {
        return { lhs.x * rhs.x,  lhs.y * rhs.y, lhs.z };
    }
    
    float4x4 Projection(uint32 Width, uint32 Height, float FOV, float zNear, float zFar, bool isGLDevice = false);
    float4x4 Orthographic(uint32 Width, uint32 Height, float zNear, float zFar, bool isGLDevice = false);

    float3x3 ExtractOrientation(const float4x4& tr);
    float4x3 ToInstanceMatrix(const float4x4& tr);

    struct Transform
    {
        float3x3 Orientation = m3Identity;
        float3 Position = { 0.0f, 0.0f, 0.0f };
        float3 Rotation = { 0.0f, 0.0f, 0.0f };
        float3 Scale = f3One;
        uint64 Parent = InvalidID<uint64>();
        float4x4 AsMatrix() const;
        float4x4 AsViewMatrix() const;
        const float3 GetEulerAngles() const;
        const float3x3& SetRotation(const float3& euler);
        void SetLookDirection(const float3& forward, const float3& up);
        float3 Forward() const;
        float3 Aside() const;
        float3 Up() const;
        float Yaw() const; // rotation around Y axis
        float Pitch() const; // rotation around X axis
        float Roll() const; // rotation around Z axis

        static float4x3 AsTransformMatrix(const float4x4& tr);
    private:
        const float3x3& UpdateOrientation(const float3& euler);
        float3x3 GetRotationMatrix(const float3& euler) const;
    };
}