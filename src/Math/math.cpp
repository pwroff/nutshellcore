#include "math.h"

namespace nshl
{
    float3 multiplyEach(const float3& lhs, const float3& rhs) {
        return { lhs.x * rhs.x,  lhs.y * rhs.y, lhs.z * rhs.z };
    }

    float4x4 Projection(uint32 Width, uint32 Height, float FOV, float zNear, float zFar, bool isGLDevice)
    {
        float AspectRatio = static_cast<float>(Width) / static_cast<float>(Height);
        float YScale = 1.f / std::tan(FOV / 2.f);
        float XScale = YScale / AspectRatio;

        return float4x4::Projection(FOV, AspectRatio, zNear, zFar, isGLDevice);
    }

    float4x4 Orthographic(uint32 Width, uint32 Height, float zNear, float zFar, bool isGLDevice)
    {
        return float4x4::Ortho(static_cast<float>(Width), static_cast<float>(Height), zNear, zFar, isGLDevice);
    }

    float3x3 ExtractOrientation(const float4x4& mat)
    {
        return {
                mat._11, mat._12, mat._13,
                mat._21, mat._22, mat._23,
                mat._31, mat._32, mat._33
        };
    }

    float4x3 ToInstanceMatrix(const float4x4& tr)
    {
        return
        {
            {
                tr._11, tr._12, tr._13,
                tr._21, tr._22, tr._23,
                tr._31, tr._32, tr._33
            },
            {
                tr._41, tr._42, tr._43
            }
        };
    }

    float4x4 Transform::AsMatrix() const
    {
        float3x3 orient = Orientation * float3x3::Scale(Scale.x, Scale.y, Scale.z);
        return float4x4
        {
            orient._11, orient._12, orient._13, 0.0f,
            orient._21, orient._22, orient._23, 0.0f,
            orient._31, orient._32, orient._33, 0.0f,
            Position.x,   Position.y,   Position.z, 1.0f
        };
    }

    float4x4 Transform::AsViewMatrix() const
    {
        float4x4 orient = (AsMatrix()).Inverse();
        
        return float4x4
        {
            orient._11, orient._12, orient._13, 0.0f,
                orient._21, orient._22, orient._23, 0.0f,
                orient._31, orient._32, orient._33, 0.0f,
                Position.x, Position.y, Position.z, 1.0f
        };
    }

    float4x3 Transform::AsTransformMatrix(const float4x4& mat)
    {
        return float4x3
        {
            {
                mat._11, mat._12, mat._13,
                mat._21, mat._22, mat._23,
                mat._31, mat._32, mat._33
            },
            {
                mat._41, mat._42, mat._43
            }
        };
    }
    const float3 Transform::GetEulerAngles() const
    {
        return Rotation;
    }
    const float3x3& Transform::SetRotation(const float3& euler)
    {
        Rotation.x = modAngle(euler.x, PI);
        Rotation.y = modAngle(euler.y, PI);
        Rotation.z = modAngle(euler.z, PI);
        return UpdateOrientation(Rotation);
    }
    const float3x3& Transform::UpdateOrientation(const float3& euler)
    {
        Orientation = GetRotationMatrix(euler);
        return Orientation;
    }
    float3x3 Transform::GetRotationMatrix(const float3& euler) const
    {
        float sy = sinf(euler.y);//rotation about Y (yaw)
        float cy = cosf(euler.y);

        float sp = sinf(euler.x);//rotation about X (pitch)
        float cp = cosf(euler.x);

        float sr = sinf(euler.z);//rotation about Z (roll)
        float cr = cosf(euler.z);

        float3x3 rotMat;

        rotMat._11 = cy * cr + sr * sp * sy;
        rotMat._12 = -sr * cp;
        rotMat._13 = -cr * sy + sr * sp * cy;

        rotMat._21 = sr * cy - cr * sp * sy;
        rotMat._22 = cr * cp;
        rotMat._23 = -sr * sy - cr * sp * cy;

        rotMat._31 = cp * sy;
        rotMat._32 = sp;
        rotMat._33 = cp * cy;
        return rotMat;
    }
    void Transform::SetLookDirection(const float3& forward, const float3& up)
    {
        const auto aside = cross(forward, up);

        Orientation = float3x3(
            aside.x, aside.y, aside.z,
            up.x, up.y, up.z,
            forward.x, forward.y, forward.z
        );
    }
    float3 Transform::Forward() const
    {
        return {
             Orientation._31,
             Orientation._32,
             Orientation._33
        };
    }
    float3 Transform::Aside() const
    {
        return {
            Orientation._11,
            Orientation._12,
            Orientation._13
        };
    }
    float3 Transform::Up() const
    {
        return {
            Orientation._21,
            Orientation._22,
            Orientation._23
        };
    }
    float Transform::Yaw() const
    {
        return Rotation.y;
    }
    float Transform::Pitch() const
    {
        return Rotation.x;
    }
    float Transform::Roll() const
    {
        return Rotation.z;
    }

    template<>
    Quaternion lerp_t<Quaternion>(const Quaternion& Left, const Quaternion& Right, float w)
    {
        return Diligent::slerp(Left, Right, w);
    }
}
