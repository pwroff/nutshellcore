/*
 *  Copyright 2019-2021 Diligent Graphics LLC
 *  Copyright 2015-2019 Egor Yusov
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  In no event and under no legal theory, whether in tort (including negligence),
 *  contract, or otherwise, unless required by applicable law (such as deliberate
 *  and grossly negligent acts) or agreed to in writing, shall any Contributor be
 *  liable for any damages, including any direct, indirect, special, incidental,
 *  or consequential damages of any character arising as a result of this License or
 *  out of the use or inability to use the software (including but not limited to damages
 *  for loss of goodwill, work stoppage, computer failure or malfunction, or any and
 *  all other commercial damages or losses), even if such Contributor has been advised
 *  of the possibility of such damages.
 */

#include "Core/Application.h"
#include "Graphics/RenderPass/RenderPass.h"
#include "Scene/Scene.h"
#include "Scene/Camera.h"
#include "Graphics/Renderable.h"
#include "Graphics/Material/ConstantBuffers.h"
#include "Graphics/Texture/Texture.h"
#include "imgui.h"
#include "Tracy.hpp"

using namespace Diligent;

namespace nshl
{
    class DemoApp: public Application
    {
    public:

        void CreateStaticResources() override
        {

            m_Scene.Init(&m_Graphics);
            const auto& sDesc = m_Graphics.GetSwapChain()->GetDesc();
            m_Camera.m_Transform.Position = { 0, 5.0f, -10.0f };
            m_Camera.UpdateProjection(sDesc.Width, sDesc.Height);
            m_Camera.UpdateView();
            m_pWFPass.Init(&m_Graphics);
            m_pDepthPass.Init(&m_Graphics);
            m_pPPPass.Init(&m_Graphics);
            m_pDeferedPass.Init(&m_Graphics);
        }

        void OnRender() override
        {
            RenderContext ctx;
            ctx.Gfx = &m_Graphics;
            m_Camera.UpdateView();
            ctx.ActiveCamera = &m_Camera;
            ctx.Scene = &m_Scene;
            ctx.RenderTarget = m_pPPPass.UseFrontRTV();
            ctx.DepthStencilView = m_pPPPass.UseFrontDSV();

            m_pDepthPass.Render(ctx);

            m_pClearBBPass.Render(ctx);
            if (auto* buff = m_Resources.Buffers().Find(CameraCB::TypeID))
                buff->Update(ctx);
            if (auto* buff = m_Resources.Buffers().Find(LightCB::TypeID))
                buff->Update(ctx);
            m_pDeferedPass.Render(ctx);
            m_pDirectPassPass.Render(ctx);
            m_pPPPass.Render(ctx);
            if (m_bWireframeEnabled)
                m_pWFPass.Render(ctx);
        }

        void OnUpdate(float dt) override
        {
            m_Scene.Update(dt);

            const auto& inp = InputAgent::State();
            static float baseSpeed = 10.0f;
            const float speed = baseSpeed * dt;
            if (inp.Trigger(InputMapping::W_Key).IsDown())
            {
                m_Camera.m_Transform.Position += m_Camera.m_Transform.Forward() * speed;
            }
            if (inp.Trigger(InputMapping::S_Key).IsDown())
            {
                m_Camera.m_Transform.Position -= m_Camera.m_Transform.Forward() * speed;
            }
            if (inp.Trigger(InputMapping::A_Key).IsDown())
            {
                m_Camera.m_Transform.Position -= m_Camera.m_Transform.Aside() * speed;
            }
            if (inp.Trigger(InputMapping::D_Key).IsDown())
            {
                m_Camera.m_Transform.Position += m_Camera.m_Transform.Aside() * speed;
            }
            if (inp.Trigger(InputMapping::E_Key).IsDown())
            {
                m_Camera.m_Transform.Position += m_Camera.m_Transform.Up() * speed;
            }
            if (inp.Trigger(InputMapping::Q_Key).IsDown())
            {
                m_Camera.m_Transform.Position -= m_Camera.m_Transform.Up() * speed;
            }
            if (inp.Trigger(InputMapping::Space_Key).IsPressed())
            {
                m_bWireframeEnabled = !m_bWireframeEnabled;
            }

            if (inp.Trigger(InputMapping::RM_Button).IsPressed())
            {
                InputAgent::LockMouse();
            }

            if (inp.Trigger(InputMapping::RM_Button).IsReleased())
            {
                InputAgent::UnlockMouse();
            }

            if (inp.Trigger(InputMapping::RM_Button).IsDown())
            {
                const float orientationSpeed = PI * 0.075f * dt;
                const float2 rotationRate =  inp.DeltaPos * orientationSpeed;
                float3 currentRot = m_Camera.m_Transform.GetEulerAngles();
                currentRot.y += rotationRate.x;
                currentRot.x -= rotationRate.y;
                m_Camera.m_Transform.SetRotation(currentRot);
            }
            baseSpeed = clamp(baseSpeed + inp.ScrollDelta, 0.001f, 1000.0f);
            
            m_Camera.UpdateView();
        }

        void OnGui() override
        {
            ImGui::SetNextWindowPos(ImVec2(10, 10), ImGuiCond_FirstUseEver);
            {
                if (ImGui::Begin("Main", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
                {
                    const float frameTime = m_Timer.GetDeltaTime();
                    const int fps = frameTime != 0.0f ? int(1.0f / frameTime) : 0;
                    ImGui::Text("FPS: %i", fps);
                    ImGui::Checkbox("Postrpocess", &m_pPPPass.m_bActive);
                    ImGui::Checkbox("Wireframe", &m_bWireframeEnabled);
                    if (ImGui::DragFloat3("Camera Position", &m_Camera.m_Transform.Position.x, 0.01f, -100.0f, 100.0f))
                        m_Camera.UpdateView();
                    float3 smRot = m_Camera.m_Transform.GetEulerAngles() * RAD2DEG;
                    if (ImGui::DragFloat3("Camera Rotation", &smRot.x))
                    {
                        m_Camera.m_Transform.SetRotation(DEG2RAD * smRot);
                        m_Camera.UpdateView();
                    }
                    if (ImGui::DragFloat("Camera FOV", &m_Camera.m_FOV))
                    {
                        const auto& sDesc = m_Graphics.GetSwapChain()->GetDesc();
                        m_Camera.UpdateProjection(sDesc.Width, sDesc.Height);
                    }

                    if (ImGui::Button("Save G Buffers"))
                    {
                        auto* text = Resources::Instance()->Textures().Resolve<DeferredRenderTargetMap>({ DeferredRenderTargetMap::LOOKUP_NAME });
                        text->DebugSaveToFile(&m_Graphics);
                    }
                }
                ImGui::End();
            }

            m_Scene.OnGui();
            
        }

        void OnWindowResize(uint32 Width, uint32 Height) override
        {
            m_Camera.UpdateProjection(Width, Height);
        }

    private:
        Camera m_Camera;
        DemoScene m_Scene;
        ClearBackBufferPass m_pClearBBPass;
        DirectRenderPass m_pDirectPassPass;
        DeferredRenderPass m_pDeferedPass;
        WireframePass m_pWFPass;
        ShadowDepthPass m_pDepthPass;
        PostProcessPass m_pPPPass;
        bool m_bWireframeEnabled = false;
    };

    
}
std::unique_ptr<nshl::DemoApp> g_pTheApp;
    // Main
    int WINAPI WinMain(HINSTANCE instance, HINSTANCE pinst, LPSTR pstr, int cmdShow)
    {
        ZoneScoped;
        g_pTheApp.reset(new nshl::DemoApp);
        /*
        AllocConsole();
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
        */
        auto ret = g_pTheApp->RunWin32(instance, pinst, pstr, cmdShow);

        g_pTheApp.release();

        return ret;
    }