#include "Application.h"
#include "Tracy.hpp"

namespace nshl
{

    LRESULT CALLBACK MessageProc(HWND wnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        return Application::Instance()->Win32MessageProc(wnd, message, wParam, lParam);
    }
	int Application::RunWin32(HINSTANCE instance, HINSTANCE, LPSTR, int cmdShow)
	{
#if defined(_DEBUG) || defined(DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
        using namespace Diligent;
        const auto* cmdLine = GetCommandLineA();
        if (!ProcessCommandLine(cmdLine))
            return -1;

        // Register our window class
        WNDCLASSEX wcex = { sizeof(WNDCLASSEX), CS_HREDRAW | CS_VREDRAW, MessageProc,
                           0L, 0L, instance, NULL, NULL, NULL, NULL, L"NutshellApp", NULL };
        RegisterClassEx(&wcex);

        // Create a window
        LONG WindowWidth = 1920;
        LONG WindowHeight = 1080;
        RECT rc = { 0, 0, WindowWidth, WindowHeight };
        AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
        HWND wnd = CreateWindow(L"NutshellApp", GetTitle(),
            WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
            rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, instance, NULL);
        if (!wnd)
        {
            MessageBox(NULL, L"Cannot create window", L"Error", MB_OK | MB_ICONERROR);
            return 0;
        }
        ShowWindow(wnd, cmdShow);
        UpdateWindow(wnd);

        if (!Initialize(wnd))
            return -1;

        CreateStaticResources();

        // Main message loop
        MSG msg = { 0 };
        while (WM_QUIT != msg.message)
        {
            if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            else
            {
                Tick();
            }
        }

        return (int)msg.wParam;
	}

    LRESULT Application::Win32MessageProc(HWND wnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        if (m_pImGui)
        {
            auto Handled = static_cast<Diligent::ImGuiImplWin32*>(m_pImGui.get())->Win32_ProcHandler(wnd, message, wParam, lParam);
            if (Handled)
                return Handled;
        }
        if (m_InputAgent.HandleWindowMessages({ wnd, message, wParam, lParam }))
            return 0;
        switch (message)
        {
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            BeginPaint(wnd, &ps);
            EndPaint(wnd, &ps);
            return 0;
        }
        case WM_SIZE: // Window size has been changed
            SystemWindowDidResize(LOWORD(lParam), HIWORD(lParam));
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;

        case WM_GETMINMAXINFO:
        {
            LPMINMAXINFO lpMMI = (LPMINMAXINFO)lParam;

            lpMMI->ptMinTrackSize.x = 320;
            lpMMI->ptMinTrackSize.y = 240;
            return 0;
        }

        default:
            return DefWindowProc(wnd, message, wParam, lParam);
        }
    }

    void Application::Quit()
    {
        PostQuitMessage(0);
    }

    void Application::SystemWindowDidResize(uint32 w, uint32 h)
    {
        m_Graphics.OnWindowResize(w, h);
        OnWindowResize(w, h);
    }

    bool Application::Initialize(HWND hwnd)
    {
        if (!m_Graphics.Initialize(hwnd, GetDeviceType()))
            return false;

        // Initialize Dear ImGUI
        const auto& SCDesc = m_Graphics.GetSwapChain()->GetDesc();
        m_pImGui.reset(new Diligent::ImGuiImplWin32(hwnd, m_Graphics.UseDevice(), SCDesc.ColorBufferFormat, SCDesc.DepthBufferFormat));
        return true;
    }
    void Application::Tick()
    {
        ZoneScoped;
        m_Timer.StartFrame();

        {
            ZoneScopedN("OnUpdate");
            OnUpdate(m_Timer.GetDeltaTime());
        }
        
        {
            ZoneScopedN("OnRender");
            OnRender();
        }
        
        {
            ZoneScopedN("OnGui");
            if (m_pImGui)
            {
                const auto& SCDesc = m_Graphics.GetSwapChain()->GetDesc();
                m_pImGui->NewFrame(SCDesc.Width, SCDesc.Height, SCDesc.PreTransform);
                OnGui();
                m_pImGui->Render(m_Graphics.UseContext());
            }
        }
        {
            ZoneScopedN("Present");
            m_Graphics.Present();
        }
        
        m_InputAgent.FrameEnd();
    }
}
