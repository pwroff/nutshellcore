#pragma once
#include "Core/core.h"
#include "Resources/Resources.h"
#include "Graphics/Gfx.h"
#include "Scene/Timer.h"
#include "Imgui/interface/ImGuiImplWin32.hpp"
#include "Platform/IO/InputAgent.h"

namespace nshl
{
	class Application: public Singletone<Application>
	{
		std::unique_ptr<Diligent::ImGuiImplDiligent> m_pImGui;

	protected:
		Timer m_Timer = { 600 };
		Gfx m_Graphics;
		Resources m_Resources;
		InputAgentWin32 m_InputAgent;

	public:
		virtual const wchar_t* GetTitle() const { return L"Nutshell"; }
		virtual bool ProcessCommandLine(const char* CmdLine)
		{
			return true;
		}
		virtual Diligent::RENDER_DEVICE_TYPE GetDeviceType() const { return Diligent::RENDER_DEVICE_TYPE::RENDER_DEVICE_TYPE_D3D12; }

		int RunWin32(HINSTANCE instance, HINSTANCE, LPSTR, int cmdShow);
		LRESULT Win32MessageProc(HWND wnd, UINT message, WPARAM wParam, LPARAM lParam);
		void Quit();

	private:
		void SystemWindowDidResize(uint32 w, uint32 h);
		bool Initialize(HWND hwnd);
		void Tick();

	protected:
		virtual void OnUpdate(float dt) {}
		virtual void OnRender() {}
		virtual void OnGui() {}
		virtual void CreateStaticResources() {}
		virtual void OnWindowResize(uint32 width, uint32 height) {}
	};
}
