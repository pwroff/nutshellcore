#pragma once
#include <cassert>

#define nshl_assert2(exp, msg) assert(((void)msg, exp))
#define nshl_assert(exp) assert((exp))

#include "Common/interface/RefCntAutoPtr.hpp"

#include "Buffer.h"

typedef unsigned char ubyte;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

template<typename T>
constexpr T InvalidID()
{
	return T(-1);
}

namespace nshl
{
	static constexpr float PI = 3.1415927f;
	static constexpr float DEG2RAD = 0.0174533f;
	static constexpr float RAD2DEG = 57.2957795f;

	using IBuffer = Diligent::IBuffer;

	class ICoreObject
	{

	};

	template<typename T>
	class RefPtr : public Diligent::RefCntAutoPtr<T>
	{
	public:
		using Diligent::RefCntAutoPtr<T>::RefCntAutoPtr;
	};

	struct RenderContext
	{
		class Gfx* Gfx;
		struct Camera* ActiveCamera;
		class IScene* Scene;
		struct RenderTarget* RenderTarget;
		struct DepthTexture* DepthStencilView;
	};
}
