#pragma once
#include "core.h"
#include <vector>
#include <typeinfo>

namespace nshl
{
    template<typename T>
    class Singletone
    {
        static T* sm_Instance;

    public:

        Singletone()
        {
            nshl_assert2(!sm_Instance, "Instance already exist");
            sm_Instance = static_cast<T*>(this);
        }
        ~Singletone()
        {
            sm_Instance = nullptr;
        }

        static T* Instance() { return sm_Instance; }
    };

    template<typename T>
    T* Singletone<T>::sm_Instance = nullptr;


    template<typename T, typename ... Args>
    struct Array : public std::vector<T, Args...>
    {
        using base = typename std::vector<T, Args...>;
        using baseIterator = typename base::iterator;

    public:
        auto Find(T obj)
        {
            for (auto it = base::begin(); it != base::end(); ++it) {
                if (*it == obj)
                    return it;
            }
            return base::end();
        }

        bool Contains(T obj) const
        {
            for (auto it = base::begin(); it != base::end(); ++it) {
                if (*it == obj)
                    return true;
            }
            return false;
        }

        bool Remove(T obj)
        {
            if (auto it = Find(obj); it != base::end())
            {
                base::erase(it);
                return true;
            }
            return false;
        }

        bool IsValid(uint64 idx) const
        {
            return idx < base::size();
        }
    };

    struct IdGen
    {
        uint64 Next()
        {
            m_Current++;
            return m_Current;
        }
    private:
        uint64 m_Current = InvalidID<uint64>();
    };

    template<typename T>
    class WithIDGen
    {
        static IdGen sm_IDGen;
    protected:
        static uint64 NextID() { return sm_IDGen.Next(); }
    public:
        virtual const char* GetName() const = 0;
        virtual uint64 GetID() const = 0;
    };

    template<typename T>
    IdGen WithIDGen<T>::sm_IDGen = {};


    template<typename T, typename Base>
    struct WithTypeID : public Base
    {
        const char* GetName() const override { return TypeName; }
        uint64 GetID() const override { return TypeID; }
        static const char* TypeName;
        static const uint64 TypeID;
    };

    template<typename T, typename Base>
    const uint64 WithTypeID<T, Base>::TypeID = Base::NextID();

    template<typename T, typename Base>
    const char* WithTypeID<T, Base>::TypeName = typeid(T).name();


    template<typename T>
    struct Bitfield
    {
        T Value = 0u;

        bool IsSet(T v) const { return (Value & v) != 0; }
        void Set(T v) { Value |= v; }
        void Clear(T v) { Value &= ~v; }

        bool IsBitSet(ubyte b) const { return (Value & (1 << b)) != 0; }
        void SetBit(ubyte b) { Value |= (1 << b); }
        void ClearBit(ubyte b) { Value &= ~(1 << b); }
    };

#define BIT(v) 1 << v
}
