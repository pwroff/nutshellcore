#include "Texture.h"
#include "Graphics/Gfx.h"
#include "TextureLoader/interface/TextureUtilities.h"
#include "TextureLoader/interface/Image.h"
#include "Graphics/RenderPass/RenderPass.h"

namespace nshl
{
	void Texture2D::Init(Gfx* gfx, const char* filePath)
	{
        Diligent::TextureLoadInfo loadInfo;
        loadInfo.IsSRGB = true;
        Diligent::CreateTextureFromFile(filePath, loadInfo, gfx->UseDevice(), &m_CTex);
        // Get shader resource view from the texture
        m_SRV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_SHADER_RESOURCE));
	}
    void TextureColor::Init(Gfx* gfx, float4 Color)
    {
        ubyte raw[4] = { 0 };
        raw[0] = ubyte(Color.x * 255);
        raw[1] = ubyte(Color.y * 255);
        raw[2] = ubyte(Color.z * 255);
        raw[3] = ubyte(Color.w * 255);

        RefPtr<Diligent::IDataBlob> blob;
        {
            Diligent::Image::EncodeInfo encInfo;
            encInfo.Width = 1;
            encInfo.Height = 1;
            encInfo.TexFormat = Diligent::TEX_FORMAT_RGBA8_UNORM;
            encInfo.KeepAlpha = true;
            encInfo.FileFormat = Diligent::IMAGE_FILE_FORMAT_PNG;
            encInfo.Stride = 4;
            encInfo.pData = raw;
            Diligent::Image::Encode(encInfo, &blob);
        }
        RefPtr<Diligent::Image> image;
        {
            Diligent::ImageLoadInfo LoadInfo;
            LoadInfo.Format = Diligent::IMAGE_FILE_FORMAT_PNG;
            Diligent::Image::CreateFromDataBlob(blob, LoadInfo, &image);
        }
        Diligent::TextureLoadInfo loadInfo;
        loadInfo.IsSRGB = true;
        loadInfo.GenerateMips = false;
        loadInfo.MipLevels = 1;
        Diligent::CreateTextureFromImage(image, loadInfo, gfx->UseDevice(), &m_CTex);
        // Get shader resource view from the texture
        m_SRV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_SHADER_RESOURCE));
    }
    void DepthTexture::Init(Gfx* gfx, uint32 width, uint32 height)
    {
        Diligent::TextureDesc SMDesc;
        SMDesc.Name = "Shadow map";
        SMDesc.Type = Diligent::RESOURCE_DIM_TEX_2D;
        SMDesc.Width = width;
        SMDesc.Height = height;
        SMDesc.Format = SHADOW_MAP_FORMAT;
        SMDesc.BindFlags = Diligent::BIND_SHADER_RESOURCE | Diligent::BIND_DEPTH_STENCIL;
        gfx->UseDevice()->CreateTexture(SMDesc, nullptr, &m_CTex);
        m_SRV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_SHADER_RESOURCE));
        m_DSV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_DEPTH_STENCIL));
    }
    void DepthTexture::Init(Gfx* gfx)
    {
        const auto& desc = gfx->GetSwapChain()->GetDesc();
        Init(gfx, desc.Width, desc.Height);
    }
    void ShadowMap::Init(Gfx* gfx)
    {
        m_aDepthTextures.reserve(MAX_SHADOW_MAPS);
        for (uint32 i = 0; i < MAX_SHADOW_MAPS; i++)
        {
            m_aDepthTextures.push_back({});
            m_aDepthTextures[i].Init(gfx, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
        }
    }
    void RenderTarget::Init(Gfx* gfx, uint32 width, uint32 height, Diligent::TEXTURE_FORMAT format)
    {
        Diligent::TextureDesc SMDesc;
        SMDesc.Name = "Render target";
        SMDesc.Type = Diligent::RESOURCE_DIM_TEX_2D;
        SMDesc.Width = width;
        SMDesc.Height = height;
        if (format == Diligent::TEXTURE_FORMAT::TEX_FORMAT_UNKNOWN)
            SMDesc.Format = gfx->GetSwapChain()->GetDesc().ColorBufferFormat;
        else
        {
            SMDesc.Format = format;
        }
        SMDesc.BindFlags = Diligent::BIND_SHADER_RESOURCE | Diligent::BIND_RENDER_TARGET;
        gfx->UseDevice()->CreateTexture(SMDesc, nullptr, &m_CTex);
        m_SRV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_SHADER_RESOURCE));
        m_RTV.Attach(m_CTex->GetDefaultView(Diligent::TEXTURE_VIEW_RENDER_TARGET));
    }
    void RenderTarget::Init(Gfx* gfx)
    {
        const auto& desc = gfx->GetSwapChain()->GetDesc();
        Init(gfx, desc.Width, desc.Height);
    }
    void DeferredRenderTargetMap::Init(Gfx* gfx)
    {
        m_aDeferredTextures.reserve(DeferredRenderPass::NUM_G_BUFFERS);
        const auto& desc = gfx->GetSwapChain()->GetDesc();
        for (uint32 i = 0; i < DeferredRenderPass::NUM_G_BUFFERS; i++)
        {
            m_aDeferredTextures.push_back({});
            m_aDeferredTextures[i].Init(gfx, desc.Width, desc.Height, DeferredRenderPass::G_BUFFER_FORMATS[i]);
        }
    }

    void DeferredRenderTargetMap::DebugSaveToFile(Gfx* gfx)
    {
        for (uint32 i = 0; i < DeferredRenderPass::NUM_G_BUFFERS; i++)
        {
            std::string name = std::string(LOOKUP_NAME) + "_" + char(i);
            gfx->SaveTextureToFile(name, &m_aDeferredTextures[i]);
        }
    }
}
