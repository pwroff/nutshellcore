#pragma once
#include "Core/core.h"
#include "Math/math.h"

namespace nshl
{
	class Gfx;
	class ITexture : public ICoreObject
	{
		
	public:
		virtual void Init(Gfx* gfx) {}
		// Shader Resource View
		virtual Diligent::ITextureView* SRV() { return nullptr; }
		// Depth Stencil View
		virtual Diligent::ITextureView* DSV() { return nullptr; }
		// Render Target View
		virtual Diligent::ITextureView* RTV() { return nullptr; }
		// G-Buffer view
		virtual Diligent::ITexture* GPUTEX() { return nullptr; }

	};

	struct Texture2D: public ITexture
	{
		RefPtr<Diligent::ITexture> m_CTex;
		RefPtr<Diligent::ITextureView> m_SRV;

		Diligent::ITextureView* SRV() override { return m_SRV; }
		Diligent::ITexture* GPUTEX() override { return m_CTex; }

		void Init(Gfx* gfx, const char* filePath);
	};

	struct TextureColor : public Texture2D
	{
		void Init(Gfx* gfx, float4 Color);
	};

	struct DepthTexture : public Texture2D
	{
		RefPtr<Diligent::ITextureView> m_DSV;
		Diligent::ITextureView* DSV() override { return m_DSV; }
		void Init(Gfx* gfx, uint32 width, uint32 height);
		void Init(Gfx* gfx);
	};

	struct RenderTarget : public Texture2D
	{
		RefPtr<Diligent::ITextureView> m_RTV;
		Diligent::ITextureView* RTV() override { return m_RTV; }
		void Init(Gfx* gfx, uint32 width, uint32 height, Diligent::TEXTURE_FORMAT format = Diligent::TEXTURE_FORMAT::TEX_FORMAT_UNKNOWN);
		void Init(Gfx* gfx);
	};

	struct DeferredRenderTargetMap : public ITexture
	{
		static constexpr char LOOKUP_NAME[] = "DeferredGBuffers";
		std::vector<RenderTarget> m_aDeferredTextures;
		void Init(Gfx* gfx);

		void DebugSaveToFile(Gfx* gfx);
	};

	struct ShadowMap : public ITexture
	{
		std::vector<DepthTexture> m_aDepthTextures;
		void Init(Gfx* gfx);
	};
}
