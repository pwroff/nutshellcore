#pragma once
#include "Core/core.h"
#include "Graphics/Material/Material.h"
#include "Graphics/Texture/Texture.h"

namespace nshl
{
    class Gfx;
}

namespace nshl
{
    class IRenderPass
    {
    public:
        virtual void Render(const RenderContext& ctx) = 0;
        virtual void Init(Gfx* gfx) {}
    };

    class ClearBackBufferPass: public IRenderPass
    {
    public:
        void Render(const RenderContext& ctx) override;
    };

    class DirectRenderPass : public IRenderPass
    {
    public:
        void Render(const RenderContext& ctx) override;
    };

    class DeferredRenderPass : public IRenderPass
    {
        DeferredRenderTargetMap* m_GBuffers;
        IMaterial* m_DeferedLightingMat;
    public:
        void Init(Gfx* gfx) override;
        void Render(const RenderContext& ctx) override;
        

        static constexpr uint32 NUM_G_BUFFERS = 4;
        static constexpr Diligent::TEXTURE_FORMAT G_BUFFER_FORMATS[NUM_G_BUFFERS] = {
            Diligent::TEXTURE_FORMAT::TEX_FORMAT_RGBA8_UNORM_SRGB,
            Diligent::TEXTURE_FORMAT::TEX_FORMAT_RGBA32_FLOAT,
            Diligent::TEXTURE_FORMAT::TEX_FORMAT_RGBA32_FLOAT,
            Diligent::TEXTURE_FORMAT::TEX_FORMAT_RGBA32_FLOAT
        };
    };

    class WireframePass : public IRenderPass
    {
        IMaterial* m_WireframeMat;
    public:
        void Init(Gfx* gfx) override;
        void Render(const RenderContext& ctx) override;
    };

    class ShadowDepthPass : public IRenderPass
    {
        IMaterial* m_DepthMaterial;
        ShadowMap* m_ShadowMap;
    public:
        void Init(Gfx* gfx) override;
        void Render(const RenderContext& ctx) override;
    };

    class PostProcessPass : public IRenderPass
    {
        Array<IMaterial*> m_aPPMaterials;
        Array<RenderTarget*> m_aAdditionalRenderTargets;
        Array<DepthTexture*> m_aAdditionalDepthTextures;
    public:
        void Init(Gfx* gfx) override;
        void Render(const RenderContext& ctx) override;
        RenderTarget* UseFrontRTV();
        DepthTexture* UseFrontDSV();

        bool m_bActive = true;
    };
}
