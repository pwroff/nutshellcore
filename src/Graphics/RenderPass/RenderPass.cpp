#include "RenderPass.h"
#include "Core/core.h"
#include "Graphics/Gfx.h"
#include "Graphics/Renderable.h"
#include "Resources/Resources.h"
#include "Scene/Scene.h"
#include "Graphics/Material/ConstantBuffers.h"
#include "Graphics/Material/PBRMaterial.h"

namespace nshl
{
    void ClearBackBufferPass::Render(const RenderContext& ctx)
    {
        auto* pRTV = ctx.RenderTarget->RTV();
        auto* pDSV = ctx.DepthStencilView->DSV();
        ctx.Gfx->UseContext()->SetRenderTargets(1, &pRTV, pDSV, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        // Clear the back buffer
        const float ClearColor[] = { 0.0150f, 0.0150f, 0.0150f, 1.0f };
        // Let the engine perform required state transitions
        ctx.Gfx->UseContext()->ClearRenderTarget(pRTV, ClearColor, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        ctx.Gfx->UseContext()->ClearDepthStencil(pDSV, Diligent::CLEAR_DEPTH_FLAG, 1.f, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
    }
    void DirectRenderPass::Render(const RenderContext& ctx)
    {
        auto& groups = ctx.Scene->GetRenderGroups();

        for (auto* grp : groups)
        {
            if (grp->m_RenderFlags.IsSet(ERenderFLag::ERF_DEFERRED_OPAQUE) == false)
            {
                grp->Render(ctx);
            }
        }
    }
    void WireframePass::Init(Gfx* gfx)
    {
        m_WireframeMat = Resources::Instance()->Materials().Resolve<WireframeMaterial>({ "WireframeMaterial" });
    }
    void WireframePass::Render(const RenderContext& ctx)
    {
        for (auto* grp : ctx.Scene->GetRenderGroups())
        {
            auto numInd = grp->m_Renderables.size();
            if (numInd > 0)
            {
                grp->m_Mesh->DrawIndexed(ctx, m_WireframeMat, grp->m_InstanceBuffer, uint32(numInd));
            }
        }
    }
    void ShadowDepthPass::Init(Gfx* gfx)
    {
        m_DepthMaterial = Resources::Instance()->Materials().Resolve<DepthRendMaterial>({ "DepthMaterial" });
        m_ShadowMap = Resources::Instance()->Textures().Resolve<ShadowMap>({"ShadowMap"});
    }
    void ShadowDepthPass::Render(const RenderContext& ctx)
    {
        const auto renderFromLight = [this](const RenderContext& inCtx, DepthTexture& buffer)
        {
            inCtx.Gfx->UseContext()->SetRenderTargets(0, nullptr, buffer.DSV(), Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
            inCtx.Gfx->UseContext()->ClearDepthStencil(buffer.DSV(), Diligent::CLEAR_DEPTH_FLAG, 1.f, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
            for (auto* grp : inCtx.Scene->GetRenderGroups())
            {
                auto numInd = grp->m_Renderables.size();
                if (numInd > 0 && grp->m_RenderFlags.IsSet(ERenderFLag::ERF_CASTS_SHADOW))
                {
                    grp->m_Mesh->DrawIndexed(inCtx, m_DepthMaterial, grp->m_InstanceBuffer, uint32(numInd));
                }
            }
        };

        auto& lights = ctx.Scene->UseLights();
        auto* buff = Resources::Instance()->Buffers().Find(CameraCB::TypeID);
        if (!buff)
            return;
        uint32 currentDepthBuffer = 0;
        for (auto& light : lights)
        {
            if (light.Type == ELightType::UNKNOWN)
                break;
            light.UpdateProjectionMatrix();
            RenderContext inCtx = ctx;
            inCtx.ActiveCamera = &light;
            buff->Update(inCtx);
            renderFromLight(inCtx, m_ShadowMap->m_aDepthTextures[currentDepthBuffer]);
            light.ShadowMapIndex = currentDepthBuffer;
            currentDepthBuffer++;
        }
    }
    void PostProcessPass::Init(Gfx* gfx)
    {
        m_aPPMaterials.push_back(Resources::Instance()->Materials().Resolve<TestPPMaterial>({ "PostProcessInvert" }));
        m_aAdditionalRenderTargets.push_back(Resources::Instance()->Textures().Resolve<RenderTarget>({ "DefaultRenderTarget" }));
        m_aAdditionalRenderTargets.push_back(Resources::Instance()->Textures().Resolve<RenderTarget>({ "InvRenderTarget" }));
        m_aAdditionalDepthTextures.push_back(Resources::Instance()->Textures().Resolve<DepthTexture>({ "DSTarget" }));
    }
    void PostProcessPass::Render(const RenderContext& ctx)
    {
        const auto renderPostProcess = [ctx](IMaterial* ppMat, RenderTarget* prevRTV, RenderTarget* rtv)
        {
            ctx.Gfx->UseContext()->SetRenderTargets(1, &rtv->m_RTV, nullptr, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
            const float ClearColor[] = { 0, 0, 0, 1 };
            ctx.Gfx->UseContext()->ClearRenderTarget(rtv->m_RTV, ClearColor, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

            ppMat->SetTexture("Tex_PrevRTV", prevRTV->SRV());
            ppMat->SetTexture("Tex_DSV", ctx.DepthStencilView->SRV());
            ppMat->Bind(ctx);

            Diligent::DrawAttribs DrawAttrs;
            DrawAttrs.NumVertices = 4;
            DrawAttrs.Flags = Diligent::DRAW_FLAG_VERIFY_ALL;
            ctx.Gfx->UseContext()->Draw(DrawAttrs);
            ctx.Gfx->UseContext()->WaitForIdle();
        };

        if (m_bActive)
        {
            for (uint32 i = 0; i < m_aPPMaterials.size(); i++)
            {
                ctx.Gfx->UseContext()->SetRenderTargets(0, nullptr, nullptr, Diligent::RESOURCE_STATE_TRANSITION_MODE_NONE);
                renderPostProcess(m_aPPMaterials[i], m_aAdditionalRenderTargets[i], m_aAdditionalRenderTargets[i + 1]);
            }
        }
        ctx.Gfx->UseContext()->SetRenderTargets(0, nullptr, nullptr, Diligent::RESOURCE_STATE_TRANSITION_MODE_NONE);
        // Final stage
        Diligent::CopyTextureAttribs attribs;
        attribs.pDstTexture = ctx.Gfx->UseSwapChain()->GetCurrentBackBufferRTV()->GetTexture();
        attribs.pSrcTexture = m_bActive ? m_aAdditionalRenderTargets.back()->m_CTex : ctx.RenderTarget->m_CTex;
        attribs.SrcTextureTransitionMode = Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION;
        attribs.DstTextureTransitionMode = Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION;
        ctx.Gfx->UseContext()->CopyTexture(attribs);

        auto* bbRTV = ctx.Gfx->UseSwapChain()->GetCurrentBackBufferRTV();

        ctx.Gfx->UseContext()->SetRenderTargets(1, &bbRTV, nullptr, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
    }
    RenderTarget* PostProcessPass::UseFrontRTV()
    {
        if (m_aAdditionalRenderTargets.size() > 0)
            return m_aAdditionalRenderTargets.front();
        return nullptr;
    }
    DepthTexture* PostProcessPass::UseFrontDSV()
    {
        if (m_aAdditionalDepthTextures.size() > 0)
            return m_aAdditionalDepthTextures.front();
        return nullptr;
    }
    void DeferredRenderPass::Init(Gfx* gfx)
    {
        m_GBuffers = Resources::Instance()->Textures().Resolve<DeferredRenderTargetMap>({ DeferredRenderTargetMap::LOOKUP_NAME});
        m_DeferedLightingMat = Resources::Instance()->Materials().Resolve<DeferredLightingMaterial>({"ComputeLightingMat"});
    }
    void DeferredRenderPass::Render(const RenderContext& ctx)
    {
        const float ClearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        static Diligent::ITextureView* L_RTVs[NUM_G_BUFFERS] = { };
        for (uint32 i = 0; i < NUM_G_BUFFERS; i++)
        {
            L_RTVs[i] = m_GBuffers->m_aDeferredTextures[i].RTV();
        }

        ctx.Gfx->UseContext()->SetRenderTargets(NUM_G_BUFFERS, L_RTVs, ctx.DepthStencilView->DSV(), Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

        for (uint32 i = 0; i < NUM_G_BUFFERS; i++)
        {
            auto* rtv = m_GBuffers->m_aDeferredTextures[i].RTV();
            ctx.Gfx->UseContext()->ClearRenderTarget(rtv, ClearColor, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        }

        auto& groups = ctx.Scene->GetRenderGroups();
        for (auto* grp : groups)
        {
            if (grp->m_RenderFlags.IsSet(ERenderFLag::ERF_DEFERRED_OPAQUE))
                grp->Render(ctx);
        }
        ctx.Gfx->UseContext()->SetRenderTargets(1, &ctx.RenderTarget->m_RTV, ctx.DepthStencilView->DSV(), Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        m_DeferedLightingMat->Bind(ctx);
        Diligent::DrawAttribs DrawAttrs;
        DrawAttrs.NumVertices = 4;
        DrawAttrs.Flags = Diligent::DRAW_FLAG_VERIFY_ALL;
        ctx.Gfx->UseContext()->Draw(DrawAttrs);
    }
}
