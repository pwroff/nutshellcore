#pragma once

#include "Graphics/Gfx.h"
#include "Graphics/Mesh/Mesh.h"
#include "Graphics/Material/Material.h"
#include "Core/Objects.h"

namespace nshl
{
    struct RenderGroup;
    class IScene;
    static constexpr uint32 MAX_INSTANCES_PER_GROUP = 1024;

    struct SceneObject : public ICoreObject
    {
    protected:
        uint64 m_ID = InvalidID<uint64>();
        uint64 m_Parent = InvalidID<uint64>();
        virtual void ObjectDidUpdate() {}
        virtual void FlagsDidUpdate() {}

        
    public:
        Transform m_Transform;
        
        uint64 GetID() const { return m_ID; }
        const Transform& GetTransform() const { return m_Transform; }
        Transform& UseTransform() { return m_Transform; }

        void Rotate(const float3x3& rotation);
        void SetOrieantation(const float3x3& orientation);
        void SetRotation(const float3& euler);
        void Translate(const float3& translation);
        void SetPosition(const float3& position);
        void SetTransform(const Transform& transform);
        void SetScale(const float3& scale);
        void SetLookDirection(const float3& forward, const float3& up);
        void AddChild(SceneObject* child);
    };

    struct Camera: public SceneObject
    {
        float4x4 m_Projection;
        float4x4 m_View;

        float m_FOV = 60.0f;
        float m_NearPlane = 0.1f;
        float m_FarPLane = 500.0f;
        bool m_bPerspectiveCamera = true;


        void UpdateProjection(uint32 Width, uint32 Height)
        {
            if (m_bPerspectiveCamera)
                m_Projection = Projection(Width, Height, m_FOV * DEG2RAD, m_NearPlane, m_FarPLane, false);
            else
                m_Projection = Orthographic(Width, Height, m_NearPlane, m_FarPLane, false);
        }

        void UpdateView()
        {
            m_View = m_Transform.AsMatrix().Inverse();
        }

        void ObjectDidUpdate() override
        {
            UpdateView();
        }

        virtual float4x4 GetWorldToScreenMatrix() const { return m_View * m_Projection; }
    };

    struct Renderable : public SceneObject
    {
    private:
        friend RenderGroup;
        friend IScene;
        RenderGroup* m_Group = nullptr;
        Bitfield<uint32> m_RenderFlags;// = { ERenderFLag::ERF_CASTS_SHADOW };
        Array<Bone> m_Nodes;
        Array<float4x4> m_NodesLocal;
    public:
        static Renderable* LoadFromFile(const std::string& path);

        void Render(const RenderContext& ctx, IMesh* mesh, IMaterial* material);

        void SetRenderFlags(uint32 flags);
        void ClearRenderFlags(uint32 flags);
        void InitNodes(const IRigDefs* rigDefs);
        void SetNodeKeys(const Array<AnimationData::TransformKey>& keys);
        Array<float4x3> ComputeBonesMS(const Array<float4x4>& modelInverse) const;
    };
    struct LightDef;

    enum class ELightType: uint32
    {
        UNKNOWN = 0,
        POINT = 1,
        SPOT = 2,
        DIRECTIONAL = 3
    };

    struct Light : public Camera
    {
        float3 Color = {1.0f, 1.0f, 1.0f}; // color
        float FalloffStart = 0.0f; // point/spot light only
        float FalloffEnd = 1.0f;   // point/spot light only
        float Power = 1.0f;
        float Bias = 0.03f;
        ELightType Type = ELightType::POINT;
        uint32 ShadowMapIndex = 0u;
        uint32 TotalShadowMaps = 1u;
        LightDef GetLightDefs() const;
        void SetType(ELightType type) {
            Type = type;
            m_FOV = 130.0f;
            m_FarPLane = 50.0f;
            m_NearPlane = 2.0f;
        }
        void UpdateProjectionMatrix();
    };

    struct RenderGroup : public ICoreObject
    {
        static IdGen IdGen;
        uint32 MAX_ITEMS = MAX_INSTANCES_PER_GROUP;
        Array<uint64> m_Renderables;
        RefPtr<IBuffer> m_InstanceBuffer;
        ICBuff* m_SkeletonesBuffer = nullptr;
        IScene* m_Scene = nullptr;
        IMesh* m_Mesh = nullptr;
        IMaterial* m_Material = nullptr;
        Bitfield<uint32> m_RenderFlags;

        void AddRenderable(Renderable& rend);
        void RemoveRenderable(Renderable& rend);
        void Init(Gfx* gfx, IMesh* mesh, IMaterial* material, Bitfield<uint32> flags);
        void UpdateBuffer(Gfx* gfx, IScene* scene);
        void Render(const RenderContext& ctx);

        static RenderGroup* FindRenderGroup(IMesh* mesh, IMaterial* material, IScene* scene, Bitfield<uint32> flags = {});
        static void UpdateRenderGroup(Renderable& rend);
    };
}

