#include "Graphics/Renderable.h"
#include "Resources/Resources.h"
#include "Graphics/Material/ConstantBuffers.h"
#include "Scene/Scene.h"
#include "Renderable.h"
namespace nshl
{
    void Renderable::Render(const RenderContext& ctx, IMesh* mesh, IMaterial* material)
    {
        // Bind vertex and index buffers
        uint32   offset = 0;
        IBuffer* pBuffs[] = { mesh->UseVertexBuffer() };
        ctx.Gfx->UseContext()->SetVertexBuffers(0, 1, pBuffs, &offset, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION, Diligent::SET_VERTEX_BUFFERS_FLAG_RESET);
        ctx.Gfx->UseContext()->SetIndexBuffer(mesh->UseIndexBuffer(), 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        material->Bind(ctx);
        Diligent::DrawIndexedAttribs DrawAttrs;     // This is an indexed draw call
        DrawAttrs.IndexType = Diligent::VT_UINT32; // Index type
        DrawAttrs.NumIndices = mesh->GetNumIndices();
        // Verify the state of vertex and index buffers
        DrawAttrs.Flags = Diligent::DRAW_FLAG_VERIFY_ALL;
        ctx.Gfx->UseContext()->DrawIndexed(DrawAttrs);
    }
    Array<float4x3> Renderable::ComputeBonesMS(const Array<float4x4>& modelInverse) const
    {
        Array<float4x4> lsBones;
        Array<float4x4> msBones;
        lsBones.resize(m_Nodes.size());
        msBones.resize(m_Nodes.size());
        for (uint32 i = 0; i < lsBones.size(); i++)
        {
            lsBones[i] = m_NodesLocal[i];// *m_Nodes[i].AsMatrix();
        }
        for (uint32 i = 0; i < msBones.size(); i++)
        {
            msBones[i] = lsBones[i];
            if (i != 0)
            {
                msBones[i] = msBones[m_Nodes[i].parent] * msBones[i];
            }
        }
        Array<float4x3> outBones;
        outBones.resize(m_Nodes.size());
        for (uint32 i = 0; i < msBones.size(); i++)
        {
            outBones[i] = ToInstanceMatrix(msBones[i]/* * modelInverse[i]*/);
        }
        return outBones;
    }
    void Renderable::SetNodeKeys(const Array<AnimationData::TransformKey>& keys)
    {
        for (const auto& key : keys)
        {
            m_Nodes[key.boneIDX].position = key.Position;
            m_Nodes[key.boneIDX].rotation = key.Rotation;
        }
    }
    void Renderable::InitNodes(const IRigDefs* rigDefs)
    {
        if (!rigDefs)
            return;
        m_Nodes.resize(rigDefs->GetSkeletone().NumBones);
        m_NodesLocal.resize(rigDefs->GetSkeletone().NumBones);
        for (uint32 i = 0; i < rigDefs->GetSkeletone().NumBones; i++)
        {
            m_Nodes[i] = rigDefs->GetSkeletone().Bones[i];
            m_NodesLocal[i] = m_Nodes[i].AsMatrix();
        }
    }
    void SceneObject::Rotate(const float3x3& rotation)
    {
        m_Transform.Orientation *= rotation;
        ObjectDidUpdate();
    }
    void SceneObject::SetOrieantation(const float3x3& orientation)
    {
        m_Transform.Orientation = orientation;
        ObjectDidUpdate();
    }
    void SceneObject::SetRotation(const float3& euler)
    {
        m_Transform.SetRotation(euler);
        ObjectDidUpdate();
    }
    void SceneObject::Translate(const float3& translation)
    {
        m_Transform.Position += translation;
        ObjectDidUpdate();
    }
    void SceneObject::SetPosition(const float3& position)
    {
        m_Transform.Position = position;
        ObjectDidUpdate();
    }
    void SceneObject::SetTransform(const Transform& transform)
    {
        m_Transform = transform;
        ObjectDidUpdate();
    }
    void SceneObject::SetScale(const float3& scale)
    {
        m_Transform.Scale = scale;
        ObjectDidUpdate();
    }
    void SceneObject::SetLookDirection(const float3& forward, const float3& up)
    {
        m_Transform.SetLookDirection(forward, up);
        ObjectDidUpdate();
    }
    void SceneObject::AddChild(SceneObject* child)
    {
        child->m_Parent = GetID();
        child->m_Transform.Parent = child->m_Parent;
    }
    Renderable* Renderable::LoadFromFile(const std::string& path)
    {
        return nullptr;
    }
    void RenderGroup::AddRenderable(Renderable& rend)
    {
        if (rend.m_Group)
            rend.m_Group->RemoveRenderable(rend);
        m_Renderables.push_back(rend.m_ID);
        rend.m_Group = this;
    }
    void RenderGroup::RemoveRenderable(Renderable& rend)
    {
        rend.m_Group = nullptr;
        m_Renderables.Remove(rend.m_ID);
    }
    void RenderGroup::Init(Gfx* gfx, IMesh* mesh, IMaterial* material, Bitfield<uint32> flags)
    {
        // Create instance data buffer that will store transformation matrices
        Diligent::BufferDesc InstBuffDesc;
        InstBuffDesc.Name = "Instance data buffer";
        InstBuffDesc.Usage = Diligent::USAGE_DEFAULT;
        InstBuffDesc.BindFlags = Diligent::BIND_VERTEX_BUFFER;
        MAX_ITEMS = m_RenderFlags.IsSet(ERenderFLag::ERF_HAS_RIG) ? MAX_SKELETONES_PER_DRAW_CALL : MAX_INSTANCES_PER_GROUP;
        InstBuffDesc.uiSizeInBytes = sizeof(float4x3) * MAX_ITEMS + sizeof(int) * MAX_ITEMS;
        gfx->UseDevice()->CreateBuffer(InstBuffDesc, nullptr, &m_InstanceBuffer);
        m_Mesh = mesh;
        m_Material = material;
        m_Renderables.reserve(MAX_ITEMS);
        m_RenderFlags = flags;

        if (m_RenderFlags.IsSet(ERenderFLag::ERF_HAS_RIG))
        {
            m_SkeletonesBuffer = Resources::Instance()->Buffers().Resolve<SkeletoneCB>();
        }
    }
    void RenderGroup::UpdateBuffer(Gfx* gfx, IScene* scene)
    {
        struct InstanceData {
            float4x3 t;
            int batchID = -1;
        };
        Array<InstanceData> InstanceData;
        InstanceData.reserve(m_Renderables.size());
        for (uint32 i = 0; i < m_Renderables.size(); i++)
        {
            const auto& rendID = m_Renderables[i];
            const auto* renderable = scene->GetRenderable(rendID);
            const auto& trInWs = scene->ToWorld(renderable->m_Transform);
            InstanceData.push_back({ Transform::AsTransformMatrix(trInWs), int(i)});
        }
        // Update instance data buffer
        uint32 DataSize = static_cast<uint32>(sizeof(InstanceData[0]) * InstanceData.size());
        gfx->UseContext()->UpdateBuffer(m_InstanceBuffer, 0, DataSize, InstanceData.data(), Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);

        if (m_RenderFlags.IsSet(ERenderFLag::ERF_HAS_RIG))
        {
            RendSkeletone skeletone;
            for (uint32 i = 0; i < m_Renderables.size(); i++)
            {
                const auto& rendID = m_Renderables[i];
                const auto* renderable = scene->GetRenderable(rendID);
                Array<float4x3> rendBones = renderable->ComputeBonesMS(m_Mesh->GetRig()->GetModelInverse());
                memcpy(&skeletone.skeletones[i], rendBones.data(), sizeof(float4x4)* rendBones.size());
            }
            Diligent::MapHelper<RendSkeletone> CBConstants(gfx->UseContext(), m_SkeletonesBuffer->m_pBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
            *CBConstants = skeletone;
        }
    }
    void RenderGroup::Render(const RenderContext& ctx)
    {
        const uint32 numInstances = uint32(m_Renderables.size());
        if (numInstances == 0)
            return;
        UpdateBuffer(ctx.Gfx, ctx.Scene);
        m_Mesh->DrawIndexed(ctx, m_Material, m_InstanceBuffer, numInstances);
    }

    RenderGroup* RenderGroup::FindRenderGroup(IMesh* mesh, IMaterial* material, IScene* scene, Bitfield<uint32> flags)
    {
        nshl_assert(mesh);
        nshl_assert(material);
        auto& groups = Resources::Instance()->RenderGroups();
        for (const auto& ent : groups)
        {
            if (ent.second->m_Mesh == mesh && ent.second->m_Material == material &&
                ent.second->m_RenderFlags.Value == flags.Value &&
                uint32(ent.second->m_Renderables.size()) < MAX_INSTANCES_PER_GROUP)
            {
                return ent.second.get();
            }
        }
        auto* ng = groups.Insert<RenderGroup>({IdGen.Next()}, mesh, material, flags);
        scene->OnRenderGroupInitialized(ng);
        return ng;
    }

    void RenderGroup::UpdateRenderGroup(Renderable& rend)
    {
        auto* mesh = rend.m_Group->m_Mesh;
        auto* material = rend.m_Group->m_Material;
        auto flags = rend.m_Group->m_RenderFlags;
        auto* prevGroup = rend.m_Group;
        rend.m_Group->RemoveRenderable(rend);
        auto* group = FindRenderGroup(mesh, material, prevGroup->m_Scene, flags);
        group->AddRenderable(rend);
    }

    IdGen RenderGroup::IdGen = {};

    LightDef Light::GetLightDefs() const
    {
        LightDef def;
        def.Color = Color;
        def.FalloffStart = FalloffStart;
        def.FalloffEnd = FalloffEnd;
        def.Position = m_Transform.Position;
        def.Direction = m_Transform.Forward();
        def.SpotPower = Power;
        def.LightCustomProps = { Bias , 0.0f, 0.0f, 0.0f };
        def.Properties[0] = uint32(Type);
        def.Properties[1] = ShadowMapIndex;
        def.Properties[2] = TotalShadowMaps;
        def.Properties[3] = 0;
        return def;
    }

    void Light::UpdateProjectionMatrix()
    {
        const uint32 size = static_cast<uint32>(FalloffEnd);
        if (Type == ELightType::DIRECTIONAL)
            m_Projection = Orthographic(size, size, m_NearPlane, m_FarPLane, false);
        else
            m_Projection = Projection(size, size, m_FOV * DEG2RAD, m_NearPlane, m_FarPLane, false);
        
        // 
    }

    void Renderable::SetRenderFlags(uint32 flags)
    {
        auto prev = m_RenderFlags;
        m_RenderFlags.Set(flags);
        if (prev.Value != m_RenderFlags.Value && m_Group)
            RenderGroup::UpdateRenderGroup(*this);
    }
    void Renderable::ClearRenderFlags(uint32 flags)
    {
        auto prev = m_RenderFlags;
        m_RenderFlags.Clear(flags);
        if (prev.Value != m_RenderFlags.Value && m_Group)
            RenderGroup::UpdateRenderGroup(*this);
    }
}