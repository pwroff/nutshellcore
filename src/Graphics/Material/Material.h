#pragma once
#include "Shader.h"
#include "Resources/Resources.h"

namespace nshl
{
	class Gfx;
	class ITexture;
	struct IMaterial : public ICoreObject
	{
		virtual void Init(Gfx* gfx) {}
		virtual void Bind(const RenderContext& ctx) {}
		virtual IShaderBase* UseShader() { return nullptr; }
		void SetTexture(const char* name, ITexture* texture);
		void SetTexture(const char* name, Diligent::ITextureView* textureView);
	
		RefPtr<Diligent::IShaderResourceBinding> m_SRB;

		virtual uint32 GetFlags() const { return 0u; }
	protected:
		void CreateSRB();
	};

	template<typename Shader, uint32 RenderFlags = 0>
	struct BaseMaterial: public IMaterial
	{
		typename Shader::MaterialConstants m_Params;
		
		uint32 GetFlags() const override { return RenderFlags; }

		void Init(Gfx* gfx) override
		{
			m_pShader = Resources::Instance()->Shaders().Resolve<Shader>();//gfx->ResolveShader<Shader>(Shader::TypeID);
			CreateSRB();
		}
		void Bind(const RenderContext& ctx) override
		{
			m_pShader->UpdateMaterialBuffer(ctx.Gfx, m_Params);
			m_pShader->Bind(ctx, m_SRB);
		}
		IShaderBase* UseShader() override { return m_pShader; }
	protected:
		Shader* m_pShader;
		
	};

	struct UnlitMaterial : public BaseMaterial<UnlitShader>
	{
	};
	struct WireframeMaterial : public BaseMaterial<WireframeShader>
	{
	};
	struct DepthMaterial : public BaseMaterial<DepthDisplayShader>
	{
	};
	struct TestPPMaterial : public BaseMaterial<PostProcessShader>
	{
	};

}
