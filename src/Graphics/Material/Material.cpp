#include "Material.h"
#include "Graphics/Gfx.h"
#include "Graphics/Texture/Texture.h"
#include "Core/Logger.h"

namespace nshl
{
	void IMaterial::SetTexture(const char* name, ITexture* texture)
	{
		SetTexture(name, texture->SRV());
	}
	void IMaterial::SetTexture(const char* name, Diligent::ITextureView* textureView)
	{
		if (m_SRB)
		{
			if (auto* var = m_SRB->GetVariableByName(Diligent::SHADER_TYPE_PIXEL, name))
			{
				var->Set(textureView);
			}
			else
			{
				LOG_S("Variable %s is not present in shader", name);
			}
		}
		else
		{
			nshl_assert2(false, "SRB is not created");
		}
	}
	void IMaterial::CreateSRB()
	{
		UseShader()->UsePipeline()->CreateShaderResourceBinding(&m_SRB, true);
	}
}