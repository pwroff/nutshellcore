#pragma once
#include "Material.h"
#include "Core/core.h"

namespace nshl
{
	class PBRShader : public ShaderBase<PBRShader, EShaderStages::eSS_COMMON>
	{
		RefPtr<IBuffer> m_pMaterialConstantBuffer;

	public:
		struct MaterialConstants
		{
			float4 DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
			float SpecularShine = 0.0f;
			float SpecularHardness = 1.0f;
			float Metalic = 0;
			float Else = 0;
			float2 UVSCale = {1.0f, 1.0f};
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst);
	};

	class DeferredDrawShader : public ShaderBase<DeferredDrawShader, EShaderStages::eSS_COMMON>
	{
		RefPtr<IBuffer> m_pMaterialConstantBuffer;

	public:
		struct MaterialConstants
		{
			float4 DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
			float SpecularShine = 0.0f;
			float SpecularHardness = 1.0f;
			float Metalic = 0;
			float Else = 0;
			float2 UVSCale = { 1.0f, 1.0f };
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst);
	};

	class DeferredLightingShader : public ShaderBase<DeferredLightingShader, EShaderStages::eSS_COMMON>
	{
		RefPtr<IBuffer> m_pMaterialConstantBuffer;

	public:
		struct MaterialConstants
		{
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst);
	};

	struct PBRMaterial : public BaseMaterial<PBRShader>
	{
	};

	struct DeferredDrawMaterial : public BaseMaterial<DeferredDrawShader, ERenderFLag::ERF_DEFERRED_OPAQUE>
	{
	};

	struct DeferredLightingMaterial : public BaseMaterial<DeferredLightingShader>
	{
	};

	class DepthRendShader : public ShaderBase<DepthRendShader, EShaderStages::eSS_VERTEX>
	{
	public:
		struct MaterialConstants
		{
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst) {}
	};

	struct DepthRendMaterial : public BaseMaterial<DepthRendShader>
	{
	};
}
