#pragma once

#include "Core/core.h"
#include "Math/math.h"
#include "Core/Objects.h"
#include "Graphics/GraphicsTools/interface/MapHelper.hpp"
#include "Graphics/Mesh/Mesh.h"

namespace nshl
{
	class Gfx;

	struct CameraConstants
	{
		float4x4 View;
		float4x4 Proj;
		float4x4 ViewProj;
		float4x4 InvView;
		float4x4 InvViewProj;
		float4 ViewDirection;
		float4 CameraProperties;
		float4 ViewportSize;
	};

	struct LightDef
	{
		float3 Color; // color
		float FalloffStart; // point/spot light only
		float3 Direction;   // directional/spot light only
		float FalloffEnd;   // point/spot light only
		float3 Position;    // point light only
		float SpotPower;    // spot light only
		float4 LightCustomProps = { -0.03f, 0.0f, 0.0f, 0.0f };
		uint32 Properties[4] = {0, 0, 0, 0}; // light type etc and extra;
		
	};

	struct LightConstants
	{
		LightDef lights[MAX_LIGHTS];
		float4x4 CasterViewProjections[MAX_SHADOW_MAPS];
	};

	struct ICBuff : public ICoreObject, public WithIDGen<ICBuff>
	{
		RefPtr<IBuffer> m_pBuffer;

		virtual void Init(Gfx* gfx) = 0;
		virtual void Update(const RenderContext& ctx) = 0;
	};

	template<typename B>
	struct ConstBaseBuffer : public WithTypeID<B, ICBuff>
	{
		void Init(Gfx* gfx) override
		{
			Diligent::BufferDesc CBDesc;

			CBDesc.Usage = Diligent::USAGE::USAGE_DYNAMIC;
			CBDesc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
			CBDesc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

			CBDesc.Name = TypeName;
			CBDesc.uiSizeInBytes = sizeof(B);

			gfx->UseDevice()->CreateBuffer(CBDesc, nullptr, &m_pBuffer);
		}

		void Update(const RenderContext& ctx) override
		{
			Diligent::MapHelper<B> CBConstants(ctx.Gfx->UseContext(), m_pBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
			*CBConstants = GetRelevantData(ctx);
		}

		virtual B GetRelevantData(const RenderContext& ctx) const { return {}; }
	};

	struct CameraCB : public ConstBaseBuffer<CameraConstants>
	{
		CameraConstants GetRelevantData(const RenderContext& ctx) const override;
	};

	struct LightCB : public ConstBaseBuffer<LightConstants>
	{
		LightConstants GetRelevantData(const RenderContext& ctx) const override;
	};

	struct SkeletoneCB : public ConstBaseBuffer<RendSkeletone>
	{

	};
}