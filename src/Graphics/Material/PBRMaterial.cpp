#include "PBRMaterial.h"
#include "Graphics/Gfx.h"
#include "Resources/Resources.h"
#include "Graphics/Texture/Texture.h"
#include "Graphics/GraphicsTools/interface/MapHelper.hpp"
#include "Graphics/RenderPass/RenderPass.h"

namespace nshl
{
	void PBRShader::Init(Gfx* gfx)
	{
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "PBR";

        const char* sourcePath = "shaders/pbr_lit.shader";

        SetDefaultLayout(PSOCreateInfo);
        
        // Shader variables should typically be mutable, which means they are expected
       // to change on a per-instance basis
        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        // clang-format off
        // Define immutable sampler for g_Texture. Immutable samplers should be used whenever possible
        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };

        Diligent::SamplerDesc SamBorderDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_CLAMP
        };
        SamBorderDesc.ComparisonFunc = Diligent::COMPARISON_FUNC_LESS;
        SamBorderDesc.MinFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MagFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MipFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.BorderColor[0] = 1.0f;
        SamBorderDesc.BorderColor[1] = 1.0f;
        SamBorderDesc.BorderColor[2] = 1.0f;
        SamBorderDesc.BorderColor[3] = 1.0f;

        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", SamLinearClampDesc},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", SamBorderDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);
        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
        {
            Diligent::BufferDesc CBDesc;

            CBDesc.Usage = Diligent::USAGE::USAGE_DYNAMIC;
            CBDesc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
            CBDesc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

            CBDesc.Name = "PBR MaterialConstants";
            CBDesc.uiSizeInBytes = sizeof(PBRShader::MaterialConstants);

            gfx->UseDevice()->CreateBuffer(CBDesc, nullptr, &m_pMaterialConstantBuffer);
            SetConstantBuffer("MaterialConstants", m_pMaterialConstantBuffer);
        }
        ShadowMap* map = Resources::Instance()->Textures().Resolve<ShadowMap>({ "ShadowMap" });
        Diligent::IDeviceObject* srvs[MAX_SHADOW_MAPS];
        for (uint32 i = 0; i < MAX_SHADOW_MAPS; i++)
        {
            srvs[i] = map->m_aDepthTextures[i].SRV();
        }
        if (auto* varArr = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap"))
            varArr->SetArray(srvs, 0, MAX_SHADOW_MAPS);
	}
    void PBRShader::UpdateMaterialBuffer(Gfx* gfx, const PBRShader::MaterialConstants& matConst)
    {
        Diligent::MapHelper<PBRShader::MaterialConstants> CBConstants(gfx->UseContext(), m_pMaterialConstantBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
        *CBConstants = matConst;
    }
    void DeferredDrawShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);

        PSOCreateInfo.GraphicsPipeline.NumRenderTargets = DeferredRenderPass::NUM_G_BUFFERS;
        for (uint32 i = 0; i < DeferredRenderPass::NUM_G_BUFFERS; i++)
        {
            PSOCreateInfo.GraphicsPipeline.RTVFormats[i] = DeferredRenderPass::G_BUFFER_FORMATS[i];
        }

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "defered draw PBR";

        const char* sourcePath = "shaders/defered_draw.shader";

        SetDefaultLayout(PSOCreateInfo);

        // Shader variables should typically be mutable, which means they are expected
       // to change on a per-instance basis
        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        // clang-format off
        // Define immutable sampler for g_Texture. Immutable samplers should be used whenever possible
        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };

        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", SamLinearClampDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);
        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
        {
            Diligent::BufferDesc CBDesc;

            CBDesc.Usage = Diligent::USAGE::USAGE_DYNAMIC;
            CBDesc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
            CBDesc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

            CBDesc.Name = "Defered MaterialConstants";
            CBDesc.uiSizeInBytes = sizeof(DeferredDrawShader::MaterialConstants);

            gfx->UseDevice()->CreateBuffer(CBDesc, nullptr, &m_pMaterialConstantBuffer);
            SetConstantBuffer("MaterialConstants", m_pMaterialConstantBuffer);
        }
    }
    void DeferredDrawShader::UpdateMaterialBuffer(Gfx* gfx, const DeferredDrawShader::MaterialConstants& matConst)
    {
        Diligent::MapHelper<DeferredDrawShader::MaterialConstants> CBConstants(gfx->UseContext(), m_pMaterialConstantBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
        *CBConstants = matConst;
    }
    void DeferredLightingShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);

        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::False;
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_NONE;
        PSOCreateInfo.GraphicsPipeline.PrimitiveTopology = Diligent::PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "PBR Deferred";

        const char* sourcePath = "shaders/defered_compute_lighting.shader";

        SetDefaultLayout(PSOCreateInfo);

        // Shader variables should typically be mutable, which means they are expected
       // to change on a per-instance basis
        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_GBuffers", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        // clang-format off
        // Define immutable sampler for g_Texture. Immutable samplers should be used whenever possible
        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };

        Diligent::SamplerDesc SamBorderDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_CLAMP
        };
        SamBorderDesc.ComparisonFunc = Diligent::COMPARISON_FUNC_LESS;
        SamBorderDesc.MinFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MagFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MipFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.BorderColor[0] = 1.0f;
        SamBorderDesc.BorderColor[1] = 1.0f;
        SamBorderDesc.BorderColor[2] = 1.0f;
        SamBorderDesc.BorderColor[3] = 1.0f;

        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_GBuffers", SamLinearClampDesc},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", SamBorderDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);
        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
        {
            Diligent::BufferDesc CBDesc;

            CBDesc.Usage = Diligent::USAGE::USAGE_DYNAMIC;
            CBDesc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
            CBDesc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

            CBDesc.Name = "PBR MaterialConstants";
            CBDesc.uiSizeInBytes = sizeof(DeferredLightingShader::MaterialConstants);

            gfx->UseDevice()->CreateBuffer(CBDesc, nullptr, &m_pMaterialConstantBuffer);
            SetConstantBuffer("MaterialConstants", m_pMaterialConstantBuffer);
        }
        // Bind G-Buffers
        {
            DeferredRenderTargetMap* map = Resources::Instance()->Textures().Resolve<DeferredRenderTargetMap>({ DeferredRenderTargetMap::LOOKUP_NAME });
            Diligent::IDeviceObject* srvs[DeferredRenderPass::NUM_G_BUFFERS];
            for (uint32 i = 0; i < DeferredRenderPass::NUM_G_BUFFERS; i++)
            {
                srvs[i] = map->m_aDeferredTextures[i].SRV();
            }
            if (auto* varArr = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_PIXEL, "Tex_GBuffers"))
                varArr->SetArray(srvs, 0, DeferredRenderPass::NUM_G_BUFFERS);
        }
        // Bind shadow map
        {
            ShadowMap* map = Resources::Instance()->Textures().Resolve<ShadowMap>({ "ShadowMap" });
            Diligent::IDeviceObject* srvs[MAX_SHADOW_MAPS];
            for (uint32 i = 0; i < MAX_SHADOW_MAPS; i++)
            {
                srvs[i] = map->m_aDepthTextures[i].SRV();
            }
            if (auto* varArr = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap"))
                varArr->SetArray(srvs, 0, MAX_SHADOW_MAPS);
        }
    }
    void DeferredLightingShader::UpdateMaterialBuffer(Gfx* gfx, const DeferredLightingShader::MaterialConstants& matConst)
    {
        Diligent::MapHelper<DeferredLightingShader::MaterialConstants> CBConstants(gfx->UseContext(), m_pMaterialConstantBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
        *CBConstants = matConst;
    }
    void DepthRendShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);
        // This is a graphics pipeline
        PSOCreateInfo.PSODesc.PipelineType = Diligent::PIPELINE_TYPE_GRAPHICS;

        // clang-format off
        // Shadow pass doesn't use any render target outputs
        PSOCreateInfo.GraphicsPipeline.NumRenderTargets = 0;
        PSOCreateInfo.GraphicsPipeline.RTVFormats[0] = Diligent::TEX_FORMAT_UNKNOWN;
        // The DSV format is the shadow map format
        PSOCreateInfo.GraphicsPipeline.DSVFormat = SHADOW_MAP_FORMAT;
        PSOCreateInfo.GraphicsPipeline.PrimitiveTopology = Diligent::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        // Cull back faces
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_BACK;
        // Enable depth testing
        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::True;
        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "DepthRender";
        const char* sourcePath = "shaders/depth_render.shader";

        SetDefaultLayout(PSOCreateInfo);

        if (gfx->UseDevice()->GetDeviceCaps().Features.DepthClamp)
        {
            //PSOCreateInfo.GraphicsPipeline.RasterizerDesc.DepthClipEnable = Diligent::False;
        }

        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
    }
}