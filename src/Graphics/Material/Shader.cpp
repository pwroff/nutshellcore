#include "Shader.h"
#include "Graphics/Gfx.h"
#include "Resources/Resources.h"
#include "Graphics/Texture/Texture.h"

#include "ConstantBuffers.h"

namespace nshl
{

    Diligent::GraphicsPipelineStateCreateInfo GetCommonPSOCreateInfo(Gfx* ctx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo;

        // This is a graphics pipeline
        PSOCreateInfo.PSODesc.PipelineType = Diligent::PIPELINE_TYPE_GRAPHICS;
        // clang-format off
        // This tutorial will render to a single render target
        PSOCreateInfo.GraphicsPipeline.NumRenderTargets = 1;
        // Set render target format which is the format of the swap chain's color buffer
        PSOCreateInfo.GraphicsPipeline.RTVFormats[0] = ctx->GetSwapChain()->GetDesc().ColorBufferFormat;
        // Set depth buffer format which is the format of the swap chain's back buffer
        PSOCreateInfo.GraphicsPipeline.DSVFormat = ctx->GetSwapChain()->GetDesc().DepthBufferFormat;
        // Primitive topology defines what kind of primitives will be rendered by this pipeline state
        PSOCreateInfo.GraphicsPipeline.PrimitiveTopology = Diligent::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        // Cull back faces
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_BACK;
        // Enable depth testing
        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::True;


        // Define variable type that will be used by default
        PSOCreateInfo.PSODesc.ResourceLayout.DefaultVariableType = Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC;

        return PSOCreateInfo;
    }
    void UnlitShader::Init(Gfx* gfx)
	{
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "Unlit";

        const char* sourcePath = "shaders/unlit_default.shader";

        SetDefaultLayout(PSOCreateInfo);
        // Shader variables should typically be mutable, which means they are expected
       // to change on a per-instance basis
        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "g_Diffuse", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        // clang-format off
        // Define immutable sampler for g_Texture. Immutable samplers should be used whenever possible
        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };
        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "g_Diffuse", SamLinearClampDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);
        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
        {
            Diligent::BufferDesc CBDesc;

            CBDesc.Usage = Diligent::USAGE::USAGE_DYNAMIC;
            CBDesc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
            CBDesc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

            CBDesc.Name = "Unlit MaterialConstants";
            CBDesc.uiSizeInBytes = sizeof(UnlitShader::MaterialConstants);

            gfx->UseDevice()->CreateBuffer(CBDesc, nullptr, &m_pMaterialConstantBuffer);
            SetConstantBuffer("MaterialConstants", m_pMaterialConstantBuffer);
        }
	}

    void UnlitShader::UpdateMaterialBuffer(Gfx* gfx, const UnlitShader::MaterialConstants& matConst)
    {
        Diligent::MapHelper<UnlitShader::MaterialConstants> CBConstants(gfx->UseContext(), m_pMaterialConstantBuffer, Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD);
        *CBConstants = matConst;
    }

    void IShaderBase::SetConstantBuffer(const char* varName, IBuffer* buffer)
    {
        auto* m_Pipeline = UsePipeline();
        auto stages = GetShaderStages();
        if (stages.IsSet(EShaderStages::eSS_PIXEL))
            if (auto* pixelCB = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_PIXEL, varName))
                pixelCB->Set(buffer);
        if (stages.IsSet(EShaderStages::eSS_GEOMETRY))
            if (auto* geomCB = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_GEOMETRY, varName))
                geomCB->Set(buffer);
        if (stages.IsSet(EShaderStages::eSS_VERTEX))
            if (auto* vertCB = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_VERTEX, varName))
                vertCB->Set(buffer);
    }

    void IShaderBase::SetCommonCBS(Gfx* gfx)
    {
        auto* cambuff = Resources::Instance()->Buffers().Resolve<CameraCB>();
        SetConstantBuffer("CameraConstants", cambuff->m_pBuffer);
        auto* lightsbuff = Resources::Instance()->Buffers().Resolve<LightCB>();
        SetConstantBuffer("LightConstants", lightsbuff->m_pBuffer);
        auto* skeletonesBuff = Resources::Instance()->Buffers().Resolve<SkeletoneCB>();
        SetConstantBuffer("RenderedSkeletones", skeletonesBuff->m_pBuffer);
    }

    void IShaderBase::SetDefaultLayout(Diligent::GraphicsPipelineStateCreateInfo& PSOCreateInfo)
    {
        uint32 i = 0;

        // Define vertex shader input layout
        static const Diligent::LayoutElement LayoutElems[] =
        {
            // Attribute 0 - vertex position
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_FLOAT32, Diligent::False},
           // Attribute 1 - vertex normal
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_FLOAT32, Diligent::False},
           // Attribute 2 - tangent
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_FLOAT32, Diligent::False},
           // Attribute 3 - bitangent
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_FLOAT32, Diligent::False},
           // Attribute 4 - joint weights
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_FLOAT32, Diligent::False},
           // Attribute 5 - joint ids
           Diligent::LayoutElement{i++, 0, 3, Diligent::VT_INT32, Diligent::False},
           // Attribute 6 - texture coordinates
           Diligent::LayoutElement{i++, 0, 2, Diligent::VT_FLOAT32, Diligent::False},

           // Instance tranformation 4x3 mat attributes
           Diligent::LayoutElement{i++, 1, 3, Diligent::VT_FLOAT32, Diligent::False, Diligent::INPUT_ELEMENT_FREQUENCY_PER_INSTANCE},
           Diligent::LayoutElement{i++, 1, 3, Diligent::VT_FLOAT32, Diligent::False, Diligent::INPUT_ELEMENT_FREQUENCY_PER_INSTANCE},
           Diligent::LayoutElement{i++, 1, 3, Diligent::VT_FLOAT32, Diligent::False, Diligent::INPUT_ELEMENT_FREQUENCY_PER_INSTANCE},
           Diligent::LayoutElement{i++, 1, 3, Diligent::VT_FLOAT32, Diligent::False, Diligent::INPUT_ELEMENT_FREQUENCY_PER_INSTANCE},
           Diligent::LayoutElement{i++, 1, 1, Diligent::VT_INT32, Diligent::False, Diligent::INPUT_ELEMENT_FREQUENCY_PER_INSTANCE}
        };

        PSOCreateInfo.GraphicsPipeline.InputLayout.LayoutElements = LayoutElems;
        PSOCreateInfo.GraphicsPipeline.InputLayout.NumElements = _countof(LayoutElems);
    }

    bool IShaderBase::CreateShadersFromFile(Gfx* gfx, const char* sourcePath, Diligent::GraphicsPipelineStateCreateInfo& PSOCreateInfo)
    {
        Diligent::ShaderCreateInfo ShaderCI;
        ShaderCI.SourceLanguage = Diligent::SHADER_SOURCE_LANGUAGE_HLSL;
        ShaderCI.UseCombinedTextureSamplers = true;
        ShaderCI.pShaderSourceStreamFactory = gfx->UseShaderFactory();

        // Create a vertex shader
        RefPtr<Diligent::IShader> pVS;
        // Create a pixel shader
        RefPtr<Diligent::IShader> pPS;
        // Create a geom shader
        RefPtr<Diligent::IShader> pGS;

        const auto stages = GetShaderStages();
        if (stages.IsSet(EShaderStages::eSS_VERTEX))
        {
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_VERTEX;
            ShaderCI.EntryPoint = "vert";
            ShaderCI.Desc.Name = sourcePath;
            ShaderCI.FilePath = sourcePath;
            gfx->UseDevice()->CreateShader(ShaderCI, &pVS);
            PSOCreateInfo.pVS = pVS;
        }

        if (stages.IsSet(EShaderStages::eSS_PIXEL))
        {
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_PIXEL;
            ShaderCI.EntryPoint = "frag";
            ShaderCI.Desc.Name = sourcePath;
            ShaderCI.FilePath = sourcePath;
            gfx->UseDevice()->CreateShader(ShaderCI, &pPS);
            PSOCreateInfo.pPS = pPS;
        }

        if (stages.IsSet(EShaderStages::eSS_GEOMETRY))
        {
            
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_GEOMETRY;
            ShaderCI.EntryPoint = "geom";
            ShaderCI.Desc.Name = sourcePath;
            ShaderCI.FilePath = sourcePath;
            gfx->UseDevice()->CreateShader(ShaderCI, &pGS);
            PSOCreateInfo.pGS = pGS;
        }
        gfx->UseDevice()->CreateGraphicsPipelineState(PSOCreateInfo, &m_Pipeline);
        const bool success = m_Pipeline != nullptr;
        nshl_assert2(success, "Failed to create pipeline");
        return success;
    }

    void WireframeShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_NONE;
        // Enable depth testing
        //PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::False;
        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthFunc = Diligent::COMPARISON_FUNCTION::COMPARISON_FUNC_LESS_EQUAL;

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "Wireframe";

        const char* sourcePath = "shaders/wireframe.shader";

        SetDefaultLayout(PSOCreateInfo);
        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
    }

    void IShaderBase::Bind(const RenderContext& ctx, Diligent::IShaderResourceBinding* srb)
    {
        // Set the pipeline state
        ctx.Gfx->UseContext()->SetPipelineState(m_Pipeline);
        // Commit shader resources. RESOURCE_STATE_TRANSITION_MODE_TRANSITION mode
        // makes sure that resources are transitioned to required states.
        ctx.Gfx->UseContext()->CommitShaderResources(srb, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
    }

    void DepthDisplayShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo = GetCommonPSOCreateInfo(gfx);
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_NONE;
        // Enable depth testing
        //PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::False;
        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthFunc = Diligent::COMPARISON_FUNCTION::COMPARISON_FUNC_LESS_EQUAL;

        // Pipeline state name is used by the engine to report issues.
        // It is always a good idea to give objects descriptive names.
        PSOCreateInfo.PSODesc.Name = "DepthDisp";

        const char* sourcePath = "shaders/depthdisplay.shader";

        SetDefaultLayout(PSOCreateInfo);

        // Shader variables should typically be mutable, which means they are expected
       // to change on a per-instance basis
        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        // clang-format off
        // Define immutable sampler for g_Texture. Immutable samplers should be used whenever possible
        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };
        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Diffuse", SamLinearClampDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);

        CreateShadersFromFile(gfx, sourcePath, PSOCreateInfo);
        SetCommonCBS(gfx);
    }

    void PostProcessShader::Init(Gfx* gfx)
    {
        Diligent::GraphicsPipelineStateCreateInfo PSOCreateInfo;

        // This is a graphics pipeline
        PSOCreateInfo.PSODesc.Name = "Post Process";
        PSOCreateInfo.PSODesc.PipelineType = Diligent::PIPELINE_TYPE_GRAPHICS;
        // clang-format off
        // This tutorial will render to a single render target
        PSOCreateInfo.GraphicsPipeline.NumRenderTargets = 1;
        // Set render target format which is the format of the swap chain's color buffer
        PSOCreateInfo.GraphicsPipeline.RTVFormats[0] = gfx->GetSwapChain()->GetDesc().ColorBufferFormat;
        // Set depth buffer format which is the format of the swap chain's back buffer
        PSOCreateInfo.GraphicsPipeline.DSVFormat = Diligent::TEX_FORMAT_UNKNOWN;
        // Primitive topology defines what kind of primitives will be rendered by this pipeline state
        PSOCreateInfo.GraphicsPipeline.PrimitiveTopology = Diligent::PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        // Cull back faces
        PSOCreateInfo.GraphicsPipeline.RasterizerDesc.CullMode = Diligent::CULL_MODE_BACK;
        // Enable depth testing
        PSOCreateInfo.GraphicsPipeline.DepthStencilDesc.DepthEnable = Diligent::False;

        PSOCreateInfo.PSODesc.ResourceLayout.DefaultVariableType = Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC;

        Diligent::ShaderCreateInfo ShaderCI;
        ShaderCI.SourceLanguage = Diligent::SHADER_SOURCE_LANGUAGE_HLSL;
        ShaderCI.UseCombinedTextureSamplers = true;
        ShaderCI.pShaderSourceStreamFactory = gfx->UseShaderFactory();

        // Create a vertex shader
        RefPtr<Diligent::IShader> pVS;
        // Create a pixel shader
        RefPtr<Diligent::IShader> pPS;
        // Create a geom shader
        RefPtr<Diligent::IShader> pGS;

        const auto stages = GetShaderStages();
        
        if (stages.IsSet(EShaderStages::eSS_VERTEX))
        {
            const char* sourcePath = GetVertexShaderPath();
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_VERTEX;
            ShaderCI.EntryPoint = "vert";
            ShaderCI.Desc.Name = sourcePath;
            ShaderCI.FilePath = sourcePath;
            gfx->UseDevice()->CreateShader(ShaderCI, &pVS);
            PSOCreateInfo.pVS = pVS;
        }

        if (stages.IsSet(EShaderStages::eSS_PIXEL))
        {
            const char* frag = GetFragmentShaderPath();
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_PIXEL;
            ShaderCI.EntryPoint = "frag";
            ShaderCI.Desc.Name = frag;
            ShaderCI.FilePath = frag;
            gfx->UseDevice()->CreateShader(ShaderCI, &pPS);
            PSOCreateInfo.pPS = pPS;
        }

        if (stages.IsSet(EShaderStages::eSS_GEOMETRY))
        {
            const char* geom = GetGeometryShaderPath();
            ShaderCI.Desc.ShaderType = Diligent::SHADER_TYPE_GEOMETRY;
            ShaderCI.EntryPoint = "geom";
            ShaderCI.Desc.Name = geom;
            ShaderCI.FilePath = geom;
            gfx->UseDevice()->CreateShader(ShaderCI, &pGS);
            PSOCreateInfo.pGS = pGS;
        }


        Diligent::ShaderResourceVariableDesc Vars[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_PrevRTV", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_DSV", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_MUTABLE},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", Diligent::SHADER_RESOURCE_VARIABLE_TYPE_STATIC}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.Variables = Vars;
        PSOCreateInfo.PSODesc.ResourceLayout.NumVariables = _countof(Vars);

        Diligent::SamplerDesc SamLinearClampDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP, Diligent::TEXTURE_ADDRESS_WRAP
        };

        Diligent::SamplerDesc SamBorderDesc
        {
            Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR, Diligent::FILTER_TYPE_LINEAR,
            Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_BORDER, Diligent::TEXTURE_ADDRESS_CLAMP
        };

        SamBorderDesc.ComparisonFunc = Diligent::COMPARISON_FUNC_LESS;
        SamBorderDesc.MinFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MagFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.MipFilter = Diligent::FILTER_TYPE_COMPARISON_LINEAR;
        SamBorderDesc.BorderColor[0] = 1.0f;
        SamBorderDesc.BorderColor[1] = 1.0f;
        SamBorderDesc.BorderColor[2] = 1.0f;
        SamBorderDesc.BorderColor[3] = 1.0f;

        Diligent::ImmutableSamplerDesc ImtblSamplers[] =
        {
            {Diligent::SHADER_TYPE_PIXEL, "Tex_PrevRTV", SamLinearClampDesc},
            {Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap", SamBorderDesc}
        };
        // clang-format on
        PSOCreateInfo.PSODesc.ResourceLayout.ImmutableSamplers = ImtblSamplers;
        PSOCreateInfo.PSODesc.ResourceLayout.NumImmutableSamplers = _countof(ImtblSamplers);

        gfx->UseDevice()->CreateGraphicsPipelineState(PSOCreateInfo, &m_Pipeline);
        const bool success = m_Pipeline != nullptr;
        if (!success)
        {
            nshl_assert2(success, "Failed to create pipeline");
            return;
        }
        SetCommonCBS(gfx);

        ShadowMap* map = Resources::Instance()->Textures().Resolve<ShadowMap>({ "ShadowMap" });
        Diligent::IDeviceObject* srvs[MAX_SHADOW_MAPS];
        for (uint32 i = 0; i < MAX_SHADOW_MAPS; i++)
        {
            srvs[i] = map->m_aDepthTextures[i].SRV();
        }
        if (auto* varArr = m_Pipeline->GetStaticVariableByName(Diligent::SHADER_TYPE_PIXEL, "Tex_Shadowmap"))
            varArr->SetArray(srvs, 0, MAX_SHADOW_MAPS);
    }

    void PostProcessShader::UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst)
    {
    }

}