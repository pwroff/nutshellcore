#include "ConstantBuffers.h"
#include "Graphics/Gfx.h"
#include "Resources/Resources.h"

#include "Scene/Camera.h"
#include "Scene/Scene.h"
#include "Graphics/Renderable.h"
#include "Math/math.h"


namespace nshl
{
    CameraConstants CameraCB::GetRelevantData(const RenderContext& ctx) const
    {
        CameraConstants CBConstants;
        CBConstants.Proj = ctx.ActiveCamera->m_Projection.Transpose();
        CBConstants.View = ctx.ActiveCamera->m_View.Transpose();
        const auto wts = ctx.ActiveCamera->GetWorldToScreenMatrix();
        CBConstants.ViewProj = (wts).Transpose();
        CBConstants.InvView = ctx.ActiveCamera->m_View.Inverse().Transpose();
        CBConstants.InvViewProj = (wts.Inverse()).Transpose();
        CBConstants.ViewDirection = float4(ctx.ActiveCamera->m_Transform.Forward(), 0.0f);
        const auto& SCDesc = ctx.Gfx->GetSwapChain()->GetDesc();
        CBConstants.ViewportSize = float4(static_cast<float>(SCDesc.Width), static_cast<float>(SCDesc.Height), 1.f / static_cast<float>(SCDesc.Width), 1.f / static_cast<float>(SCDesc.Height));
        CBConstants.CameraProperties = float4(ctx.ActiveCamera->m_NearPlane, ctx.ActiveCamera->m_FarPLane, float(SCDesc.Width) / float(SCDesc.Height), ctx.ActiveCamera->m_FOV * DEG2RAD);
        return CBConstants;
    }
    LightConstants LightCB::GetRelevantData(const RenderContext& ctx) const
    {
        LightConstants sceneLight;
        const auto& lights = ctx.Scene->GetLights();
        const auto s = min(uint32(lights.size()), MAX_LIGHTS);
        for (uint32 i = 0; i < s; i++)
        {
            sceneLight.lights[i] = lights[i].GetLightDefs();
            if (lights[i].Type == ELightType::UNKNOWN)
                break;
            sceneLight.CasterViewProjections[lights[i].ShadowMapIndex] = lights[i].GetWorldToScreenMatrix().Transpose();
        }
        if (s < (MAX_LIGHTS - 1))
            sceneLight.lights[s + 1] = {};
        return sceneLight;
    }
}
