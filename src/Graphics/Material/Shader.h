#pragma once
#include "Core/core.h"
#include "Math/math.h"
#include "Core/Objects.h"

namespace nshl
{
	class Gfx;
	enum ERenderFLag : uint32
	{
		ERF_CASTS_SHADOW = BIT(0),
		ERF_DEFERRED_OPAQUE = BIT(1),
		ERF_HAS_RIG = BIT(2),
	};


	template<typename T>
	struct EFlag
	{
		uint32 Value = 0u;

		bool IsSet(uint32 v) const { return (Value & v) != 0; }
	};

	enum EShaderStages : uint32
	{
		eSS_PIXEL = 1 << 0,
		eSS_VERTEX = 1 << 1,
		eSS_GEOMETRY = 1 << 2,


		eSS_COMMON = eSS_PIXEL | eSS_VERTEX
	};
	Diligent::GraphicsPipelineStateCreateInfo GetCommonPSOCreateInfo(Gfx* ctx);

	class IShaderBase : public ICoreObject, public WithIDGen<IShaderBase>
	{
	protected:
		RefPtr<Diligent::IPipelineState> m_Pipeline;
		
		virtual void SetConstantBuffer(const char* varName, IBuffer* buffer);
		void SetCommonCBS(Gfx* gfx);

		static void SetDefaultLayout(Diligent::GraphicsPipelineStateCreateInfo& PSOCreateInfo);
		bool CreateShadersFromFile(Gfx* gfx, const char* sourcePath, Diligent::GraphicsPipelineStateCreateInfo& PSOCreateInfo);
	public:
		virtual void Init(Gfx* gfx) {}
		virtual void Bind(const RenderContext& ctx, Diligent::IShaderResourceBinding* srb);
		virtual Diligent::IPipelineState* UsePipeline() {
			return m_Pipeline;
		}
		virtual EFlag<EShaderStages> GetShaderStages() const = 0;
	};

	template<typename T, uint32 Stages>
	class ShaderBase : public WithTypeID<T, IShaderBase>
	{
	protected:
		
		EFlag<EShaderStages> GetShaderStages() const override { return SHADER_STAGES; }
	public:
		static constexpr EFlag<EShaderStages> SHADER_STAGES = { Stages };
	};

	class UnlitShader: public ShaderBase<UnlitShader, EShaderStages::eSS_COMMON>
	{
	protected:
		RefPtr<IBuffer> m_pMaterialConstantBuffer;
	public:
		struct MaterialConstants
		{
			float3 Color;
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst);
	};

	class WireframeShader : public ShaderBase<WireframeShader, eSS_COMMON | eSS_GEOMETRY>
	{
	public:
		struct MaterialConstants
		{
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst) {}
	};

	class DepthDisplayShader : public ShaderBase<DepthDisplayShader, eSS_COMMON>
	{
	public:
		struct MaterialConstants
		{
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst) {}
	};

	class PostProcessShader : public ShaderBase<PostProcessShader, EShaderStages::eSS_COMMON>
	{
	public:
		struct MaterialConstants
		{
		};
		void Init(Gfx* gfx) override;
		void UpdateMaterialBuffer(Gfx* gfx, const MaterialConstants& matConst);

		virtual const char* GetVertexShaderPath() const { return "shaders/post_process.shader"; }
		virtual const char* GetFragmentShaderPath() const { return "shaders/post_process.shader"; }
		virtual const char* GetGeometryShaderPath() const { return nullptr; }
	};
}

