#pragma once
#include "Core/core.h"
#include "Math/math.h"
#include "Core/Objects.h"

namespace nshl
{
    class Gfx;
    struct IMaterial;
    struct RenderContext;
    struct Vertex
    {
        float3 position = f3Zero;
        float3 normal = f3Up;
        float3 tangent = f3Aside;
        float3 bitangent = f3Up;
        float3 weights = f3Zero;
        int joints[3] = { -1 };
        float2 uv = {0, 0};
        Vertex() = default;
        Vertex(const float3& pos, const float3& norm, const float2& texcoord);
    };

    static constexpr uint32 MAX_BONES = 75;
    static constexpr uint32 MAX_SKELETONES_PER_DRAW_CALL = 5;

    struct Bone
    {
        Quaternion rotation;
        float3 position;
        uint32 parent = InvalidID<uint32>();
        std::string Name;

        float4x4 AsMatrix() const;
    };

    struct Skeletone
    {
        uint32 NumBones = 0;
        Bone Bones[MAX_BONES];
    };

    struct RendSkeletone
    {
        float4x3 skeletones[MAX_SKELETONES_PER_DRAW_CALL][MAX_BONES];
    };

    struct MeshData
    {
        std::vector<Vertex> vertices;
        std::vector<uint32> indices;
        Skeletone rig;
        const char* name;

        void AddTriangle(uint32 a, uint32 b, uint32 c);
        void ComputeFlatNormals();
        static MeshData LoadFromFile(const std::string& path);
    };

    struct Bounds
    {
        float3 mins;
        float3 maxs;

        void SetMinMax(const float3& point);
    };

    MeshData CreateCubeData(float3 size = f3One, float3 offset = f3Zero);
    MeshData CreatePlaneData(const float3& resolution);
    MeshData CreateQuadData(float2 size);


    class IRigDefs : public ICoreObject
    {
    protected:
        Skeletone m_Skeletone = {};
        Array<float4x4> m_InverseModel;
        Array<float4x4> m_Model;
    public:
        void Init(Gfx* gfx, const Skeletone& skeletone);
        const Skeletone& GetSkeletone() const { return m_Skeletone; }
        const Array<float4x4>& GetModelInverse() const { return m_InverseModel; }
        const Array<float4x4>& GetModel() const { return m_Model; }
        uint32 FindNodeIndex(const char* boneName) const;
    };

    class IMesh : public ICoreObject
    {
    protected:
        Bounds m_Bounds;
        uint32 m_NumIndices;
        RefPtr<IBuffer> m_pVertexBuffer;
        RefPtr<IBuffer> m_pIndexBuffer;
        std::unique_ptr<IRigDefs> m_Rig;
        std::string Name = {};
        bool m_HasRig = false;
    public:
        void Init(Gfx* gfx, const MeshData& data);

        IBuffer* UseVertexBuffer() { return m_pVertexBuffer; }
        IBuffer* UseIndexBuffer() { return m_pIndexBuffer; }

        uint32 GetNumIndices() const { return m_NumIndices; }
        bool HasRig() const { return m_HasRig; }
        const IRigDefs* GetRig() const { return m_HasRig ? m_Rig.get() : nullptr; }

        void DrawIndexed(const RenderContext& ctx, IMaterial* material, IBuffer* instacesData, uint32 numInstances);
    };

    class Mesh : public IMesh
    {
    public:
    };
}
