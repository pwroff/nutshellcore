#include "Mesh.h"
#include "../Gfx.h"
#include "Graphics/Material/Material.h"
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

namespace nshl
{
    void MeshData::AddTriangle(uint32 a, uint32 b, uint32 c)
    {
        indices.push_back(a);
        indices.push_back(b);
        indices.push_back(c);
    }

    void MeshData::ComputeFlatNormals()
    {
        for (size_t i = 0; i < indices.size(); i += 3)
        {
            auto& v0 = vertices[indices[i]];
            auto& v1 = vertices[indices[i + 1]];
            auto& v2 = vertices[indices[i + 2]];
            const auto& p0 = v0.position;
            const auto& p1 = v1.position;
            const auto& p2 = v2.position;

            const auto n = Diligent::normalize(Diligent::cross(p1 - p0, p2 - p1));
            v0.normal = n;
            v1.normal = n;
            v2.normal = n;
        }
    }

    MeshData CreateCubeData(float3 size, float3 offset)
    {
        size *= 0.5f;
        MeshData data;
        data.vertices =
        {
            {multiplyEach(float3(-1,-1,-1), size) + offset, float3(0, -1, 0), float2(0,1)},
            {multiplyEach(float3(-1,+1,-1), size) + offset, float3(0, -1, 0), float2(0,0)},
            {multiplyEach(float3(+1,+1,-1), size) + offset, float3(0, -1, 0), float2(1,0)},  // bootom
            {multiplyEach(float3(+1,-1,-1), size) + offset, float3(0, -1, 0), float2(1,1)},

            {multiplyEach(float3(-1,-1,-1) ,size) + offset, float3(0, 0, -1),float2(0,1)},
            {multiplyEach(float3(-1,-1,+1) ,size) + offset, float3(0, 0, -1),float2(0,0)},
            {multiplyEach(float3(+1,-1,+1) ,size) + offset, float3(0, 0, -1),float2(1,0)}, // front
            {multiplyEach(float3(+1,-1,-1) ,size) + offset, float3(0, 0, -1),float2(1,1)},

            {multiplyEach(float3(+1,-1,-1) ,size) + offset, float3(1, 0, 0), float2(0,1)},
            {multiplyEach(float3(+1,-1,+1) ,size) + offset, float3(1, 0, 0), float2(1,1)},
            {multiplyEach(float3(+1,+1,+1) ,size) + offset, float3(1, 0, 0), float2(1,0)}, // right
            {multiplyEach(float3(+1,+1,-1) ,size) + offset, float3(1, 0, 0), float2(0,0)},

            {multiplyEach(float3(+1,+1,-1) ,size) + offset, float3(0, 0, 1), float2(0,1)}, // 12
            {multiplyEach(float3(+1,+1,+1) ,size) + offset, float3(0, 0, 1), float2(0,0)}, // 13     // back
            {multiplyEach(float3(-1,+1,+1) ,size) + offset, float3(0, 0, 1), float2(1,0)}, // 14
            {multiplyEach(float3(-1,+1,-1) ,size) + offset, float3(0, 0, 1), float2(1,1)}, // 15 (15, 14, 13) (15, 13, 12)

            {multiplyEach(float3(-1,+1,-1) ,size) + offset, float3(-1, 0, 0), float2(1,0)},
            {multiplyEach(float3(-1,+1,+1) ,size) + offset, float3(-1, 0, 0), float2(0,0)}, // left // (16, 18, 17) (16, 19, 18)
            {multiplyEach(float3(-1,-1,+1) ,size) + offset, float3(-1, 0, 0), float2(0,1)},
            {multiplyEach(float3(-1,-1,-1) ,size) + offset, float3(-1, 0, 0), float2(1,1)},

            {multiplyEach(float3(-1,-1,+1) ,size) + offset, float3(0, 1, 0), float2(1,1)},
            {multiplyEach(float3(+1,-1,+1) ,size) + offset, float3(0, 1, 0), float2(0,1)},
            {multiplyEach(float3(+1,+1,+1) ,size) + offset, float3(0, 1, 0), float2(0,0)}, // top
            {multiplyEach(float3(-1,+1,+1) ,size) + offset, float3(0, 1, 0), float2(1,0)}
        };
        data.indices =
        {
            2,0,1,    2,3,0,
            4,6,5,    4,7,6,
            8,10,9,   8,11,10,
            12,14,13, 12,15,14,
            16,18,17, 16,19,18,
            20,21,22, 20,22,23
        };
        data.name = "Cube";
        return data;
    }

    MeshData CreatePlaneData(const float3& resolution)
    {
        int width = int(resolution.x);
        int height = int(resolution.y);
        float topLeftX = (width - 1) * -0.5f;
        float topLeftZ = (height - 1) * 0.5f;

        int meshSimplificationIncrement = int(resolution.z);
        int verticesPerLine = (width - 1) / meshSimplificationIncrement + 1;

        MeshData meshData;
        int vertexIndex = 0;

        for (int y = 0; y < height; y += meshSimplificationIncrement)
        {
            for (int x = 0; x < width; x += meshSimplificationIncrement)
            {
                Vertex vert;
                vert.position = { topLeftX + x, 0.0f, topLeftZ - y };
                vert.uv = float2{ x / (float)width, y / (float)height };
                meshData.vertices.push_back(std::move(vert));

                if (x < width - 1 && y < height - 1)
                {
                    meshData.AddTriangle(vertexIndex, vertexIndex + verticesPerLine + 1, vertexIndex + verticesPerLine);
                    meshData.AddTriangle(vertexIndex + verticesPerLine + 1, vertexIndex, vertexIndex + 1);
                }
                vertexIndex++;
            }
        }
        meshData.ComputeFlatNormals();
        meshData.name = "Plane";
        return meshData;
    }

    MeshData CreateQuadData(float2 size)
    {
        size *= 0.5f;
        MeshData data;
        data.vertices =
        {
            {float3(-1,-1, 0) * size, float3(0, 0, -1),float2(0,1)},
            {float3(-1,+1, 0) * size, float3(0, 0, -1),float2(0,0)},
            {float3(+1,+1, 0) * size, float3(0, 0, -1),float2(1,0)}, // front
            {float3(+1,-1, 0) * size, float3(0, 0, -1),float2(1,1)},
        };
        data.indices =
        {
            2,0,1,    2,3,0,
        };
        data.name = "Quad";
        return data;
    }

    void IMesh::Init(Gfx* gfx, const MeshData& data)
    {
        for (const auto& vert : data.vertices)
            m_Bounds.SetMinMax(vert.position);
        m_NumIndices = uint32(data.indices.size());
        Diligent::BufferDesc VertBuffDesc;
        std::string name = std::string(data.name) + " Vertex Buffer";
        VertBuffDesc.Name = name.c_str();
        VertBuffDesc.Usage = Diligent::USAGE_IMMUTABLE;
        VertBuffDesc.BindFlags = Diligent::BIND_VERTEX_BUFFER;
        VertBuffDesc.uiSizeInBytes = sizeof(Vertex) * uint32(data.vertices.size());
        Diligent::BufferData VBData;
        VBData.pData = data.vertices.data();
        VBData.DataSize = VertBuffDesc.uiSizeInBytes;

        gfx->UseDevice()->CreateBuffer(VertBuffDesc, &VBData, &m_pVertexBuffer);

        Diligent::BufferDesc IndBuffDesc;
        name = std::string(data.name) + " Index Buffer";
        IndBuffDesc.Name = name.c_str();
        IndBuffDesc.Usage = Diligent::USAGE_IMMUTABLE;
        IndBuffDesc.BindFlags = Diligent::BIND_INDEX_BUFFER;
        IndBuffDesc.uiSizeInBytes = sizeof(uint32) * uint32(data.indices.size());
        Diligent::BufferData IBData;
        IBData.pData = data.indices.data();
        IBData.DataSize = IndBuffDesc.uiSizeInBytes;
        Name = data.name;
        m_HasRig = data.rig.NumBones > 0;
        if (data.rig.NumBones > 0)
        {
            m_Rig = std::make_unique<IRigDefs>();
            m_Rig->Init(gfx, data.rig);
        }
        gfx->UseDevice()->CreateBuffer(IndBuffDesc, &IBData, &m_pIndexBuffer);
    }
    void IMesh::DrawIndexed(const RenderContext& ctx, IMaterial* material, IBuffer* instacesData, uint32 numInstances)
    {
        // Bind vertex and index buffers
        uint32   offsets[] = { 0, 0 };
        IBuffer* pBuffs[] = { m_pVertexBuffer, instacesData };
        ctx.Gfx->UseContext()->SetVertexBuffers(0, _countof(pBuffs), pBuffs, offsets, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION, Diligent::SET_VERTEX_BUFFERS_FLAG_RESET);
        ctx.Gfx->UseContext()->SetIndexBuffer(m_pIndexBuffer, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
        
        material->Bind(ctx);

        Diligent::DrawIndexedAttribs DrawAttrs;     // This is an indexed draw call
        DrawAttrs.IndexType = Diligent::VT_UINT32; // Index type
        DrawAttrs.NumIndices = m_NumIndices;
        DrawAttrs.NumInstances = numInstances;
        // Verify the state of vertex and index buffers
        DrawAttrs.Flags = Diligent::DRAW_FLAG_VERIFY_ALL;
        ctx.Gfx->UseContext()->DrawIndexed(DrawAttrs);
    }
    MeshData MeshData::LoadFromFile(const std::string& path)
    {
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* scene = importer.ReadFile(path,
            aiProcess_CalcTangentSpace |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_GenBoundingBoxes |
            aiProcess_PopulateArmatureData
        );

        // If the import failed, report it
        if (!scene) {
            auto str = importer.GetErrorString();
            return MeshData();
        }
        if (scene->mNumMeshes > 0)
        {
            MeshData data;
            auto* mesh = scene->mMeshes[0];
            data.vertices.reserve(mesh->mNumVertices);
            for (auto i = 0u; i < mesh->mNumVertices; i++)
            {
                Vertex vert;
                vert.position = float3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
                vert.normal = float3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
                vert.uv = float2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
                data.vertices.push_back(std::move(vert));
            }
            const auto numIndices = mesh->mNumFaces * 3;
            data.indices.reserve(numIndices);
            for (auto i = 0u; i < mesh->mNumFaces; i++)
            {
                data.AddTriangle(mesh->mFaces[i].mIndices[0], mesh->mFaces[i].mIndices[1], mesh->mFaces[i].mIndices[2]);
            }

            return data;
        }
        return MeshData();
    }
    void Bounds::SetMinMax(const float3& point)
    {
        {
            if (point.x < mins.x)
                mins.x = point.x;
            if (point.y < mins.y)
                mins.y = point.y;
            if (point.z < mins.z)
                mins.z = point.z;
        }
        {
            if (point.x > mins.x)
                maxs.x = point.x;
            if (point.y > mins.y)
                maxs.y = point.y;
            if (point.z > mins.z)
                maxs.z = point.z;
        }
    }
    Vertex::Vertex(const float3& pos, const float3& norm, const float2& texcoord):
        position(pos),
        normal(norm),
        uv(texcoord)
    {
        bitangent = Diligent::cross(f3Aside, norm);
        tangent = Diligent::cross(norm, bitangent);
    }

    void IRigDefs::Init(Gfx* gfx, const Skeletone& skeletone)
    {
        m_Skeletone.NumBones = skeletone.NumBones;
        m_InverseModel.resize(m_Skeletone.NumBones);
        m_Model.resize(m_Skeletone.NumBones);
        for (uint32 i = 0; i < m_Skeletone.NumBones; i++)
        {
            m_Skeletone.Bones[i] = skeletone.Bones[i];
            m_Model[i] = m_Skeletone.Bones[i].AsMatrix();
            if (i != 0)
            {
                m_Model[i] = m_Model[m_Skeletone.Bones[i].parent] * m_Model[i];
                m_InverseModel[i] = m_Model[i].Inverse();//m_Model[m_Skeletone.Bones[i].parent].Inverse() * m_Model[i];
            }
            else
            {
                m_InverseModel[i] = m4Identity;
            }
        }
    }
    uint32 IRigDefs::FindNodeIndex(const char* boneName) const
    {
        for (uint32 i = 0; i < m_Skeletone.NumBones; i++)
        {
            if (strcmp(boneName, m_Skeletone.Bones[i].Name.c_str()) == 0)
                return i;
        }
        return InvalidID<uint32>();
    }
    float4x4 Bone::AsMatrix() const
    {
        float4x4 orient = rotation.ToMatrix();

        return float4x4
        {
            orient._11, orient._12, orient._13, 0.0f,
            orient._21, orient._22, orient._23, 0.0f,
            orient._31, orient._32, orient._33, 0.0f,
            position.x, position.y, position.z, 1.0f
        };
    }
}
