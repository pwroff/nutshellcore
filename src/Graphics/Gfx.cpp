#include "Gfx.h"
#include "Graphics/Texture/Texture.h"
#include "Common/interface/FileWrapper.hpp"
#include "Platforms/interface/FileSystem.hpp"
#include "TextureLoader/interface/TextureUtilities.h"
#include "TextureLoader/interface/Image.h"

namespace nshl
{
    Gfx* Gfx::sm_Instance = nullptr;

    Gfx::Gfx()
    {
        sm_Instance = this;
    }
	Gfx::~Gfx()
	{
		if (m_pImmediateContext)
            m_pImmediateContext->Flush();
	}
	bool Gfx::Initialize(HWND hWnd, Diligent::RENDER_DEVICE_TYPE deviceType)
	{
        using namespace Diligent;
        SwapChainDesc SCDesc;
        SCDesc.BufferCount = 3;
        switch (deviceType)
        {
#if D3D11_SUPPORTED
        case RENDER_DEVICE_TYPE_D3D11:
        {
            EngineD3D11CreateInfo EngineCI;
#    ifdef DILIGENT_DEBUG
            EngineCI.DebugFlags |=
                D3D11_DEBUG_FLAG_CREATE_DEBUG_DEVICE |
                D3D11_DEBUG_FLAG_VERIFY_COMMITTED_SHADER_RESOURCES;
#    endif
#    if ENGINE_DLL
            // Load the dll and import GetEngineFactoryD3D11() function
            auto GetEngineFactoryD3D11 = LoadGraphicsEngineD3D11();
#    endif
            auto* pFactoryD3D11 = GetEngineFactoryD3D11();
            pFactoryD3D11->CreateDeviceAndContextsD3D11(EngineCI, &m_pDevice, &m_pImmediateContext);
            Win32NativeWindow Window{ hWnd };
            pFactoryD3D11->CreateSwapChainD3D11(m_pDevice, m_pImmediateContext, SCDesc, FullScreenModeDesc{}, Window, &m_pSwapChain);
            m_pEngineFactory.Attach(pFactoryD3D11);
        }
        break;
#endif


#if D3D12_SUPPORTED
        case RENDER_DEVICE_TYPE_D3D12:
        {
#    if ENGINE_DLL
            // Load the dll and import GetEngineFactoryD3D12() function
            auto GetEngineFactoryD3D12 = LoadGraphicsEngineD3D12();
#    endif
            EngineD3D12CreateInfo EngineCI;
#    ifdef _DEBUG
            //EngineCI.EnableDebugLayer = true;
#    endif
            auto* pFactoryD3D12 = GetEngineFactoryD3D12();
            pFactoryD3D12->CreateDeviceAndContextsD3D12(EngineCI, &m_pDevice, &m_pImmediateContext);
            Win32NativeWindow Window{ hWnd };
            pFactoryD3D12->CreateSwapChainD3D12(m_pDevice, m_pImmediateContext, SCDesc, FullScreenModeDesc{}, Window, &m_pSwapChain);
            m_pEngineFactory.Attach(pFactoryD3D12);
        }
        break;
#endif


#if GL_SUPPORTED
        case RENDER_DEVICE_TYPE_GL:
        {

#    if EXPLICITLY_LOAD_ENGINE_GL_DLL
            // Load the dll and import GetEngineFactoryOpenGL() function
            auto GetEngineFactoryOpenGL = LoadGraphicsEngineOpenGL();
#    endif
            auto* pFactoryOpenGL = GetEngineFactoryOpenGL();

            EngineGLCreateInfo EngineCI;
            EngineCI.Window.hWnd = hWnd;
#    ifdef DILIGENT_DEBUG
            EngineCI.CreateDebugContext = true;
#    endif
            pFactoryOpenGL->CreateDeviceAndSwapChainGL(EngineCI, &m_pDevice, &m_pImmediateContext, SCDesc, &m_pSwapChain);
            m_pEngineFactory.Attach(pFactoryOpenGL);
        }
        break;
#endif


#if VULKAN_SUPPORTED
        case RENDER_DEVICE_TYPE_VULKAN:
        {
#    if EXPLICITLY_LOAD_ENGINE_VK_DLL
            // Load the dll and import GetEngineFactoryVk() function
            auto GetEngineFactoryVk = LoadGraphicsEngineVk();
#    endif
            EngineVkCreateInfo EngineCI;
#    ifdef DILIGENT_DEBUG
            EngineCI.EnableValidation = true;
#    endif
            auto* pFactoryVk = GetEngineFactoryVk();
            pFactoryVk->CreateDeviceAndContextsVk(EngineCI, &m_pDevice, &m_pImmediateContext);

            if (!m_pSwapChain && hWnd != nullptr)
            {
                Win32NativeWindow Window{ hWnd };
                pFactoryVk->CreateSwapChainVk(m_pDevice, m_pImmediateContext, SCDesc, Window, &m_pSwapChain);
            }
            m_pEngineFactory.Attach(pFactoryVk);
        }
        break;
#endif


        default:
            std::cerr << "Unknown/unsupported device type";
            return false;
            break;
        }
        m_pEngineFactory->CreateDefaultShaderSourceStreamFactory("C:/Projects/nutshellcore/assets;C:/Projects/nutshellcore/assets/shaders", &m_pShaderFactory);
        return true;
	}
    void Gfx::Present()
    {
        m_pSwapChain->Present();
    }
    void Gfx::OnWindowResize(uint32 Width, uint32 Height)
    {
        if (m_pSwapChain)
        {
            m_pSwapChain->Resize(Width, Height);
        }
    }
    bool Gfx::SaveTextureToFile(const std::string& FileName, ITexture* texture)
    {
        Diligent::MappedTextureSubresource TexData;
        m_pImmediateContext->MapTextureSubresource(texture->GPUTEX(), 0, 0, Diligent::MAP_READ, Diligent::MAP_FLAG_DO_NOT_WAIT, nullptr, TexData);
        const auto& TexDesc = texture->GPUTEX()->GetDesc();

        Diligent::Image::EncodeInfo Info;
        Info.Width = TexDesc.Width;
        Info.Height = TexDesc.Height;
        Info.TexFormat = TexDesc.Format;
        Info.KeepAlpha = false;
        Info.pData = TexData.pData;
        Info.Stride = TexData.Stride;

        Diligent::RefCntAutoPtr<Diligent::IDataBlob> pEncodedImage;
        Diligent::Image::Encode(Info, &pEncodedImage);
        m_pImmediateContext->UnmapTextureSubresource(texture->GPUTEX(), 0, 0);
        Diligent::FileWrapper pFile(FileName.c_str(), EFileAccessMode::Overwrite);
        if (pFile)
        {
            auto res = pFile->Write(pEncodedImage->GetDataPtr(), pEncodedImage->GetSize());
            if (!res)
            {
                LOG_ERROR_MESSAGE("Failed to write screen capture file '", FileName, "'.");
                return false;
            }
            pFile.Close();
        }
        else
        {
            LOG_ERROR_MESSAGE("Failed to create screen capture file '", FileName, "'. Verify that the directory exists and the app has sufficient rights to write to this directory.");
            return false;
        }

        return true;
    }
}