#pragma once
#include "Core/core.h"

namespace nshl
{
	class ITexture;
	class Gfx : public ICoreObject
	{
		static Gfx* sm_Instance;
		RefPtr<Diligent::IRenderDevice> m_pDevice;
		RefPtr<Diligent::IDeviceContext> m_pImmediateContext;
		RefPtr<Diligent::ISwapChain> m_pSwapChain;

		RefPtr<Diligent::IEngineFactory> m_pEngineFactory;
		RefPtr<Diligent::IShaderSourceInputStreamFactory> m_pShaderFactory;

	public:

		Gfx();
		~Gfx();

		bool Initialize(HWND hWnd, Diligent::RENDER_DEVICE_TYPE deviceType);
		void Present();
		void OnWindowResize(uint32 Width, uint32 Height);

		Diligent::IRenderDevice* UseDevice() { return m_pDevice; }
		Diligent::IDeviceContext* UseContext() { return m_pImmediateContext; }
		Diligent::ISwapChain* UseSwapChain() { return m_pSwapChain; }

		Diligent::IShaderSourceInputStreamFactory* UseShaderFactory() { return m_pShaderFactory; }

		const Diligent::IRenderDevice* GetDevice() const { return m_pDevice; }
		const Diligent::ISwapChain* GetSwapChain() const { return m_pSwapChain; }


		bool SaveTextureToFile(const std::string& FileName, ITexture* texture);

		static Gfx* Instance() { return sm_Instance; }
	};
}
