#pragma once
#include "ResourceHelpers.h"
#include "Core/Objects.h"
#include "Simulation/Animation/Animation.h"


namespace nshl
{
	class Gfx;
	struct RenderGroup;
	class IMesh;
	struct IMaterial;
	struct ICBuff;
	class IRig;
	class IShaderBase;
	class ITexture;
	class IScene;

	class Resources : public Singletone<Resources>
	{
		CacheMap<IShaderBase, Gfx> m_mShaderPool;
		CacheMap<IMaterial, Gfx> m_mMaterialPool;
		CacheMap<ICBuff, Gfx> m_mBuffersPool;
		CacheMap<RenderGroup, Gfx> m_mRenderGroupsPool;
		CacheMap<IMesh, Gfx> m_mMeshPool;
		CacheMap<ITexture, Gfx> m_mTexturePool;
		CacheMap<IAnimation, Gfx> m_mAnimationPool;
	public:
		CacheMap<IShaderBase, Gfx>& Shaders() { return m_mShaderPool; }
		CacheMap<IMaterial, Gfx>& Materials() { return m_mMaterialPool; }
		CacheMap<ICBuff, Gfx>& Buffers() { return m_mBuffersPool; }
		CacheMap<RenderGroup, Gfx>& RenderGroups() { return m_mRenderGroupsPool; }
		CacheMap<IMesh, Gfx>& Meshes() { return m_mMeshPool; }
		CacheMap<ITexture, Gfx>& Textures() { return m_mTexturePool; }
		CacheMap<IAnimation, Gfx>& Animations() { return m_mAnimationPool; }

	public:
		struct LoadedAssets
		{
			struct LoadedAsResource
			{
				Array<ResourceID> Entries;
				void Add(const ResourceID& res);
			};
			LoadedAsResource Materials;
			LoadedAsResource Meshes;
			LoadedAsResource Animations;
			Array<uint64> Renderables;
		};
		LoadedAssets LoadRenderableFromFile(IScene* scene, const std::string& path, const float meshScale = 1.0f);
	};

}
