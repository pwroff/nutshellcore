#include "Resources.h"
#include "Graphics/Material/PBRMaterial.h"
#include "Graphics/Mesh/Mesh.h"
#include "Simulation/Animation/Animation.h"
#include "Graphics/Texture/Texture.h"
#include "Graphics/Gfx.h"
#include "Scene/Scene.h"
#include "Graphics/Renderable.h"
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "Core/Logger.h"
#include <filesystem>
#include <algorithm>

namespace nshl
{
    namespace 
    {
        [[nodiscard]] ResourceID ResolveMaterial(Resources* resources, const aiMaterial* material, const std::string& path)
        {
            ResourceID resource = { material->GetName().C_Str() };
            if (resources->Materials().Find(resource))
                return {};
            DeferredDrawMaterial* internalMat = resources->Materials().Resolve<DeferredDrawMaterial>(resource);
           

            const auto setTexture = [&](const char* texturePath, const char* textureType)
            {
                const std::string absolute = [&]() -> std::string {

                    std::filesystem::path assetPath(texturePath);

                    if (assetPath.has_root_name())
                        return std::string(texturePath);

                    return std::string(path) + "\\" + std::string(texturePath);
                }();
                Texture2D* diffuseTex = resources->Textures().Resolve<Texture2D>({ absolute.c_str() }, absolute.c_str());
                internalMat->SetTexture(textureType, diffuseTex);
            };
            aiString texturePath;
            unsigned int numTextures = material->GetTextureCount(aiTextureType_DIFFUSE);

            if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0 && material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == AI_SUCCESS)
            {
                setTexture(texturePath.C_Str(), "Tex_Diffuse");
            }
            else
            {
                std::filesystem::path assetPath(path);
                if (assetPath.has_filename())
                {
                    assetPath.remove_filename();
                }
                assetPath.append("Tex_Diffuse.png");
                setTexture(assetPath.string().c_str(), "Tex_Diffuse");
            }

            return resource;
        }
        
        namespace
        {
            Array<aiNode*>::iterator SortedInsertAINode(Array<aiNode*>& sorted, aiNode* node)
            {
                if (auto itPos = sorted.Find(node); itPos != sorted.end())
                    return itPos;
                if (!node->mParent)
                {
                    sorted.push_back(node);
                    return sorted.begin();
                }
                auto parentNodePos = SortedInsertAINode(sorted, node->mParent);
                if (parentNodePos == sorted.end() - 1)
                {
                    sorted.push_back(node);
                    parentNodePos = sorted.end() - 1;
                }
                else
                {
                    parentNodePos++;
                    sorted.insert(parentNodePos, node);
                }
                return parentNodePos;
            }
        }

        [[nodiscard]] ResourceID ResolveMesh(Resources* resources, const aiMesh* mesh, const std::string& path, const float meshScale)
        {
            ResourceID resource = { mesh->mName.C_Str() };
            if (resources->Meshes().Find(resource))
                return {};
            MeshData data;
            data.vertices.reserve(mesh->mNumVertices);
            data.name = mesh->mName.C_Str();

            Skeletone& skeletone = data.rig;
            Array<aiNode*> sorted;
            if (mesh->mNumBones > 0)
                sorted.reserve(size_t(mesh->mNumBones) + 1);

            struct JointWeight
            {
                float weight = 0.0f;
                int idx = - 1;
            };

            Array<Array<JointWeight>> weights;
            std::map<std::string, uint32> boneMap;

            for (auto i = 0u; i < mesh->mNumBones; i++)
            {
                const auto* bone = mesh->mBones[i];
                boneMap.insert({ bone->mName.C_Str(), i });
                // We need to make sure that all children nodes are inserted after parent node 
                SortedInsertAINode(sorted, bone->mNode);
            }
            if (sorted.size() > 0)
            {
                weights.resize(mesh->mNumVertices);
                skeletone.NumBones = uint32(sorted.size());
                for (uint32 i = 0; i < skeletone.NumBones; i++)
                {
                    if (i >= MAX_BONES)
                    {
                        skeletone.NumBones = MAX_BONES;
                        nshl_assert2(false, "Asset skeleton num bones more then MAX_BONES");
                        break;
                    }
                    const auto* node = sorted[i];

                    aiVector3D pos;
                    aiVector3D axis;
                    aiVector3D scale;
                    float angle;
                    node->mTransformation.Decompose(scale, axis, angle, pos);
                    pos *= meshScale;

                    skeletone.Bones[i].rotation = Quaternion::RotationFromAxisAngle({ axis.x, axis.y, axis.z }, angle);
                    skeletone.Bones[i].position = { pos.x, pos.y, pos.z };
                    skeletone.Bones[i].Name = node->mName.C_Str();
                    // Look back into array to find the parent node
                    // it can be other then i-1 in case parent has multiple children
                    for (uint32 j = i; j > 0; j--)
                    {
                        if (node->mParent == sorted[j - 1])
                        {
                            skeletone.Bones[i].parent = j - 1;
                            break;
                        }
                    }
                    if (auto boneSearch = boneMap.find(node->mName.C_Str()); boneSearch != boneMap.end())
                    {
                        uint32 boneIndex = boneSearch->second;
                        const auto* bone = mesh->mBones[boneIndex];
                        for (uint32 w = 0; w < bone->mNumWeights; w++)
                        {
                            weights[bone->mWeights[w].mVertexId].push_back({ bone->mWeights[w].mWeight, int(i) });
                        }
                    }
                }
            }

            for (uint32 i = 0; i < weights.size(); i++)
            {
                Array<JointWeight>& vertWeights = weights[i];
                if (vertWeights.size() > 3)
                {
                    std::sort(vertWeights.begin(), vertWeights.end(), [](const JointWeight& a, const JointWeight& b) -> bool
                        {
                            return b.weight < a.weight;
                        }
                    );
                    vertWeights.resize(3);
                    /*
                    float fweights[3] = { vertWeights[0].weight, vertWeights[1].weight, vertWeights[2].weight };
                    const float sum = fweights[0] + fweights[1] + fweights[2];
                    vertWeights[0].weight = fweights[0] / sum;
                    vertWeights[1].weight = fweights[1] / sum;
                    vertWeights[2].weight = fweights[2] / sum;*/
                }

            }

            for (auto i = 0u; i < mesh->mNumVertices; i++)
            {
                Vertex vert;
                vert.position = float3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z) * meshScale;
                vert.normal = float3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);

                if (mesh->mTangents)
                {
                    vert.tangent = float3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);
                }
                if (mesh->mBitangents)
                {
                    vert.bitangent = float3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z);
                }
                vert.uv = float2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
                
                
                if (weights.size() > 0)
                {
                    Array<JointWeight>& vertWeights = weights[i];
                    if (vertWeights.size() > 3)
                    {
                        std::sort(vertWeights.begin(), vertWeights.end(), [](const JointWeight& a, const JointWeight& b) -> bool
                            {
                                return b.weight > a.weight;
                            }
                        );
                        vertWeights.resize(3);
                    }
                    if (vertWeights.size() > 0)
                    {
                        for (uint32 i = 0; i < vertWeights.size(); i++)
                        {
                            vert.weights[i] = vertWeights[i].weight;
                            vert.joints[i] = vertWeights[i].idx;
                        }
                    }
                }

                data.vertices.push_back(std::move(vert));


            }
            const auto numIndices = mesh->mNumFaces * 3;
            data.indices.reserve(numIndices);
            for (auto i = 0u; i < mesh->mNumFaces; i++)
            {
                data.AddTriangle(mesh->mFaces[i].mIndices[0], mesh->mFaces[i].mIndices[1], mesh->mFaces[i].mIndices[2]);
            }
            resources->Meshes().Resolve<Mesh>(resource, data);
            return resource;
        }

        [[nodiscard]] ResourceID ResolveAnimation(Resources* resources, const std::string& path, const aiAnimation* animAsset, const IRigDefs* rig, const float meshScale)
        {
            ResourceID resource = { animAsset->mName.C_Str() };
            if (!rig || resources->Animations().Find(resource))
                return {};


            AnimationData data;
            data.Name = animAsset->mName.C_Str();
            const float sampleTime = animAsset->mTicksPerSecond > 0 ? float(animAsset->mTicksPerSecond) : 60.0f;
            data.Duration = float(animAsset->mDuration) / sampleTime;
            data.Nodes.reserve(animAsset->mNumChannels);
            for (uint32 i = 0; i < animAsset->mNumChannels; i++)
            {
                const aiNodeAnim* aiChan = animAsset->mChannels[i];
                uint32 nodeID = rig->FindNodeIndex(aiChan->mNodeName.C_Str());
                if (nodeID == InvalidID<uint32>())
                {
                    LOG_S("Invalid animation data, couldn't find node %s in animation %s at path %s", aiChan->mNodeName.C_Str(), data.Name.c_str(), path.c_str());
                    continue;
                }
                data.Nodes.push_back({});
                auto& node = data.Nodes[data.Nodes.size() - 1];
                node.boneIDX = nodeID;
                node.Rotations.resize(aiChan->mNumRotationKeys);
                for (uint32 r = 0; r < aiChan->mNumRotationKeys; r++)
                {
                    const auto& rotKey = aiChan->mRotationKeys[r];
                    node.Rotations[r].Time = float(rotKey.mTime) / sampleTime;
                    node.Rotations[r].Value = Quaternion(rotKey.mValue.x, rotKey.mValue.y, rotKey.mValue.z, rotKey.mValue.w);
                }
                node.Positions.resize(aiChan->mNumPositionKeys);
                for (uint32 p = 0; p < aiChan->mNumPositionKeys; p++)
                {
                    const auto& posKey = aiChan->mPositionKeys[p];
                    node.Positions[p].Time = float(posKey.mTime) / sampleTime;
                    node.Positions[p].Value = float3(posKey.mValue.x, posKey.mValue.y, posKey.mValue.z) * meshScale;
                }
            }
            resources->Animations().Resolve<IAnimation>(resource, std::move(data));
            return resource;
        }
    }
    Resources::LoadedAssets Resources::LoadRenderableFromFile(IScene* scene, const std::string& path, const float meshScale)
	{
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* assetsScene = importer.ReadFile(path,
            aiProcess_CalcTangentSpace |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_PopulateArmatureData |
            aiProcess_LimitBoneWeights |
            aiProcess_RemoveRedundantMaterials
            | aiProcess_FindInstances
            | aiProcess_EmbedTextures
            | aiProcess_FlipUVs
            | aiProcess_ConvertToLeftHanded
        );
        // If the import failed, report it
        if (!assetsScene) {
            auto str = importer.GetErrorString();
            LOG_S("Couldn't Load file %s", str);
            return {};
        }

        std::filesystem::path assetPath(path);
        assetPath.remove_filename();
        const std::string assetPathString = assetPath.string();
        LoadedAssets loaded;

        for (uint32 i = 0; i < assetsScene->mNumMaterials; i++)
        {
            loaded.Materials.Add(ResolveMaterial(this, assetsScene->mMaterials[i], assetPathString));
        }
        const IRigDefs* loadedRig = nullptr;
        for (uint32 i = 0; i < assetsScene->mNumMeshes; i++)
        {
            const auto* mesh = assetsScene->mMeshes[i];
            loaded.Meshes.Add(ResolveMesh(this, assetsScene->mMeshes[i], assetPathString, meshScale));
            const auto* mat = assetsScene->mMaterials[assetsScene->mMeshes[i]->mMaterialIndex];

            auto* sceneMesh = Meshes().Find({ mesh->mName.C_Str() });
            auto& renderable = scene->MakeRenderable(sceneMesh, Materials().Find({ mat->GetName().C_Str() }), { ERenderFLag::ERF_CASTS_SHADOW });
            renderable.InitNodes(sceneMesh->GetRig());
            loadedRig = loadedRig ? loadedRig : sceneMesh->GetRig();
            loaded.Renderables.push_back(renderable.GetID());
        }
        for (uint32 i = 0; i < assetsScene->mNumAnimations; i++)
        {
            loaded.Animations.Add(ResolveAnimation(this, assetPathString, assetsScene->mAnimations[i], loadedRig, meshScale));
        }
		return loaded;
	}
    void Resources::LoadedAssets::LoadedAsResource::Add(const ResourceID& res)
    {
        if (res.IsValid())
            Entries.push_back(res);
    }
}
