#include "ResourceHelpers.h"
#include <functional>

namespace nshl
{

	ResourceID::ResourceID(uint64 id, const std::string& name)
		: ID(id), Name(name)
	{
	}
	ResourceID::ResourceID(const std::string& name)
		: Name(name), ID(std::hash<std::string>{}(name))
	{
	}
	ResourceID::ResourceID(uint64 id)
		: ID(id)
	{
	}
	bool operator== (const ResourceID& c1, const ResourceID& c2)
	{
		return (c1.ID == c2.ID);
	}
	bool operator!= (const ResourceID& c1, const ResourceID& c2)
	{
		return !(c1 == c2);
	}
}
