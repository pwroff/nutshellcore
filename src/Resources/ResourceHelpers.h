#pragma once
#include "Core/core.h"
#include <string>
#include <map>
#include <utility>

namespace nshl
{
	struct ResourceID
	{
		const uint64 ID = InvalidID<uint64>();
		const std::string Name = {};

		ResourceID() = default;
		ResourceID(uint64, const std::string& name);
		ResourceID(const std::string& name);
		ResourceID(uint64);
		ResourceID(const ResourceID&) = default;

		operator uint64() { return ID; }

		bool IsValid() const { return ID != InvalidID<uint64>(); }

		friend bool operator== (const ResourceID& c1, const ResourceID& c2);
		friend bool operator!= (const ResourceID& c1, const ResourceID& c2);
	};

	struct ResourceIDCompare
	{
		bool operator() (const ResourceID& lhs, const ResourceID& rhs) const
		{
			return lhs.ID < rhs.ID;
		}
	};

	template<typename Base, typename InitBase>
	struct CacheMap
	{
		using base = typename std::map<ResourceID, std::unique_ptr<Base>, ResourceIDCompare>;
		using baseIterator = typename base::iterator;
	private:
		base m_vals;
	public:
		template<typename T, typename ... Args>
		T* Resolve(const ResourceID& ID, Args&& ... insArg)
		{
			nshl_assert2(ID.IsValid(), "Invalid resource ID");
			if (auto* item = Find(ID))
			{
				return static_cast<T*>(item);
			}

			return Insert<T, Args...>(ID, std::forward<Args>(insArg)...);
		}

		template<typename T, typename ... Args>
		T* Insert(const ResourceID& ID, Args&& ... insArg)
		{
			T* inserted = new T;
			inserted->Init(InitBase::Instance(), std::forward<Args>(insArg)...);
			std::unique_ptr<Base> ins;
			ins.reset(inserted);
			m_vals[ID] = std::move(ins);
			return inserted;
		}

		Base* Find(const ResourceID& ID)
		{
			auto search = m_vals.find(ID);
			if (search != m_vals.end())
			{
				return search->second.get();
			}
			return nullptr;
		}

		template<typename T>
		T* Resolve()
		{
			return Resolve<T>({ T::TypeID });
		}

		baseIterator begin() const { return m_vals.begin(); }
		baseIterator end() const { return m_vals.end(); }
		baseIterator begin() { return m_vals.begin(); }
		baseIterator end() { return m_vals.end(); }
	};
}
