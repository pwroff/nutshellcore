#pragma once
#include "Core/core.h"
#include "Core/Objects.h"
#include <entt/entt.hpp>
#include "Simulation/Physics/PhysWorld.h"

namespace nshl
{
	struct RenderGroup;
	struct Renderable;
	struct Light;
	class IMesh;
	struct IMaterial;

	class IScene : public ICoreObject
	{
		friend RenderGroup;
	protected:
		Array<Renderable> m_Renderables;
		Array<Light> m_Lights;
		Array<RenderGroup*> m_RenderGroups;
		void OnRenderGroupInitialized(RenderGroup* rg);
	public:
		virtual void Init(class Gfx* gfx) = 0;
		virtual void Update(float dt) = 0;
		virtual void OnGui() {};
		Renderable& MakeRenderable(IMesh* mesh, IMaterial* material, Bitfield<uint32> flags = {});
		Array<RenderGroup*>& GetRenderGroups() { return m_RenderGroups; }
		const Renderable* GetRenderable(uint64 ID) const;
		Renderable* UseRenderable(uint64 ID);
		const Array<Light>& GetLights() const { return m_Lights; }
		Array<Light>& UseLights() { return m_Lights; }
		float4x4 ToWorld(const Transform& tr) const;
	};

	class DemoScene : public IScene
	{
		bool m_bSimulatePhysics = false;
		entt::registry registry;
		std::unique_ptr<physics::PhysWorld> m_physWorld;
	public:
		void Init(class Gfx* gfx) override;
		void Update(float dt) override;
		void OnGui() override;
	};
}
