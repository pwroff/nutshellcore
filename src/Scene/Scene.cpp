#include "Scene.h"
#include "Graphics/Renderable.h"
#include "Graphics/Gfx.h"
#include "Graphics/Material/Material.h"
#include "Graphics/Material/PBRMaterial.h"
#include "Graphics/Texture/Texture.h"
#include "Math/math.h"
#include "Simulation/ecs/Systems.h"
#include "Simulation/ecs/Components.h"
#include "imgui.h"
#include "Core/Logger.h"
namespace nshl
{
    void TransformGUI(SceneObject* object)
    {
        auto Pos = object->GetTransform().Position;
        auto Rot = object->GetTransform().GetEulerAngles() * RAD2DEG;

        if (ImGui::DragFloat3("Position", &Pos.x, 0.01f, -100.0f, 100.0f))
            object->SetPosition(Pos);
        if (ImGui::DragFloat3("Rotation", &Rot.x, 1.0f, -360.0f, 360.0f))
        {
            object->SetRotation(DEG2RAD * Rot);
        }
    }
    void CameraGUI(Camera* object)
    {
        ImGui::DragFloat("FOV", &object->m_FOV);
        ImGui::DragFloat("NearPlane", &object->m_NearPlane, 0.01f, 0.001f, 1000.0f);
        ImGui::DragFloat("FarPlane", &object->m_FarPLane, 0.01f, 0.001f, 1000.0f);

    }
    void DemoScene::Init(Gfx* gfx)
	{
        m_physWorld.reset(physics::CreateWorld());
        auto* resources = Resources::Instance();

        auto* redUnlit = resources->Materials().Resolve<UnlitMaterial>({ "RedUnlit" });
        redUnlit->m_Params.Color = { 0.5f, 0.0f, .1f };
        auto* cubeMesh = resources->Meshes().Resolve<Mesh>({ "Cube" }, CreateCubeData(f3One));
        auto* planeMesh = resources->Meshes().Resolve<Mesh>({ "Plane" }, CreatePlaneData({ 10.0f, 10.0f, 1.0f }));

        auto suzyData = MeshData::LoadFromFile("C:/Projects/nutshellcore/assets/meshes/suzy.obj");
        suzyData.name = "Suzzy";

        {
            const auto& loaded = resources->LoadRenderableFromFile(this, "C:/Projects/nutshellcore/assets/meshes/pilot-avatar/source/Pilot_LP_Animated.fbx", 0.1f);

            if (loaded.Renderables.size() > 0 && loaded.Animations.Entries.size() > 1)
            {
                const auto entity = registry.create();
                const auto& data = resources->Animations().Find(loaded.Animations.Entries[0])->GetData();
                Array<AnimationData::TransformKey> keys;
                keys.resize(data.Nodes.size());

                registry.emplace<components::Animated>(entity, &data, 0.0f, std::move(keys));
                registry.emplace<components::VisObject>(entity, loaded.Renderables[0]);
            }
        }

        auto coneData = MeshData::LoadFromFile("C:/Projects/nutshellcore/assets/meshes/cone.obj");
        coneData.name = "Cone";

        auto invCubeData = MeshData::LoadFromFile("C:/Projects/nutshellcore/assets/meshes/cube_inv.obj");
        invCubeData.name = "InvertedCube";

        auto* suzzyMesh = resources->Meshes().Resolve<Mesh>({ "SuzzyMesh" }, suzyData);
        auto* coneMesh = resources->Meshes().Resolve<Mesh>({ "ConeMesh" }, coneData);
        auto* invCube = resources->Meshes().Resolve<Mesh>({ "InvertedCube" }, invCubeData);
        auto* redPBR = resources->Materials().Resolve<DeferredDrawMaterial>({ "RedPBR" });
        redPBR->m_Params.DiffuseAlbedo = { 0.5f, 0.0f, .1f };
        redPBR->SetTexture("Tex_Diffuse", resources->Textures().Resolve<TextureColor>({ "White" }, float4{ 1, 1, 1, 1 }));



        m_Renderables.reserve(500);
        {
            auto* cobleStone = resources->Materials().Resolve<DeferredDrawMaterial>({ "CobleStone" });
            cobleStone->m_Params.DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
            cobleStone->m_Params.UVSCale = { 10, 10 };
            cobleStone->SetTexture("Tex_Diffuse", resources->Textures().Resolve<Texture2D>({ "CobleStoneSeemless" }, "D:/Assets/Textures/Cobblestone/seem_diffuse.png"));
            Renderable& planeRend = MakeRenderable(planeMesh, cobleStone);
            planeRend.SetPosition({ 0.0f, 0.0f, 0.0f });

            physics::BodyCreateDescription bodyDesc;
            bodyDesc.mass = 0.0f;
            physics::BoxColliderDescription boxColl;
            boxColl.size = f3One * 100.0f;
            boxColl.size.y = 0.1f;
            auto bodyId = m_physWorld->CreateBody(planeRend.GetTransform(), bodyDesc, boxColl);
        }
        if (true)
        {
            auto* testWhitePBR = resources->Materials().Resolve<DeferredDrawMaterial>({ "TestWhitePBR" });
            testWhitePBR->m_Params.DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
            testWhitePBR->SetTexture("Tex_Diffuse", resources->Textures().Resolve<TextureColor>({ "White" }, float4{ 1, 1, 1, 1 }));
            Renderable& cubeInside = MakeRenderable(cubeMesh, testWhitePBR, { ERenderFLag::ERF_CASTS_SHADOW });
            cubeInside.m_Transform.Position = float3(2, 0.5, 0);
        }
        if (false)
        {
            auto* testWhitePBR = resources->Materials().Resolve<DeferredDrawMaterial>({ "TestWhitePBR" });
            testWhitePBR->m_Params.DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
            testWhitePBR->SetTexture("Tex_Diffuse", resources->Textures().Resolve<TextureColor>({ "White" }, float4{ 1, 1, 1, 1 }));
            Renderable& plane = MakeRenderable(planeMesh, testWhitePBR, { ERenderFLag::ERF_CASTS_SHADOW });
            //cubeInside.SetScale(f3One * 20.0f);
        }
        if (false) {
           
            auto* depthMat = resources->Materials().Resolve<DepthMaterial>({ "DepthDisplay" });
           
            depthMat->SetTexture("Tex_Diffuse", &resources->Textures().Resolve<ShadowMap>({ "ShadowMap" })->m_aDepthTextures[0]);

            Renderable& planeRend = MakeRenderable(planeMesh, depthMat);
            planeRend.SetPosition({ 0.0f, .0f, 70.0f });
            planeRend.SetScale({ 10, 10, 10 });
            planeRend.SetRotation(float3{ 90, 0, 0 } *DEG2RAD);
        }
        if (false) {
            Renderable& suzy = MakeRenderable(suzzyMesh, redUnlit);
            components::Position pos = { -3.0f, 10.0f, 9.0f };

            const auto entity = registry.create();
            registry.emplace<components::Position>(entity, pos);
            registry.emplace<components::Rotation>(entity);
            //registry.emplace<components::RotationRate>(entity, 0.0f, PI * 0.1f, 0.0f);
            registry.emplace<components::VisObject>(entity, suzy.GetID());
        }
        if (false) {
            Renderable& suzy = MakeRenderable(suzzyMesh, redPBR, { ERenderFLag::ERF_CASTS_SHADOW });
            const auto entity = registry.create();
            components::Position pos = { .0f, 5.0f, .0f };
            registry.emplace<components::Position>(entity, pos);
            registry.emplace<components::Rotation>(entity);
            registry.emplace<components::RotationRate>(entity, 0.0f, PI * 0.1f, 0.0f);
            registry.emplace<components::VisObject>(entity, suzy.GetID());
        }
        std::srand(0);

        static const float3 cubesgrid = {0, 0, 0};
        static const float3 gridOffset = float3(0, 10, 10) + cubesgrid * -0.5f;

        for (uint32 z = 0; z < cubesgrid.z; z++)
            for (uint32 y = 0; y < cubesgrid.y; y++)
                for (uint32 x = 0; x < cubesgrid.x; x++)
                {
                    float3 position = float3{ float(x), float(y), float(z) } + f3One * 0.5f + gridOffset;
                    float randScale = 0.6f + 0.1f * (std::rand() / float(RAND_MAX + 1));
                    Renderable& cubeRend = MakeRenderable(suzzyMesh, redPBR, { ERenderFLag::ERF_CASTS_SHADOW });
                    cubeRend.SetScale(f3One * randScale);
                    float random_variablex = (std::rand() / float(RAND_MAX + 1)) * 2.0f - 1;
                    float random_variabley = (std::rand() / float(RAND_MAX + 1)) * 2.0f - 1;
                    float random_variablez = (std::rand() / float(RAND_MAX + 1)) * 2.0f - 1;
                    cubeRend.SetPosition(position);

                    components::Position pos = { position.x, position.y, position.z };
                    const auto entity = registry.create();
                    registry.emplace<components::Position>(entity, pos);
                    registry.emplace<components::Rotation>(entity);
                    registry.emplace<components::RotationRate>(entity, PI * 0.1f * random_variablex, PI * 0.25f * random_variablez, 0.0f);
                    registry.emplace<components::VisObject>(entity, cubeRend.GetID());

                    physics::BodyCreateDescription bodyDesc;
                    bodyDesc.mass = 20.0f;
                    physics::BoxColliderDescription boxColl;
                    boxColl.size = f3One * randScale;
                    auto bodyId = m_physWorld->CreateBody(cubeRend.GetTransform(), bodyDesc, boxColl);
                    registry.emplace<physics::RigidBody>(entity, bodyId);
                }

        m_Lights.reserve(10);
        
        if (false)
        {
            Light spotLight;
            spotLight.SetType(ELightType::SPOT);
            spotLight.Color = { 1.0f, 1.0f, 0.7f };
            spotLight.FalloffEnd = 15.0f;
            spotLight.SetRotation(float3{ -90.0f, 0.0f, 0.0f } * DEG2RAD);
            spotLight.SetPosition({ 0.0f, 10.0f, 0 });
            spotLight.SetType(ELightType::SPOT);
            spotLight.m_FOV = 95;
            spotLight.m_NearPlane = 1.f;
            m_Lights.push_back(spotLight);

            {
                //Renderable& lightVis = MakeRenderable(coneMesh, Resources::Instance()->Materials().Resolve<WireframeMaterial>({ "WireframeMaterial" }));
                //m_Lights.back().AddChild(&lightVis);
            }
        }
        if (true)
        {
            Light pointLight;
            pointLight.Color = { 0.2f, 1.0f, 1.0f };
            pointLight.FalloffEnd = 30.0f;
            pointLight.SetRotation(float3{ -90.0f, 0.0f, 0.0f } *DEG2RAD);
            pointLight.SetPosition({ 0.0f, 20.f, 2.5f });
            pointLight.SetType(ELightType::POINT);
            pointLight.m_FOV = 90;
            pointLight.m_NearPlane = 1.f;
            pointLight.Bias = 0.004f;
            m_Lights.push_back(pointLight);

            {
                //Renderable& lightVis = MakeRenderable(cubeMesh, Resources::Instance()->Materials().Resolve<WireframeMaterial>({ "WireframeMaterial" }));
                //m_Lights.back().AddChild(&lightVis);
            }
        }
        if (true)
        {
            Light pointLight;
            pointLight.Color = { 1.0f, 1.0f, 0.7f };
            pointLight.FalloffEnd = 30.0f;
            pointLight.SetRotation(float3{ -90.0f, 0.0f, 0.0f } *DEG2RAD);
            pointLight.SetPosition({ 0.0f, 20.f, 5.5f });
            pointLight.SetType(ELightType::POINT);
            pointLight.m_FOV = 90;
            pointLight.m_NearPlane = 1.f;
            pointLight.Bias = 0.004f;
            m_Lights.push_back(pointLight);

            {
                //Renderable& lightVis = MakeRenderable(cubeMesh, Resources::Instance()->Materials().Resolve<WireframeMaterial>({ "WireframeMaterial" }));
                //m_Lights.back().AddChild(&lightVis);
            }
        }

      

        if (false)
        {
            Light directional;
            directional.SetType(ELightType::DIRECTIONAL);
            directional.SetRotation(float3{ -25.0f, 0, 0 } *DEG2RAD);
            directional.SetPosition({ 0.f, 20.0f, -.0f });
            directional.Color = { 1.0f, .8f, 0.8f };
            directional.m_bPerspectiveCamera = false;
            m_Lights.push_back(directional);

            {
                Renderable& lightVis = MakeRenderable(coneMesh, Resources::Instance()->Materials().Resolve<WireframeMaterial>({ "WireframeMaterial" }));
                m_Lights.back().AddChild(&lightVis);
            }

        }

       
	}
    void DemoScene::Update(float dt)
    {
        if (m_bSimulatePhysics)
        {
            m_physWorld->Simulate(dt, registry);
        }
        systems::RotateAtRate(registry, dt);
        systems::AnimateSkeletal(registry, dt);
        systems::SyncRenderables(registry, this);
    }

    void DemoScene::OnGui()
    {
        ImGui::SetNextWindowPos(ImVec2(10, 200), ImGuiCond_FirstUseEver);
        if (ImGui::Begin("Scene", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Checkbox("Simulate", &m_bSimulatePhysics);
            int id = 0;
           
            for (auto& l : m_Lights)
            {
                if (l.Type < ELightType::POINT)
                {
                    continue;
                }
                ImGui::PushID(id);
                id++;
                if (l.Type == ELightType::POINT)
                {
                    ImGui::LabelText("Lights ", "%s", "Point");
                }
                else if (l.Type == ELightType::SPOT)
                {
                    ImGui::LabelText("Lights ", "%s", "Spot");
                }
                else if (l.Type == ELightType::DIRECTIONAL)
                {
                    ImGui::LabelText("Lights ", "%s", "Directional");
                }
                
                TransformGUI(&l);
                CameraGUI(&l);
                ImGui::ColorEdit3("ColorPreview", &l.Color.x);

                if (l.Type == ELightType::POINT)
                {
                    ImGui::DragFloat2("Falloff", &l.FalloffStart);
                }
                else if (l.Type == ELightType::SPOT)
                {
                    ImGui::DragFloat2("Falloff", &l.FalloffStart);
                    ImGui::DragFloat("Power", &l.Power);
                }
                ImGui::DragFloat("ShadowBias", &l.Bias, 0.0001f, -1.0f, 1.0f);
                ImGui::PopID();
            }

            auto drawMatParams = [](int id, PBRMaterial* mat, const char* name)
            {
                ImGui::PushID(id);
                ImGui::LabelText("Materail ", "%s", name);
                ImGui::DragFloat("SpecularShine", &mat->m_Params.SpecularShine);
                ImGui::DragFloat("SpecularHardness", &mat->m_Params.SpecularHardness);
                ImGui::DragFloat("Metalic", &mat->m_Params.Metalic);
                ImGui::PopID();
            };

            auto* testWhitePBR = Resources::Instance()->Materials().Resolve<PBRMaterial>({ "TestWhitePBR" });
            auto* cobleStone = Resources::Instance()->Materials().Resolve<PBRMaterial>({ "CobleStone" });

            drawMatParams(20, testWhitePBR, "TestWhite");
            drawMatParams(21, cobleStone, "CobleStone");
        }
        ImGui::End();
    }
    Renderable& IScene::MakeRenderable(IMesh* mesh, IMaterial* material, Bitfield<uint32> flags)
    {
        m_Renderables.push_back({});
        Renderable& rend = m_Renderables.back();
        rend.m_ID = m_Renderables.size() - 1;
        rend.m_RenderFlags.Set(flags.Value | material->GetFlags());
        if (mesh->HasRig())
        {
            rend.m_RenderFlags.Set(ERenderFLag::ERF_HAS_RIG);
        }
        auto group = RenderGroup::FindRenderGroup(mesh, material, this, rend.m_RenderFlags);
        group->AddRenderable(rend);
        return rend;
    }
    void IScene::OnRenderGroupInitialized(RenderGroup* rg)
    {
        if (m_RenderGroups.Contains(rg))
            return;
        m_RenderGroups.push_back(rg);
    }
    const Renderable* IScene::GetRenderable(uint64 ID) const
    {
        if (m_Renderables.IsValid(ID))
            return &m_Renderables[ID];
        return nullptr;
    }
    Renderable* IScene::UseRenderable(uint64 ID)
    {
        if (m_Renderables.IsValid(ID))
            return &m_Renderables[ID];
        return nullptr;
    }
    float4x4 MatRec(const Transform& tr, const IScene* scene)
    {
        if (tr.Parent == InvalidID<uint64>())
            return tr.AsMatrix();
        return tr.AsMatrix() * MatRec(scene->GetRenderable(tr.Parent)->GetTransform(), scene);
    }
    float4x4 IScene::ToWorld(const Transform& tr) const
    {
        return MatRec(tr, this);
    }
}