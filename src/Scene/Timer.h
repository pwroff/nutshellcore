#pragma once
#include <chrono>
#include <thread>
#include "Core/types.h"

namespace nshl
{
	class Timer
	{
	private:
		std::chrono::time_point<std::chrono::system_clock> m_then;
		std::chrono::milliseconds m_targetFPS;
		std::chrono::milliseconds m_deltaTime;
		std::chrono::milliseconds m_frameTime;


		std::chrono::milliseconds GetWaitTime()
		{
			auto now = std::chrono::system_clock::now();
			m_frameTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_then);
			return m_targetFPS - m_frameTime;
		}

	public:

		Timer(uint32 FPS = 120) :
			m_targetFPS(1000 / FPS),
			m_deltaTime(0),
			m_then(std::chrono::system_clock::now()),
			m_frameTime(0)
		{

		}

		float GetDeltaTime() const
		{
			return float(m_deltaTime.count()) / 1000.0f;
		}

		void StartFrame()
		{
			auto now = std::chrono::system_clock::now();
			m_deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_then);
			m_then = now;
		}

		void EndFrame()
		{
			auto waitTime = GetWaitTime();
			if (waitTime > std::chrono::milliseconds(0))
			{
				std::this_thread::sleep_for(waitTime);
			}
		}

		inline long GetFrameTime() const
		{
			return long(m_frameTime.count());
		}
	};
}
