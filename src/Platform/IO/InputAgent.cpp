#include "InputAgent.h"
#include "Core/core.h"

namespace nshl
{
    void InputAgentWin32::FrameEnd()
	{
		for (uint32 i = 0; i < uint32(InputMapping::Last); ++i)
		{
			m_State.Triggers[i].Value &= 1; // Release all states but IsDown if present
		}

        m_State.ScrollDelta = 0.0f;
        m_State.DeltaPos = { 0, 0 };
	}

	InputMapping GetWinToKey(UCHAR nKey)
	{
        switch (nKey)
        {
        case 'W':
            return InputMapping::W_Key;
        case 'A':
            return InputMapping::A_Key;
        case 'D':
            return InputMapping::D_Key;
        case 'S':
            return InputMapping::S_Key;
        case 'Q':
            return InputMapping::Q_Key;
        case 'E':
            return InputMapping::E_Key;
        case 'R':
            return InputMapping::R_Key;
        case 'F':
            return InputMapping::F_Key;
        case VK_SHIFT:
            return InputMapping::Shift_Key;
        case VK_CONTROL:
            return InputMapping::Ctrl_Key;
        case VK_MENU:
            return InputMapping::Alt_Key;
        case VK_ESCAPE:
            return InputMapping::ESC_Key;
        case VK_LEFT:
            return InputMapping::Left_Key;
        case VK_RIGHT:
            return InputMapping::Right_Key;
        case VK_UP:
            return InputMapping::Up_Key;
        case VK_DOWN:
            return InputMapping::Down_Key;
        case VK_SPACE:
            return InputMapping::Space_Key;
        default:
            return InputMapping::Unknown;
        }
	}
    void InputAgentWin32::UpdateMousePos()
    {
        POINT MousePosition;
        GetCursorPos(&MousePosition);
        auto hwnd = GetActiveWindow();
        ScreenToClient(hwnd, &MousePosition);
        
        float2 currentPosition = { 0, 0 };
        currentPosition.x = static_cast<float>(MousePosition.x);
        currentPosition.y = static_cast<float>(MousePosition.y);

        m_State.DeltaPos += (currentPosition - m_State.CursorPos);
        if (m_State.MouseLock)
        {
            MousePosition.x = int(m_State.CursorPos.x);
            MousePosition.y = int(m_State.CursorPos.y);
            ClientToScreen(hwnd, &MousePosition);
            SetCursorPos(MousePosition.x, MousePosition.y);
            if (!m_State.CursorHidden)
            {
                ShowCursor(false);
                m_State.CursorHidden = true;
            }
        }
        else
        {
            m_State.CursorPos = currentPosition;
            if (m_State.CursorHidden)
            {
                ShowCursor(true);
                m_State.CursorHidden = false;
            }
        }
    }
	bool InputAgentWin32::HandleWindowMessages(WinCallbackParams params)
	{
		auto hWnd = params.wnd;
		auto uMsg = params.message;
		auto wParam = params.wParam;
		auto lParam = params.lParam;

        auto processKeyDown = [this](InputMapping mappedKey)
        {
            if (mappedKey != InputMapping::Unknown)
            {
                m_State.Triggers[uint32(mappedKey)].SetBit(0);
                m_State.Triggers[uint32(mappedKey)].SetBit(1);
            }
        };
        auto processMouseClick = [&](InputMapping mappedKey, bool isDBC = false)
        {
            if (mappedKey != InputMapping::Unknown)
            {
                m_State.Triggers[uint32(mappedKey)].SetBit(0);
                if (!isDBC)
                {
                    m_State.Triggers[uint32(mappedKey)].SetBit(1);
                    return;
                }
                m_State.Triggers[uint32(mappedKey)].ClearBit(1);
                m_State.Triggers[uint32(mappedKey)].SetBit(3);
            }
        };
        auto processMouseRelease = [&](InputMapping mappedKey)
        {
            if (mappedKey != InputMapping::Unknown)
            {
                m_State.Triggers[uint32(mappedKey)].ClearBit(0);
                m_State.Triggers[uint32(mappedKey)].SetBit(2);
            }
        };
        auto processKeyUp = [this](InputMapping mappedKey)
        {
            if (mappedKey != InputMapping::Unknown)
            {
                m_State.Triggers[uint32(mappedKey)].ClearBit(0);
                m_State.Triggers[uint32(mappedKey)].SetBit(2);
            }
        };
        UpdateMousePos();
        switch (uMsg)
        {
        case WM_KEYDOWN:
        {
            processKeyDown(GetWinToKey((UINT)wParam));
            return true;
        }

        case WM_KEYUP:
        {
            processKeyUp(GetWinToKey((UINT)wParam));
            return true;
        }

        case WM_RBUTTONDOWN:
            processMouseClick(InputMapping::RM_Button);
            return true;
        case WM_RBUTTONDBLCLK:
            processMouseClick(InputMapping::RM_Button, true);
            return true;
        case WM_MBUTTONDOWN:
            processMouseClick(InputMapping::MM_Button);
            return true;
        case WM_MBUTTONDBLCLK:
            processMouseClick(InputMapping::MM_Button, true);
            return true;
        case WM_LBUTTONDOWN:
            processMouseClick(InputMapping::LM_Button);
            return true;
        case WM_LBUTTONDBLCLK:
            processMouseClick(InputMapping::LM_Button, true);
            return true;

        case WM_RBUTTONUP:
            processMouseRelease(InputMapping::RM_Button);
            return true;
        case WM_MBUTTONUP:
            processMouseRelease(InputMapping::MM_Button);
            return true;
        case WM_LBUTTONUP:
            processMouseRelease(InputMapping::LM_Button);
            return true;

        case WM_MOUSEWHEEL:
            // Update member var state
            float WheelDelta = (float)((short)HIWORD(wParam)) / (float)WHEEL_DELTA;
            m_State.ScrollDelta += WheelDelta;
            break;
        }
		return false;
	}
}