#pragma once

#include "Core/Objects.h"
#include "Math/math.h"

namespace nshl
{
	enum class InputMapping : uint32
	{
		W_Key = 0,
		A_Key,
		D_Key,
		S_Key,
		Q_Key,
		E_Key,
		R_Key,
		F_Key,
		Shift_Key,
		Ctrl_Key,
		Alt_Key,
		ESC_Key,
		Left_Key,
		Right_Key,
		Up_Key,
		Down_Key,
		Space_Key,
		LM_Button,
		MM_Button,
		RM_Button,





		Unknown,
		Last
	};
	struct TriggerState: protected Bitfield<ubyte>
	{
		friend class InputAgentWin32;
		bool IsDown() const { return IsBitSet(0); }
		bool IsPressed() const { return IsBitSet(1); }
		bool IsReleased() const { return IsBitSet(2); }
		bool IsDoubleClick() const { return IsBitSet(3); }
	};

	struct InputState
	{
		float2 CursorPos = {0, 0};
		float2 DeltaPos = {0, 0};
		float ScrollDelta = 0.f;
		bool MouseLock = false;
		bool CursorHidden = false;
		TriggerState Triggers[uint32(InputMapping::Last)] = {};

		TriggerState Trigger(InputMapping key) const { return Triggers[uint32(key)]; }
	};

	class InputAgent : public Singletone<InputAgent>
	{
	protected:

		virtual const InputState& GetState() const = 0;
		virtual InputState& UseState() = 0;
	public:
		
		static const InputState& State() { return Instance()->GetState(); }
		static void LockMouse() { Instance()->UseState().MouseLock = true; }
		static void UnlockMouse() { Instance()->UseState().MouseLock = false; }
	};

	class InputAgentWin32 : public InputAgent
	{
		InputState m_State;
	protected:
		const InputState& GetState() const override { return m_State; };
		InputState& UseState() override { return m_State; }
		void UpdateMousePos();
	public:
		void FrameEnd();
		bool HandleWindowMessages(struct WinCallbackParams);
	};
}
