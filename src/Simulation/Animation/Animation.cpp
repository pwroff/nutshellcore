#include "Animation.h"

namespace nshl
{

	template<typename T>
	T SampleTimedValue(const Array<TimedKey<T>>& Values, const float time)
	{
		if (Values.size() == 0)
			return {};

		if (time <= Values[0].Time)
			return Values[0].Value;

		for (uint32 i = 1; i < Values.size(); i++)
		{
			if (time > Values[i].Time)
				continue;

			if (time == Values[i].Time)
				return Values[i].Value;

			if (time < Values[i].Time)
			{
				const float alpha = (time - Values[i - 1].Time) / (Values[i].Time - Values[i - 1].Time);
				return lerp_t(Values[i - 1].Value, Values[i].Value, alpha);
			}
		}
		return Values[Values.size() - 1].Value;
	}

	float3 AnimationData::NodeFrame::SamplePosition(const float time) const
	{
		return SampleTimedValue(Positions, time);
	}
	Quaternion AnimationData::NodeFrame::SampleRotation(const float time) const
	{
		return SampleTimedValue(Rotations, time);
	}
	AnimationData::TransformKey AnimationData::Sample(uint32 nodeID, const float time, bool loop) const
	{
		TransformKey key;
		const auto& node = Nodes[nodeID];
		key.boneIDX = node.boneIDX;
		if (time > Duration)
		{
			if (!loop)
			{
				if (node.Positions.size() > 0)
					key.Position = node.Positions[node.Positions.size() - 1].Value;
				if (node.Rotations.size() > 0)
					key.Rotation = node.Rotations[node.Rotations.size() - 1].Value;
				return key;
			}
		}
		const float ts = float(uint32(time * 1000) % uint32(Duration * 1000)) / 1000.0f;
		if (node.Positions.size() > 0)
			key.Position = node.SamplePosition(ts);
		if (node.Rotations.size() > 0)
			key.Rotation = node.SampleRotation(ts);
		return key;
	}
}
