#pragma once
#include "Core/core.h"
#include "Core/Objects.h"
#include "Math/math.h"

namespace nshl
{
	template<typename T>
	struct TimedKey
	{
		T Value = {};
		float Time = 0.0f;
	};

	class Gfx;
	struct AnimationData
	{
		struct TransformKey
		{
			float3 Position;
			uint32 boneIDX = InvalidID<uint32>();
			Quaternion Rotation;
		};
		struct NodeFrame
		{
			uint32 boneIDX = InvalidID<uint32>();
			Array<TimedKey<float3>> Positions;
			Array<TimedKey<Quaternion>> Rotations;

			Quaternion SampleRotation(const float time) const;
			float3 SamplePosition(const float time) const;
		};

		std::string Name = {};
		Array<NodeFrame> Nodes = {};
		float Duration = 0.0f;

		TransformKey Sample(uint32 nodeID, const float time, bool loop) const;
	};

	class IAnimation
	{
		AnimationData m_Data;
	public:
		void Init(const Gfx* gfx, AnimationData&& data)
		{
			m_Data = std::move(data);
		}
		const AnimationData& GetData() const { return m_Data; }
	};
}
