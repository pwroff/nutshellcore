#include "PhysWorld.h"
#include "btBulletDynamicsCommon.h"
#include "BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolverMt.h"
#include "LinearMath/btThreads.h"
#include "Simulation/ecs/Components.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include  <atomic>
#include "Core/Logger.h"
#include "Tracy.hpp"
namespace nshl
{

    class semaphore
    {
        std::mutex m_mutex[2];
        std::condition_variable m_cond[2];
        uint32 m_c = 0;
    public:

        template<typename F, typename...T>
        void use(F func, T&& ... fargs)
        {
            auto currC = m_c;
            auto nextC = (m_c + 1) % 2;
            {
                std::unique_lock<std::mutex> lk(m_mutex[currC]);
                m_cond[currC].wait(lk, [this, currC]() { return m_c == currC; });
                m_c = nextC;
            }
            
            {
                std::lock_guard<std::mutex> lk(m_mutex[nextC]);
                func(std::forward<T>(fargs)...);
            }
            m_cond[nextC].notify_all();
        }
    };

    namespace physics
    {
        class TestWorld : public PhysWorld
        {
            btDefaultCollisionConfiguration* collisionConfiguration = nullptr;
            btCollisionDispatcher* dispatcher = nullptr;
            btBroadphaseInterface* overlappingPairCache = nullptr;
            btSequentialImpulseConstraintSolverMt* solver = nullptr;
            btDiscreteDynamicsWorld* dynamicsWorld = nullptr;
            btAlignedObjectArray<btCollisionShape*> collisionShapes;
            btAlignedObjectArray<btRigidBody*> rigidBodies;
            std::thread m_SimulationThread;
            semaphore m_Semaphore;
            float m_fStepDT = 0.0f;
            float m_fAccumulatedDT = 0.0f;
            std::atomic<bool> m_bRunning = true;
            std::atomic<bool> m_bCanSimulate = false;
            std::atomic<bool> m_bCanSync = true;
        public:
            TestWorld()
            {
            }
            virtual void Init() override
            {
                ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
                collisionConfiguration = new btDefaultCollisionConfiguration();

                ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
                dispatcher = new btCollisionDispatcher(collisionConfiguration);

                ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
                overlappingPairCache = new btDbvtBroadphase();

                ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
                solver = new btSequentialImpulseConstraintSolverMt;

                dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
                m_SimulationThread = std::thread([this] { PhysStepFunc(); });
                auto* sched = btCreateDefaultTaskScheduler();
                btSetTaskScheduler(sched);
            }
            virtual void Simulate(float dt, entt::registry& registry) override
            {
                ZoneScoped;
                {
                    m_fAccumulatedDT += dt;
                    if (!m_bCanSync)
                        return;
                    ZoneScopedN("WaitingToFinishSimulation");
                    while (!m_bCanSync)
                    {
                        // wait
                    }
                }

                {
                    SyncEntitiesSystem(registry);
                    m_fStepDT = m_fAccumulatedDT;
                    m_fAccumulatedDT = 0.0f;
                    m_bCanSimulate = true;
                }
            }

            void PhysStepFunc()
            {
                while (m_bRunning)
                {
                    while (!m_bCanSimulate && m_bRunning)
                    {
                    }
                    {
                        ZoneScopedN("Step simulation");
                        m_bCanSync = false;
                        if (m_fStepDT > 0)
                            dynamicsWorld->stepSimulation(m_fStepDT, 1, 1.0f / 30.0f);
                        m_fStepDT = 0.0f;
                        m_bCanSimulate = false;
                        m_bCanSync = true;
                    }
                }
            }

            void SyncEntitiesSystem(entt::registry& registry) override
            {
                ZoneScoped;
                auto posView = registry.view<const RigidBody, components::Position, components::Rotation>();

                posView.each([this](const RigidBody& rb, components::Position& pos, components::Rotation& rot)
                    {
                        const auto* body = rigidBodies[rb.ID];
                        
                        if (body && body->getMotionState())
                        {
                            btTransform trans;
                            body->getMotionState()->getWorldTransform(trans);
                            pos.x = trans.getOrigin().x();
                            pos.y = trans.getOrigin().y();
                            pos.z = trans.getOrigin().z();
                            trans.getBasis().getEulerZYX(rot.y, rot.x, rot.z);
                        }
                    });
            }
            PhysBodyID CreateBody(const Transform& transform, const BodyCreateDescription& descr, const BoxColliderDescription& desc) override
            {
                btCollisionShape* boxShape = new btBoxShape(btVector3(desc.size.x * 0.5f, desc.size.y * 0.5f, desc.size.z * 0.5f));

                collisionShapes.push_back(boxShape);

                btTransform groundTransform;
                btMatrix3x3 basis;
                auto euler = transform.GetEulerAngles();
                basis.setEulerYPR(euler.y, euler.x, euler.z);
                groundTransform.setBasis(basis);
                groundTransform.setOrigin(btVector3(transform.Position.x, transform.Position.y, transform.Position.z));

                btScalar mass(descr.mass);

                //rigidbody is dynamic if and only if mass is non zero, otherwise static
                bool isDynamic = (mass != 0.f);

                btVector3 localInertia(0, 0, 0);
                if (isDynamic)
                    boxShape->calculateLocalInertia(mass, localInertia);

                //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
                btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
                btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, boxShape, localInertia);
                btRigidBody* body = new btRigidBody(rbInfo);

                //add the body to the dynamics world
                dynamicsWorld->addRigidBody(body);
                rigidBodies.push_back(body);
                return PhysBodyID(rigidBodies.size() - 1);
            }
            ~TestWorld()
            {
                m_bRunning = false;
                if (m_SimulationThread.joinable())
                    m_SimulationThread.join();
                //remove the rigidbodies from the dynamics world and delete them
                for (auto i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
                {
                    btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
                    btRigidBody* body = btRigidBody::upcast(obj);
                    if (body && body->getMotionState())
                    {
                        delete body->getMotionState();
                    }
                    dynamicsWorld->removeCollisionObject(obj);
                    delete obj;
                }

                //delete collision shapes
                for (int j = 0; j < collisionShapes.size(); j++)
                {
                    btCollisionShape* shape = collisionShapes[j];
                    collisionShapes[j] = 0;
                    delete shape;
                }
                //delete dynamics world
                if (dynamicsWorld)
                    delete dynamicsWorld;

                //delete solver
                if (solver)
                    delete solver;

                //delete broadphase
                if (overlappingPairCache)
                    delete overlappingPairCache;

                //delete dispatcher
                if (dispatcher)
                    delete dispatcher;

                if (collisionConfiguration)
                    delete collisionConfiguration;

                collisionShapes.clear();
                rigidBodies.clear();
                if (auto* sched = btGetTaskScheduler())
                {
                    sched->deactivate();
                    delete sched;
                    btSetTaskScheduler(nullptr);
                }
            }
        };

        PhysWorld* CreateWorld()
        {
            auto* world = new TestWorld;
            world->Init();
            return world;
        }
    }
}
