#pragma once
#include "Core/core.h"
#include "Math/math.h"
#include "Simulation/ecs/Systems.h"

namespace nshl
{
	namespace physics
	{
		typedef uint32 PhysBodyID;

		struct BodyCreateDescription
		{
			float mass = 0.0f; // 0 means static body, dynamic otherwise
		};

		struct BoxColliderDescription
		{
			float3 size = f3One;
		};

		class PhysWorld
		{
		public:
			virtual void Init() = 0;
			virtual void Simulate(float dt, entt::registry& registry) = 0;
			virtual PhysBodyID CreateBody(const Transform& transform, const BodyCreateDescription& descr, const BoxColliderDescription& desc) = 0;
			virtual void SyncEntitiesSystem(entt::registry& registry) = 0;
			virtual ~PhysWorld() {}
		};

		PhysWorld* CreateWorld();

		struct RigidBody
		{
			PhysBodyID ID;
		};
	}
}
