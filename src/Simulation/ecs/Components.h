#pragma once
#include "Core/core.h"
#include "Math/math.h"
#include "Simulation/Animation/Animation.h"

namespace nshl
{
	namespace components
	{
		struct VisObject
		{
			uint64 RenderableID = InvalidID<uint64>();
		};

		struct Position: public float3
		{
			using float3::float3;
		};

		struct Rotation : public float3
		{
			using float3::float3;
		};

		struct RotationRate : public float3
		{
			using float3::float3;
		};

		struct Animated
		{
			const AnimationData* animationData = nullptr;
			float CurrentTime = 0.0f;
			Array<AnimationData::TransformKey> Keys;
		};
	}
}