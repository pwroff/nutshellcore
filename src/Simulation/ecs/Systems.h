#pragma once
#include "entt/entt.hpp"

namespace nshl
{
	class IScene;
	namespace systems
	{
		void SyncRenderables(entt::registry& registry, IScene* scene);
		void RotateAtRate(entt::registry& registry, float dt);
		void AnimateSkeletal(entt::registry& registry, float dt);
	}
}
