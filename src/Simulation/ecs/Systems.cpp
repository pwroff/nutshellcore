#include "Systems.h"
#include "Scene/Scene.h"
#include "Graphics/Renderable.h"
#include "Components.h"

namespace nshl
{
	using namespace components;
	namespace systems
	{
		void SyncRenderables(entt::registry& registry, IScene* scene)
		{
			auto posView = registry.view<VisObject, Position>();

			posView.each([scene](const VisObject& vo, const Position& pos)
			{
					if (auto* rend = scene->UseRenderable(vo.RenderableID))
					{
						rend->SetPosition(pos);
					}
			});

			auto rotView = registry.view<VisObject, Rotation>();

			rotView.each([scene](const VisObject& vo, const Rotation& rot)
				{
					if (auto* rend = scene->UseRenderable(vo.RenderableID))
					{
						rend->SetRotation(rot);
					}
				});

			/*
			auto animView = registry.view<VisObject, Animated>();

			animView.each([scene](const VisObject& vo, const Animated& anim)
				{
					if (auto* rend = scene->UseRenderable(vo.RenderableID))
					{
						rend->SetNodeKeys(anim.Keys);
					}
				});
				*/
		}

		void RotateAtRate(entt::registry& registry, float dt)
		{
			auto view = registry.view<Rotation, const RotationRate>();

			for (auto& [entity, rot, rotRate] : view.each()) {
				rot += (rotRate * dt);
			}
		}
		void AnimateSkeletal(entt::registry& registry, float dt)
		{
			/*
			auto view = registry.view<Animated>();
			for (auto& [entity, animData] : view.each()) {
				animData.CurrentTime += dt;
				for (uint32 i = 0; i < animData.Keys.size(); i++)
				{
					animData.Keys[i] = animData.animationData->Sample(i, animData.CurrentTime, true);
				}
			}
			*/
		}
	}
}
