#include "matrix_op.fxh"
#include "common_buffers.fxh"
#include "lighting.fxh"


Texture2D    Tex_Diffuse;
SamplerState Tex_Diffuse_sampler; // By convention, texture samplers must use the '_sampler' suffix

cbuffer MaterialConstants
{
    PBRMaterial material;
};

struct VSOutput
{
    float4 Pos : SV_POSITION;
    float2 UV : TEX_COORD;
    float3 NormalWS : NORMAL;
    float3 PosWS: POSITION;
};

// Note that if separate shader objects are not supported (this is only the case for old GLES3.0 devices), vertex
// shader output variable name must match exactly the name of the pixel shader input variable.
// If the variable has structure type (like in this example), the structure declarations must also be indentical.
void vert(in VSInput VSIn,
    out VSOutput PSIn)
{
    // HLSL matrices are row-major while GLSL matrices are column-major. We will
    // use convenience function MatrixFromRows() appropriately defined by the engine
    float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));

    float4 pos = float4(VSIn.Pos, 1);

    float3 normal = VSIn.Normal;
    // Skinning
    if (VSIn.Joints[0] != -1)
    {
        float3 totalPosition = 0;
        for (int i = 0; i < 3; i++)
        {
            if (VSIn.Joints[i] != -1)
            {
                float3x4 boneTransform = transpose(g_Skeletones[VSIn.BatchID].Bones[VSIn.Joints[i]]);
                float3 localPosition = mul(boneTransform, VSIn.Pos);
                totalPosition += localPosition * VSIn.Weights[i];
                //float3 localNormal = mat3(finalBonesMatrices[boneIds[i]]) * norm;
                //m += boneTransform;
            }
        }
        //normal = normalize(mul((float3x3)m, normal));
        pos = float4(totalPosition, 1);
    }
    // Apply rotation
    float4 TransformedPos = mul(pos, InstanceMatr);
    float3x3 orient =
    {
        VSIn.Row0, VSIn.Row1, VSIn.Row2
    };
    // Apply view-projection matrix
    PSIn.Pos = mul(mul(TransformedPos, g_View), g_Proj);
    PSIn.UV = VSIn.UV;
    PSIn.NormalWS = mul(transpose(orient), normal);
    PSIn.PosWS = float3(TransformedPos.xyz);
}

struct PSOutput
{
    float4 Color : SV_TARGET0;
    float4 Position : SV_TARGET1;
    float4 Normal : SV_TARGET2;
    float4 Material : SV_TARGET3;
};

void frag(in VSOutput PSIn,
    out PSOutput PSOut)
{
    float4 DiffuseAlbedo = material.DiffuseAlbedo * Tex_Diffuse.Sample(Tex_Diffuse_sampler, (PSIn.UV * material.UVScale));
    PSOut.Color = DiffuseAlbedo;
    PSOut.Position = float4(PSIn.PosWS, 1);
    PSOut.Normal = float4(PSIn.NormalWS, 1);
    float4 mat;
    mat.x = material.SpecularShine;
    mat.y = material.SpecularHardness;
    mat.z = material.Metalic;
    mat.w = material.Else;
    PSOut.Material = mat;
}
