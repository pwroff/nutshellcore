#include "common_buffers.fxh"

struct VSOutput
{
    float4 Pos : SV_POSITION;
};

// Note that if separate shader objects are not supported (this is only the case for old GLES3.0 devices), vertex
// shader output variable name must match exactly the name of the pixel shader input variable.
// If the variable has structure type (like in this example), the structure declarations must also be indentical.
void vert(in  VSInput VSIn,
    out VSOutput PSIn)
{
    // HLSL matrices are row-major while GLSL matrices are column-major. We will
 // use convenience function MatrixFromRows() appropriately defined by the engine
    float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));
    // Apply rotation
    float4 TransformedPos = mul(float4(VSIn.Pos, 1.0), InstanceMatr);
    // Apply view-projection matrix
    PSIn.Pos = mul(TransformedPos, g_ViewProj);
}
