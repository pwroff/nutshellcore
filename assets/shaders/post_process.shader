#include "common_buffers.fxh"
#include "matrix_op.fxh"
#include "lighting.fxh"

Texture2D    Tex_Shadowmap[MAX_SHADOW_MAPS];
SamplerState Tex_Shadowmap_sampler; // By convention, texture samplers must use the '_sampler' suffix

void vert(in  PPVSInput VSIn,
          out PPVSOutput PSIn)
{
    float4 Pos[4];
    Pos[0] = float4(-1.0, -1.0, 0.0, 1.0);
    Pos[1] = float4(-1.0, +1.0, 0.0, 1.0);
    Pos[2] = float4(+1.0, -1.0, 0.0, 1.0);
    Pos[3] = float4(+1.0, +1.0, 0.0, 1.0);

    float2 UV[4];
    UV[0] = float2(+0.0, +1.0);
    UV[1] = float2(+0.0, +0.0);
    UV[2] = float2(+1.0, +1.0);
    UV[3] = float2(+1.0, +0.0);

    PSIn.Pos = Pos[VSIn.VertexID];
    PSIn.UV = UV[VSIn.VertexID];
    float4 posw = mul(PSIn.Pos, g_InvViewProj);
    posw.w = 1.0 / posw.w;
    posw.x *= posw.w;
    posw.y *= posw.w;
    posw.z *= posw.w;
    PSIn.PosWSA = posw.xyz;
    posw = mul(float4(PSIn.Pos.xy, 1.0, 1.0), g_InvViewProj);
    posw.w = 1.0 / posw.w;
    posw.x *= posw.w;
    posw.y *= posw.w;
    posw.z *= posw.w;
    PSIn.PosWSB = posw.xyz;
}


Texture2D    Tex_PrevRTV;
Texture2D    Tex_DSV;
SamplerState    Tex_PrevRTV_sampler;

struct PSOutput
{
    float4 Color : SV_TARGET;
};

float CalculateFog(float d)
{
    const float start = 0.989;
    const float fogIntensity = 0.1f;
    d = saturate((d - start) / (1 - start));
    // Linear falloff.
    return saturate(d * d * d) * fogIntensity;
}

float random(float2 p)
{
    float2 K1 = float2(
        23.14069263277926, // e^pi (Gelfond's constant)
        2.665144142690225 // 2^sqrt(2) (Gelfond–Schneider constant)
    );
    return frac(cos(dot(p, K1)) * 12345.6789);
}

struct Ray
{
    float3 o;		// origin
    float3 d;		// direction
};

struct Sphere
{
    float r;	// radius
    float3 o;		// center position
};

struct Cone
{
    float cosa;	// half cone angle
    float h;	// height
    float3 c;		// tip position
    float3 v;		// axis
};


bool RaySphereIntersection(Sphere s, Ray r, out float3 p1, out float3 p2)
{
    float t = dot(s.o - r.o, r.d);
    float3 p = r.o + r.d * t;
    float y = length(s.o - p);
    if (y > s.r)
        return false;
    float x = sqrt(s.r * s.r - y * y);
    float t1 = t - x;
    float t2 = t + x;
    p1 = r.o + r.d * t1;
    p2 = r.o + r.d * t2;
    return dot(p1 - r.o, r.d) > 0 || dot(p2 - r.o, r.d) > 0;
}

bool RayConeIntersection(Cone s, Ray r, out float3 p1, out float3 p2)
{
    float3 co = r.o - s.c;

    float a = dot(r.d, s.v) * dot(r.d, s.v) - s.cosa * s.cosa;
    float b = 2. * (dot(r.d, s.v) * dot(co, s.v) - dot(r.d, co) * s.cosa * s.cosa);
    float c = dot(co, s.v) * dot(co, s.v) - dot(co, co) * s.cosa * s.cosa;

    float det = b * b - 4. * a * c;
    if (det <= 0.) return false;

    det = sqrt(det);
    float t1 = (-b - det) / (2. * a);
    float t2 = (-b + det) / (2. * a);

    // This is a bit messy; there ought to be a more elegant solution.
    float t = t1;
    if (t < 0. || t2 > 0. && t2 < t) t = t2;
    if (t < 0.) return false;

    float3 cp = r.o + t * r.d - s.c;
    float h = dot(cp, s.v);
    if (h < 0. || h > s.h) return false;

    p1 = r.o + r.d * t1;
    p2 = r.o + r.d * t2;

    return dot(p1 - r.o, r.d) > 0 || dot(p2 - r.o, r.d) > 0;
}

float3 CalculateVolumetrics(float3 fragWS, float3 fragWSB, float d)
{
    float3 color = float4(0, 0, 0, 0);
    const int maxSteps = 400;
    const float maxDistance = 500.0f;
    Ray ray;
    ray.o = fragWS;
    ray.d = normalize(fragWSB - fragWS);
    float3 eyePos = extract_position(g_InvView);
    float3 poi1 = float3(0, 0, 0);
    float3 poi2 = float3(0, 0, 0);
    float overallIntensity = 0.5;
    for (int i = 0; i < MAX_LIGHTS; i++)
    {
        if (g_Lights[i].Properties.x < 1)
        {
            break;
        }
        float3 lightPos = g_Lights[i].Position;
        Sphere sphere;
        sphere.o = lightPos;
        sphere.r = g_Lights[i].FalloffEnd;
        Cone cone;
        cone.c = lightPos;
        cone.h = g_Lights[i].FalloffEnd;
        float r = cone.h;
        float s = sqrt(cone.h * cone.h + r * r);
        float sina = 2 * asin(r / s);
        cone.cosa = 0.9;
        cone.v = g_Lights[i].Direction;

        if (RaySphereIntersection(sphere, ray, poi1, poi2))
        {
            float3 rayThrough = poi2 - poi1;
            float rLength = length(rayThrough);
            rayThrough /= rLength;
            int pointsThroughRay = min(40 * rLength, 300);
            for (float j = 0; j < pointsThroughRay; j++)
            {
                float3 particlePos = poi1 + rayThrough * (j / float(pointsThroughRay - 1)) * rLength;

                float4 depthPosProj = mul(float4(particlePos, 1.0f), g_ViewProj);
                float3 depthCoord = depthPosProj.xyz / depthPosProj.w;

                if (d < depthCoord.z)
                    continue;

                float3 pointDir = particlePos - lightPos;
                float disanceToLight = length(pointDir);
                pointDir /= disanceToLight;
                float3 toEye = normalize(particlePos - eyePos);

                float shadowMult = 1.0;

                float4 shadowPosProj = mul(float4(particlePos, 1.0f), g_CasterViewProjections[g_Lights[i].Properties.y]);
                float3 shadowCoord = shadowPosProj.xyz / shadowPosProj.w;
                float2 sampleCoords = float2(shadowCoord.xy);
                float depth = shadowCoord.z;
                if (abs(sampleCoords.x) <= 1.0 && abs(sampleCoords.y) <= 1.0 && depth <= 1.0 && depth >= 0.0)
                {
                    sampleCoords.x = sampleCoords.x * 0.5 + 0.5;
                    sampleCoords.y = sampleCoords.y * -0.5 + 0.5;
                    float shadow = Tex_Shadowmap[g_Lights[i].Properties.y].Sample(Tex_Shadowmap_sampler, sampleCoords.xy).r;
                    float bias = g_Lights[i].LightCustomProps.x;
                    if (shadow < shadowCoord.z)
                    {
                        shadowMult = 0.0;
                        j += 3;
                    }
                }
                float distanceAlpha = 1 - (disanceToLight / g_Lights[i].FalloffEnd);
                distanceAlpha *= distanceAlpha;
                float scattering = (0.01f + 0.01f * random(float2(particlePos.xy) * 33.0));
                float normalIntensity = saturate(dot(pointDir, toEye));
                color += g_Lights[i].Strength * distanceAlpha * scattering * overallIntensity * normalIntensity * shadowMult;

            }
            /*
            float3 particlePos = poi1;
            float3 pointDir = particlePos - lightPos;
            float disanceToLight = length(pointDir);
            pointDir /= disanceToLight;
            float3 toEye = normalize(eyePos - lightPos);
            color += (g_Lights[i].Strength * ((disanceToLight) / g_Lights[i].FalloffEnd)) * saturate(dot(pointDir, toEye)) * (0.05f + 0.01f * random(float2(particlePos.xy) * 33.0));
            */
          //  float3 pointDir = normalize(poi1 - g_Lights[i].Position);
            //float3 toEye = normalize(g_Lights[i].Position - eyePos);
            //color += g_Lights[i].Strength * 0.1f * abs(dot(pointDir, toEye));
        }
        /*
        float disanceToLight = length(g_Lights[i].Position - posWS);
        float radius = g_Lights[i].FalloffEnd;
        if (disanceToLight <= radius)
        {
            color += (g_Lights[i].Strength * ((disanceToLight * 0.5) / radius)) * (0.05f + 0.01f * random(float2(fragWS.xy) * 33.0));
            return color;
        }
        */
    }
    return saturate(color);
}

void frag(in PPVSOutput PSIn,
    out PSOutput PSOut)
{
    float d = Tex_DSV.Sample(Tex_PrevRTV_sampler, PSIn.UV).r;
    float fog = CalculateFog(d);
    PSOut.Color = Tex_PrevRTV.Sample(Tex_PrevRTV_sampler, PSIn.UV) * (1 - fog) + float4(0.2, 0.2, 0.25, 1) * fog;

    PSOut.Color.xyz += CalculateVolumetrics(PSIn.PosWSA, PSIn.PosWSB, d);


}
