#include "matrix_op.fxh"
#include "common_buffers.fxh"

cbuffer MaterialConstants
{
};

Texture2D    Tex_Diffuse;
SamplerState Tex_Diffuse_sampler; // By convention, texture samplers must use the '_sampler' suffix

struct VSOutput
{
    float4 Pos : SV_POSITION;
    float2 UV  : TEX_COORD;
};

void vert(in  VSInput VSIn,
    out VSOutput PSIn)
{
       // HLSL matrices are row-major while GLSL matrices are column-major. We will
    // use convenience function MatrixFromRows() appropriately defined by the engine
	float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));
    // Apply rotation
	float4 TransformedPos = mul(float4(VSIn.Pos, 1.0), InstanceMatr);
    // Apply view-projection matrix
    PSIn.Pos = mul(mul(TransformedPos, g_View), g_Proj);
    PSIn.UV = VSIn.UV;
}

struct PSOutput
{
    float4 Color : SV_TARGET;
};

void frag(in  VSOutput  PSIn,
    out PSOutput PSOut)
{
    float4 Color = Tex_Diffuse.Sample(Tex_Diffuse_sampler, PSIn.UV);
    PSOut.Color = Color.xxxx;
}
