#include "common_buffers.fxh"
#include "matrix_op.fxh"
#include "lighting.fxh"

#define NUM_G_BUFFERS int(4)

Texture2D    Tex_GBuffers[NUM_G_BUFFERS];
SamplerState Tex_GBuffers_sampler; // By convention, texture samplers must use the '_sampler' suffix

Texture2D    Tex_Shadowmap[MAX_SHADOW_MAPS];
SamplerState Tex_Shadowmap_sampler; // By convention, texture samplers must use the '_sampler' suffix

struct VSOutput
{
    float4 Pos : SV_POSITION;
    float2 UV : TEX_COORD;
};

void vert(in  PPVSInput VSIn,
    out VSOutput PSIn)
{
    float4 Pos[4];
    Pos[0] = float4(-1.0, -1.0, 0.0, 1.0);
    Pos[1] = float4(-1.0, +1.0, 0.0, 1.0);
    Pos[2] = float4(+1.0, -1.0, 0.0, 1.0);
    Pos[3] = float4(+1.0, +1.0, 0.0, 1.0);

    float2 UV[4];
    UV[0] = float2(+0.0, +1.0);
    UV[1] = float2(+0.0, +0.0);
    UV[2] = float2(+1.0, +1.0);
    UV[3] = float2(+1.0, +0.0);

    PSIn.Pos = Pos[VSIn.VertexID];
    PSIn.UV = UV[VSIn.VertexID];
}


struct PSOutput
{
    float4 Color : SV_TARGET;
};

float4 ComputeLighting(PBRMaterial mat,
    float3 pos, float3 normal, float3 toEye)
{
    float3 result = 0.0f;
    bool bshadow = false;
    for (int i = 0; i < MAX_LIGHTS; i++)
    {
        if (g_Lights[i].Properties.x < 1)
        {
            break;
        }
        float4 shadowPosProj = mul(float4(pos, 1.0f), g_CasterViewProjections[g_Lights[i].Properties.y]);
        float3 shadowCoord = shadowPosProj.xyz / shadowPosProj.w;
        float2 sampleCoords = float2(shadowCoord.xy);
        float depth = shadowCoord.z;
        if (abs(sampleCoords.x) <= 1.0 && abs(sampleCoords.y) <= 1.0 && depth <= 1.0 && depth >= 0.0)
        {
            sampleCoords.x = sampleCoords.x * 0.5 + 0.5;
            sampleCoords.y = sampleCoords.y * -0.5 + 0.5;
            float shadow = Tex_Shadowmap[g_Lights[i].Properties.y].Sample(Tex_Shadowmap_sampler, sampleCoords.xy).r;
            float bias = g_Lights[i].LightCustomProps.x;
            if (shadow < (shadowCoord.z - bias))
            {
                continue;
            }
        }
        if (g_Lights[i].Properties.x < 2)
        {
            result += ComputePointLight(g_Lights[i], mat, pos, normal, toEye);
        }
        else if (g_Lights[i].Properties.x < 3)
        {
            result += ComputeSpotLight(g_Lights[i], mat, pos, normal, toEye);
        }
        else if (g_Lights[i].Properties.x < 4)
        {
            result += ComputeDirectionalLight(g_Lights[i], mat, normal, toEye);
        }
    }
    result = saturate(result);
    return float4(result, 0.0f);
}

void frag(in VSOutput PSIn,
    out PSOutput PSOut)
{
    float4 DiffuseAlbedo = Tex_GBuffers[0].Sample(Tex_GBuffers_sampler, PSIn.UV);
    float3 posWS = Tex_GBuffers[1].Sample(Tex_GBuffers_sampler, PSIn.UV).xyz;
    float3 normal = normalize(float3(Tex_GBuffers[2].Sample(Tex_GBuffers_sampler, PSIn.UV).xyz));
    float3 toEyeW = normalize(posWS - float3(g_View[3].xyz));
    float4 matSample = Tex_GBuffers[3].Sample(Tex_GBuffers_sampler, PSIn.UV);

    PBRMaterial mat;
    mat.SpecularShine = matSample.x;
    mat.SpecularHardness = matSample.y;
    mat.Metalic = matSample.z;
    mat.Else = matSample.w;

    float4 ambient = DiffuseAlbedo * (0.005 + abs(dot(float3(0, 0, 1), normal)) * 0.005);

    float4 LightColor = ComputeLighting(mat, posWS, normal, toEyeW);

    PSOut.Color = saturate(ambient + DiffuseAlbedo * LightColor + DiffuseAlbedo * length(LightColor));
}

