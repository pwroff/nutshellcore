#include "common_buffers.fxh"

struct VSOutput
{
    float4 Pos : SV_POSITION;
};

// Note that if separate shader objects are not supported (this is only the case for old GLES3.0 devices), vertex
// shader output variable name must match exactly the name of the pixel shader input variable.
// If the variable has structure type (like in this example), the structure declarations must also be indentical.
void vert(in  VSInput VSIn,
    out VSOutput PSIn)
{
       // HLSL matrices are row-major while GLSL matrices are column-major. We will
    // use convenience function MatrixFromRows() appropriately defined by the engine
	float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));
    // Apply rotation
	float4 TransformedPos = mul(float4(VSIn.Pos, 1.0), InstanceMatr);
    // Apply view-projection matrix
    PSIn.Pos = mul(mul(TransformedPos, g_View), g_Proj);
}

struct GSOutput
{
    VSOutput VSOut;
    float3 DistToEdges : DIST_TO_EDGES;
};


struct PSOutput
{
    float4 Color : SV_TARGET;
};


[maxvertexcount(3)]
void geom(triangle VSOutput In[3],
    inout TriangleStream<GSOutput> triStream)
{
    // Compute screen-space position of every vertex
    float2 v0 = g_ViewportSize.xy * In[0].Pos.xy / In[0].Pos.w;
    float2 v1 = g_ViewportSize.xy * In[1].Pos.xy / In[1].Pos.w;
    float2 v2 = g_ViewportSize.xy * In[2].Pos.xy / In[2].Pos.w;
    float2 edge0 = v2 - v1;
    float2 edge1 = v2 - v0;
    float2 edge2 = v1 - v0;
    // Compute triangle area
    float area = abs(edge1.x * edge2.y - edge1.y * edge2.x);

    GSOutput Out;

    Out.VSOut = In[0];
    // Distance to edge0
    Out.DistToEdges = float3(area / length(edge0), 0.0, 0.0);
    triStream.Append(Out);

    Out.VSOut = In[1];
    // Distance to edge1
    Out.DistToEdges = float3(0.0, area / length(edge1), 0.0);
    triStream.Append(Out);

    Out.VSOut = In[2];
    // Distance to edge2
    Out.DistToEdges = float3(0.0, 0.0, area / length(edge2));
    triStream.Append(Out);
}

void frag(in  GSOutput  PSIn,
    out PSOutput PSOut)
{
    float4 Color = float4(1.0, 1.0, 1.0, 1.0);
    // Compute distance to the closest edge
    float minDist = min(PSIn.DistToEdges.x, PSIn.DistToEdges.y);
    minDist = min(minDist, PSIn.DistToEdges.z);
    float lineWidth = 1.0;
    
    float lineIntensity = saturate((lineWidth - minDist) / lineWidth);
    clip( lineIntensity - 0.01 );
    float3 EdgeColor = float3(0.0, 0.0, 0.0);
    Color.rgb = lerp(Color.rgb, EdgeColor, lineIntensity);
    PSOut.Color = Color;
}
