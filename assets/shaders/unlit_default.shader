#include "matrix_op.fxh"
#include "common_buffers.fxh"

cbuffer MaterialConstants
{
	float3 g_Tint;
};

struct VSOutput
{
    float4 Pos : SV_POSITION;
    float2 UV  : TEX_COORD;
    float3 NormalWS : NORMAL;
};

// Note that if separate shader objects are not supported (this is only the case for old GLES3.0 devices), vertex
// shader output variable name must match exactly the name of the pixel shader input variable.
// If the variable has structure type (like in this example), the structure declarations must also be indentical.
void vert(in  VSInput VSIn,
    out VSOutput PSIn)
{
       // HLSL matrices are row-major while GLSL matrices are column-major. We will
    // use convenience function MatrixFromRows() appropriately defined by the engine
	float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));
    // Apply rotation
	float4 TransformedPos = mul(float4(VSIn.Pos, 1.0), InstanceMatr);
    // Apply view-projection matrix
    PSIn.Pos = mul(mul(TransformedPos, g_View), g_Proj);
    PSIn.UV = VSIn.UV;
    PSIn.NormalWS = mul(extract_rotation_matrix(InstanceMatr), VSIn.Normal);
}

struct PSOutput
{
    float4 Color : SV_TARGET;
};

void frag(in  VSOutput  PSIn,
    out PSOutput PSOut)
{
    float4 Color = float4(g_Tint, 1.0f) * clamp(((PSIn.NormalWS.y + 1) * 0.5f), 0.5, 1);
    Color.rgb += clamp(PSIn.NormalWS.y + 0.75, -0.015, 0);
    PSOut.Color = Color;
}
