#include "matrix_op.fxh"
#include "common_buffers.fxh"
#include "lighting.fxh"


Texture2D    Tex_Diffuse;
SamplerState Tex_Diffuse_sampler; // By convention, texture samplers must use the '_sampler' suffix

Texture2D    Tex_Shadowmap[MAX_SHADOW_MAPS];
SamplerState Tex_Shadowmap_sampler; // By convention, texture samplers must use the '_sampler' suffix

cbuffer MaterialConstants
{
    PBRMaterial material;
};

struct VSOutput
{
    float4 Pos : SV_POSITION;
    float2 UV : TEX_COORD;
    float3 NormalWS : NORMAL;
    float3 FragPosWS: POSITION1;
    float3 EyePosWS: POSITION2;
};

// Note that if separate shader objects are not supported (this is only the case for old GLES3.0 devices), vertex
// shader output variable name must match exactly the name of the pixel shader input variable.
// If the variable has structure type (like in this example), the structure declarations must also be indentical.
void vert(in VSInput VSIn,
    out VSOutput PSIn)
{
       // HLSL matrices are row-major while GLSL matrices are column-major. We will
    // use convenience function MatrixFromRows() appropriately defined by the engine
    float4x4 InstanceMatr = MatrixFromRows(float4(VSIn.Row0, 0.0), float4(VSIn.Row1, 0.0), float4(VSIn.Row2, 0.0), float4(VSIn.Row3, 1.0));
    // Apply rotation
    float4 TransformedPos = mul(float4(VSIn.Pos, 1.0), InstanceMatr);
    float3x3 orient = 
    {
        VSIn.Row0, VSIn.Row1, VSIn.Row2
    };
    // Apply view-projection matrix
    PSIn.Pos = mul(mul(TransformedPos, g_View), g_Proj);
    PSIn.FragPosWS = TransformedPos.xyz;
    PSIn.EyePosWS = float3(g_View[3].xyz);
    PSIn.UV = VSIn.UV;
    PSIn.NormalWS = mul(transpose(orient), VSIn.Normal);
}

float4 ComputeLighting(PBRMaterial mat,
                       float3 pos, float3 normal, float3 toEye)
{
    float3 result = 0.0f;
    bool bshadow = false;
    for (int i = 0; i < MAX_LIGHTS; i++)
    {
        if (g_Lights[i].Properties.x < 1)
        {
            break;
        }
        float4 shadowPosProj = mul(float4(pos, 1.0f), g_CasterViewProjections[g_Lights[i].Properties.y]);
        float3 shadowCoord = shadowPosProj.xyz / shadowPosProj.w;
        float2 sampleCoords = float2(shadowCoord.xy);
        float depth = shadowCoord.z;
        if (abs(sampleCoords.x) <= 1.0 && abs(sampleCoords.y) <= 1.0 && depth <= 1.0 && depth >= 0.0)
        {
            sampleCoords.x = sampleCoords.x * 0.5 + 0.5;
            sampleCoords.y = sampleCoords.y * -0.5 + 0.5;
            float shadow = Tex_Shadowmap[g_Lights[i].Properties.y].Sample(Tex_Shadowmap_sampler, sampleCoords.xy).r;
            float bias = g_Lights[i].LightCustomProps.x;
            if (shadow < (shadowCoord.z - bias))
            {
                continue;
            }
        }
        if (g_Lights[i].Properties.x < 2)
        {
            result += ComputePointLight(g_Lights[i], mat, pos, normal, toEye);
        }
        else if (g_Lights[i].Properties.x < 3)
        {
            result += ComputeSpotLight(g_Lights[i], mat, pos, normal, toEye);
        }
        else if (g_Lights[i].Properties.x < 4)
        {
            result += ComputeDirectionalLight(g_Lights[i], mat, normal, toEye);
        }
    }
    result = saturate(result);
    return float4(result, 0.0f);
}

struct PSOutput
{
    float4 Color : SV_TARGET;
};

void frag(in VSOutput PSIn,
    out PSOutput PSOut)
{
    float3 normal = normalize(PSIn.NormalWS);
    float3 toEyeW = normalize(PSIn.FragPosWS - PSIn.EyePosWS);
    float4 DiffuseAlbedo = material.DiffuseAlbedo * Tex_Diffuse.Sample(Tex_Diffuse_sampler, (PSIn.UV * material.UVScale));
    float4 ambient = DiffuseAlbedo * (0.005 + abs(dot(float3(0, 0, 1), normal)) * 0.005);

    float4 LightColor = ComputeLighting(material, PSIn.FragPosWS, normal, toEyeW);

    PSOut.Color = saturate(ambient + DiffuseAlbedo * LightColor + DiffuseAlbedo * length(LightColor));
}
