cbuffer CameraConstants
{
    float4x4 g_View;
    float4x4 g_Proj;
    float4x4 g_ViewProj;
    float4x4 g_InvView;
    float4x4 g_InvViewProj;
    float4 g_ViewDir;
    float4 g_CamParams;
    float4 g_ViewportSize;
};

struct VSInput
{
    float3 Pos : ATTRIB0;
    float3 Normal : ATTRIB1;
    float3 Tangent: ATTRIB2;
    float3 BiTangent: ATTRIB3;
    float3 Weights: ATTRIB4;
    int3 Joints: ATTRIB5;
    float2 UV : ATTRIB6;

    float3 Row0 : ATTRIB7;
    float3 Row1 : ATTRIB8;
    float3 Row2 : ATTRIB9;
    float3 Row3 : ATTRIB10;
    int BatchID : ATTRIB11;
};

#define MAX_BONES int(75)
#define MAX_SKELETONES int(5)

struct Skeletone
{
    float4x3 Bones[MAX_BONES];
};

cbuffer RenderedSkeletones
{
    Skeletone g_Skeletones[MAX_SKELETONES];
};

// Post Process VSInput
struct PPVSInput
{
    uint VertexID : SV_VertexID;
};

// Post Process PSInput
struct PPVSOutput
{
    float4 Pos    : SV_POSITION;
    float3 PosWSA    : POSITION1;
    float3 PosWSB    : POSITION2;
    float2 UV     : TEX_COORD;
};

struct Light
{
    float3 Strength; // color
    float FalloffStart; // point/spot light only
    float3 Direction;   // directional/spot light only
    float FalloffEnd;   // point/spot light only
    float3 Position;    // point light only
    float SpotPower;    // spot light only
    float4 LightCustomProps;
    uint4 Properties;
};

#define MAX_LIGHTS int(20)
#define MAX_SHADOW_MAPS int(30)

cbuffer LightConstants
{
    Light g_Lights[MAX_LIGHTS];
    float4x4 g_CasterViewProjections[MAX_SHADOW_MAPS];
};
