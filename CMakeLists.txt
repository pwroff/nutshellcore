cmake_minimum_required (VERSION 3.9)

project(Nutshell CXX)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

function(file_list_recurse outVar)
	file(GLOB_RECURSE relPaths RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" ${ARGN})
	set(${outVar} ${relPaths} PARENT_SCOPE)
endfunction()

file_list_recurse(Common_FILES
	"src/*.cpp"
	"src/*.inl"
	"src/*.h"
    "entt/src/*"
    "assets/shaders/*.shader"
    "assets/shaders/*.fxh"
    "assets/meshes/*.fbx"
)

add_executable(Nutshell WIN32 ${Common_FILES} "tracy/TracyClient.cpp")

foreach(source IN LISTS Common_FILES)
    get_filename_component(source_path "${source}" PATH)
    string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    source_group("${source_path_msvc}" FILES "${source}")
endforeach()  

set(DILIGENT_NO_FORMAT_VALIDATION ON)

#Bullet config
set(BUILD_OPENGL3_DEMOS OFF)
set(BUILD_BULLET2_DEMOS OFF)
set(BUILD_EXTRAS OFF)
set(BUILD_UNIT_TESTS OFF)
set(BUILD_ENET OFF)
set(BUILD_CLSOCKET OFF)
set(BUILD_CPU_DEMOS OFF)
set(BULLET2_MULTITHREADING ON)

add_subdirectory(DiligentEngine)
add_subdirectory(assimp)
add_subdirectory(bullet3)

target_compile_options(Nutshell PRIVATE -DUNICODE -DENGINE_DLL)
target_include_directories(Nutshell PRIVATE 
    "DiligentEngine/DiligentCore" 
    "DiligentEngine/DiligentTools" 
    "entt/src/"
    "src/"
    "assimp/include"
    "bullet3/src"
    "tracy/"
)

SOURCE_GROUP("ThirdParty" bullet3 assimp DiligentEngine)

target_link_libraries(Nutshell
    Diligent-GraphicsEngineD3D11-shared
    Diligent-GraphicsEngineOpenGL-shared
    Diligent-GraphicsEngineD3D12-shared
    Diligent-GraphicsEngineVk-shared
    Diligent-Imgui
    Diligent-TextureLoader
    assimp
    BulletDynamics 
    BulletCollision 
    LinearMath
)
copy_required_dlls(Nutshell)

# Copy assets to target folder
add_custom_command(TARGET Nutshell POST_BUILD
COMMAND ${CMAKE_COMMAND} -E copy_directory
    "${CMAKE_CURRENT_SOURCE_DIR}/assets"
    "\"$<TARGET_FILE_DIR:Nutshell>\"")


target_compile_features(Nutshell PRIVATE cxx_std_17)

add_definitions(-DTRACY_ENABLE -DNOMINMAX) 
